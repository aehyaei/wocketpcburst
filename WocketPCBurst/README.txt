This project is a Java application runs on computer for calibrating Wockets.


The program helps to calibrate Wockets'
  *Sampling rate
  *Accelerometer
  *Noise 
  *Battery

Wockets need to be initialized and fully charged before the calibration procedure.

The program is executable in both Eclipse and Netbeans platforms."mainJFrame.java" in UserInterface folder includes the main function. The project folder also contain a runnable jar file in the dist folder that could be executed via command prompt.   

The application allows setting Wockets with different sampling rates in the defined range (5-126) to the highest possible precision which depends on the sampling rate. 


