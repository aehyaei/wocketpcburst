package xmlManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;
import wockets.data.ParticipantData;
import wockets.data.WocketSensor;


public class XMLWriter {	
	    
    private static int num = 4;
    private static ParticipantData pData;
    
    public XMLWriter() {
	}

    public static void write (ParticipantData pd) throws XmlPullParserException, IOException {
    	pData = pd;
    	ArrayList<WocketSensor> wockets = pData.getWockets();
    	String fileName = "sleepStudy.xml";
        String path = "c:/sleepStudy/" + pData.getpID() + "/";
        File outputDir = new File(path);
    	if(!outputDir.isDirectory()){
			outputDir.mkdirs();
		}
    	File wocketFile = new File(path + fileName);
    	if(!wocketFile.exists()){
			wocketFile.createNewFile();
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(wocketFile));
		
    	XmlPullParserFactory factory = XmlPullParserFactory.newInstance(
        System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        XmlSerializer xmlSerializer = factory.newSerializer();
        System.out.println("serializer implementation class is "+ xmlSerializer.getClass());
        
        xmlSerializer.setOutput(writer);
		xmlSerializer.startDocument("UTF-8",true);
		
		xmlSerializer.startTag(null, "STUDYDATA");
		xmlSerializer.attribute(null, "xmlns", "urn:mites-schema");
		xmlSerializer.attribute(null, "dataset", "Sleep Study");		
		
		xmlSerializer.startTag(null, "PARTICIPANT");					
			xmlSerializer.attribute(null, "id", ""+ pData.getpID());			
		xmlSerializer.endTag(null, "PARTICIPANT");
				
		xmlSerializer.startTag(null, "SENSORDATA");		
		
		//------------Receivers-------------------------------
		xmlSerializer.startTag(null, "RECEIVERS");
		for (int i = 0; i < num; i++) {
			xmlSerializer.startTag(null, "RECEIVER");
			xmlSerializer.attribute(null, "id", "" + i);
			xmlSerializer.attribute(null, "type", "RFCOMM");
			xmlSerializer.attribute(null, "MacAddress", wockets.get(i).getMacID());
			xmlSerializer.attribute(null, "PIN", "1234");
			xmlSerializer.attribute(null, "PortNumber", "9");
			xmlSerializer.attribute(null, "Parity", "False");
			xmlSerializer.attribute(null, "StopBit", "True");
			xmlSerializer.attribute(null, "BaudRate", "57600");
			xmlSerializer.attribute(null, "BufferSize", "4096");
			xmlSerializer.attribute(null, "MaxSR", "70");
			xmlSerializer.endTag(null, "RECEIVER");
		}		
		xmlSerializer.endTag(null, "RECEIVERS");
		//----------------------------------------------------
		
		//------------Decoders--------------------------------
		xmlSerializer.startTag(null, "DECODERS");		
		for (int i = 0; i < num; i++) {
			xmlSerializer.startTag(null, "DECODER");
			xmlSerializer.attribute(null, "id", ""+i);
			xmlSerializer.attribute(null, "type", "Wockets");
			xmlSerializer.endTag(null, "DECODER");
		}
		xmlSerializer.endTag(null, "DECODERS");
		//----------------------------------------------------
		
		//------------Sensors--------------------------------
		xmlSerializer.startTag(null, "SENSORS");
			for (int i = 0; i < num; i++) {
				xmlSerializer.startTag(null, "SENSOR");
				xmlSerializer.attribute(null, "class", "Wockets");
				xmlSerializer.attribute(null, "type", "ACCEL");
				xmlSerializer.attribute(null, "id", ""+i);
				xmlSerializer.attribute(null, "SR", "40");
				xmlSerializer.attribute(null, "MINRANGE", "0");
				xmlSerializer.attribute(null, "MAXRANGE", "1024");
				xmlSerializer.attribute(null, "POSITION", ""+wockets.get(i).getPosition());
				xmlSerializer.attribute(null, "LOCATION", ""+wockets.get(i).getLocation());
				xmlSerializer.attribute(null, "COLOR", ""+wockets.get(i).getColor());
				xmlSerializer.attribute(null, "DESCRIPTION", "acc");
				
				/*xmlSerializer.startTag(null, "ID");
				xmlSerializer.attribute(null, "id", ""+i);
				xmlSerializer.endTag(null, "ID");
				
				xmlSerializer.startTag(null, "SR");
				xmlSerializer.attribute(null, "text", "91");
				xmlSerializer.endTag(null, "SR");

				xmlSerializer.startTag(null, "RANGE");
				xmlSerializer.attribute(null, "min", "0");
				xmlSerializer.attribute(null, "max", "1024");
				xmlSerializer.endTag(null, "RANGE");

				xmlSerializer.startTag(null, "OBJECT");
				xmlSerializer.attribute(null, "text", "");
				xmlSerializer.endTag(null, "OBJECT");

				xmlSerializer.startTag(null, "LOCATION");
				xmlSerializer.attribute(null, "text", wocket.getLocation());
				xmlSerializer.endTag(null, "LOCATION");
				
				xmlSerializer.startTag(null, "POSITION");
				xmlSerializer.attribute(null, "text", wocket.getPosition());
				xmlSerializer.endTag(null, "POSITION");
				
				xmlSerializer.startTag(null, "COLOR");
				xmlSerializer.attribute(null, "text", wocket.getColor());
				xmlSerializer.endTag(null, "COLOR");

				xmlSerializer.startTag(null, "DESCRIPTION");
				xmlSerializer.attribute(null, "text", "acc");
				xmlSerializer.endTag(null, "DESCRIPTION");

				xmlSerializer.startTag(null, "RECEIVER");
				xmlSerializer.attribute(null, "id", ""+i);
				xmlSerializer.endTag(null, "RECEIVER");

				xmlSerializer.startTag(null, "DECODER");
				xmlSerializer.attribute(null, "id", ""+i);
				xmlSerializer.endTag(null, "DECODER");

				xmlSerializer.startTag(null, "CALIBRATION");
				xmlSerializer.attribute(null, "x1g", "779.92");
				xmlSerializer.attribute(null, "xn1g", "241.40");
				xmlSerializer.attribute(null, "y1g", "747.20");
				xmlSerializer.attribute(null, "yn1g", "215.72");
				xmlSerializer.attribute(null, "z1g", "832.26");
				xmlSerializer.attribute(null, "zn1g", "293.57");
				xmlSerializer.attribute(null, "xstd", "0.25");
				xmlSerializer.attribute(null, "ystd", "0.26");
				xmlSerializer.attribute(null, "zstd", "0.31");
				xmlSerializer.endTag(null, "CALIBRATION");*/

				xmlSerializer.endTag(null, "SENSOR");
			}
			
		xmlSerializer.endTag(null, "SENSORS");
		//-----------------------------------------------------------
		
		xmlSerializer.endTag(null, "SENSORDATA");
		xmlSerializer.endTag(null, "STUDYDATA");
		xmlSerializer.endDocument();
		
		xmlSerializer.flush();
		writer.flush();
		writer.close();   
    }
		
}
	
