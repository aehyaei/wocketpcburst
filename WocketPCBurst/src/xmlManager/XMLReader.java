package xmlManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import wockets.data.ParticipantData;
import wockets.data.WocketSensor;


public class XMLReader{
		
	public static BufferedInputStream fileFinder(String pID){
		String fileName = "sleepStudy.xml";
        String filePath = "c:/sleepStudy/" + pID +"/";
        
		BufferedInputStream inputStream = null;
		File inputFile = new File(filePath + fileName);
		if(!inputFile.exists()){
			//System.out.println("No XML file.");
			return null;
		}
		try {
			inputStream = new BufferedInputStream(new FileInputStream(inputFile));
		} catch (FileNotFoundException e) {			
			System.out.println("No Input Stream.");
		}
		return inputStream;
	}
	
	public static ParticipantData xmlParser(BufferedInputStream iStream){
		ParticipantData pData = new ParticipantData();
		pData.setWockets(new ArrayList<WocketSensor>());
		try {
            XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
            parser.setInput(iStream, null);
            
            int eventType = parser.getEventType();
            while(eventType != XmlPullParser.END_DOCUMENT) {
                switch(eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                        
                    case XmlPullParser.START_TAG:
                        String tagName = parser.getName();
                        if(tagName.equalsIgnoreCase("participant")) {
                        	pData.setpID(parser.getAttributeValue(0));
                        }
                        WocketSensor wocket = new WocketSensor();
                        if(tagName.equalsIgnoreCase("receiver")) {
                        	String id = null;
                			String macAddress = null;
                			for (int i = 0; i < parser.getAttributeCount(); i++) {                				
                				if(parser.getAttributeName(i).equalsIgnoreCase("macaddress")){
                					macAddress = parser.getAttributeValue(i);
                				}
                				if(parser.getAttributeName(i).equalsIgnoreCase("id")){
                					id = parser.getAttributeValue(i);
                				}
                			}
                			pData.getWockets().add(new WocketSensor());
                			pData.getWockets().get(Integer.parseInt(id)).setMacID(macAddress); 
                        } 
                        else if(tagName.equalsIgnoreCase("sensor")) {
                        	String id = null;
                			String location = null;
                			String position = null;
                			String color = null;
                			
                			for (int i = 0; i < parser.getAttributeCount(); i++) {	
                				String attName = parser.getAttributeName(i);
                				if(attName.equalsIgnoreCase("location")){
                					location = parser.getAttributeValue(i);
                				}
                				if(attName.equalsIgnoreCase("position")){
                					position = parser.getAttributeValue(i);
                				}
                				if(attName.equalsIgnoreCase("color")){
                					color = parser.getAttributeValue(i);
                				}
                				if(attName.equalsIgnoreCase("id")){
                					id = parser.getAttributeValue(i);
                				}
                			}
                			int intId = Integer.parseInt(id);
                			pData.getWockets().get(intId).setLocation(location);
                			pData.getWockets().get(intId).setPosition(position);
                			pData.getWockets().get(intId).setColor(color);
                        }
                        pData.getWockets().add(wocket);
                        break;
                }
                // jump to next event
                eventType = parser.next();
            }
            

        } catch (XmlPullParserException e) {
        	System.out.println("In XMLReader; XmlPullParserException:" + e);
        } catch (IOException e) {
        	System.out.println("In XMLReader; IOException:" + e);
        }	
		return pData;
	}
	
	
}
