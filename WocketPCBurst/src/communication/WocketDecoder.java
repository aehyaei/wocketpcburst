package communication;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import bafWocket.BafEncoder;
import classification.ClassifierOnline;
import wockets.data.SWData;
import wockets.data.WocketParam;
import wockets.data.Acc;
import wockets.data.types.ResponseTypes;
import wockets.data.types.SensorDataType;


public final class WocketDecoder
{
    //private final int BUFFER_SIZE = 3072;
    private final int ACCSIZE = 13;
    private final int MAXRAW = 2996;//The maximum number of raw data packets which a Wocket can save
    private final int ONE_MIN = 60000;
    private final int DEFAULT_RAW_DISTANCE = 25;
    protected byte[] packet;
    protected int packetPosition = 0;
    private boolean headerSeen = false;
    public long _TotalSamples = 0;
	
	SensorDataType packetType;
	private ResponseTypes responseType;
	public int activityCountOffset = 0;
	public int batchCount = 0;
    public int samplingRate = 0;   
	int lastSentSeqNum = 0;
	
    private short prevx = 0;
    private short prevy = 0;
    private short prevz = 0;
    private BafEncoder bafEncoder; 
   
    private int bytesToRead = 0;
    private int lastRcvSeqNum = -1;  

    //private int[][] tenSecData = new int [10000][3];
    private int[][] accValues = new int [2][ACCSIZE];
    private int[] accCnt = {0, 0};
    boolean[] sazonovFull = {false, false};
    //boolean[] sadehFull = {false, false};
    //boolean[] kripkeFull = {false, false};
    
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat mHealthFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");	 
	SimpleDateFormat hourFormat = new SimpleDateFormat("HH");
	SimpleDateFormat fileNameFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
	SimpleDateFormat dateFormat_raw = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
	String annotLabel = "START_TIME,END_TIME,LABEL,VARIANCE";
	String rawLabel = "TIME_STAMP,X,Y,Z";
	String acLabel = "TIME_STAMP,VALUE";
	String orientationLabel = "TIME_STAMP,PITCH,ROLL,THETA";
	SWData swData = new SWData("Unknown", null, 0);
	boolean sleepStartFlag = false;
	boolean wakeStartFlag = false;
	Date sleepStartTime = null;
	Date wakeStartTime = null;
	ArrayList<SWData> wakeData = new ArrayList<SWData>();
	boolean question = false;
	boolean swChanged = false;
	double rawDataDistance;
	double summaryDataDistance;
	double rawMillis;// = currentMillis - (long)(MAXRAW * rawDataDistance);
	
    //*************************************************Decode***************************************************************************    
	public void Decode (byte[] data, int end, WocketParam wocketParam, String macID, Calendar calendar, String subID) {

		long currentMillis = calendar.getTimeInMillis();	
		int uncompressedPDUCount = 0;
	    int compressedPDUCount = 0;
		int rawDataIndex = 0;
		int sec = -1;
		//Date startTime = null;
		int sumx = 0, sumy = 0, sumz = 0, counter = 0;
		rawDataDistance = DEFAULT_RAW_DISTANCE;
		summaryDataDistance = ONE_MIN;
		rawMillis = currentMillis - (long)(MAXRAW * rawDataDistance);
    	Calendar temp = Calendar.getInstance();
		String outputFilePath;
		String fileName; 
		String msg;
		
		bafEncoder = new BafEncoder();
		//bcFalg = false;	
		batchCount = 0;
		lastRcvSeqNum = wocketParam.last_seqNumber;
		
		while ((rawDataIndex < end)) {
			
			if (((data[rawDataIndex]) & 0x80) != 0) {// Header:First byte of a packet				
				packetPosition = 0;
				headerSeen = true;
				packetType = SensorDataType.values()[((data[rawDataIndex] << 1) & 0xff) >> 6];
				bytesToRead = DefineByteToRead(packetType,data[rawDataIndex]);
				packet = new byte[bytesToRead];
			}

			if ((headerSeen == true) && (packetPosition < bytesToRead)) {
				packet[packetPosition] = data[rawDataIndex];
			}

			packetPosition++;

			if ((packetPosition == bytesToRead)) {// a full packet is received
				
				// -----------------------Decode raw data packets-------------------------------------
				if ((packetType == SensorDataType.UNCOMPRESSED_DATA_PDU) || (packetType == SensorDataType.COMPRESSED_DATA_PDU)) {
					bytesToRead = 0;					
															    	
					bytesToRead = 0;
					short x = 0;
					short y = 0;
					short z = 0;
															    	
					if (packetType == SensorDataType.UNCOMPRESSED_DATA_PDU) {
						x = (short) ((((int) ((int) this.packet[0] & 0x03)) << 8) | (((int) ((int) this.packet[1] & 0x7f)) << 1) | (((int) ((int) this.packet[2] & 0x40)) >> 6));
						y = (short) ((((int) ((int) this.packet[2] & 0x3f)) << 4) | (((int) ((int) this.packet[3] & 0x78)) >> 3));
						z = (short) ((((int) ((int) this.packet[3] & 0x07)) << 7) | ((int) ((int) this.packet[4] & 0x7f)));
						uncompressedPDUCount++;
					} else {
						x = (short) (((this.packet[0] & 0x0f) << 1) | ((this.packet[1] & 0x40) >> 6));
						x = ((((short) ((this.packet[0] >> 4) & 0x01)) == 1) ? ((short) (prevx + x))
								: ((short) (prevx - x)));
						y = (short) (this.packet[1] & 0x1f);
						y = ((((short) ((this.packet[1] >> 5) & 0x01)) == 1) ? ((short) (prevy + y))
								: ((short) (prevy - y)));
						z = (short) ((this.packet[2] >> 1) & 0x1f);
						z = ((((short) ((this.packet[2] >> 6) & 0x01)) == 1) ? ((short) (prevz + z))
								: ((short) (prevz - z)));
						compressedPDUCount++;
					}
					
					prevx = x;
					prevy = y;
					prevz = z;
					
					// --------------------------calculate time stamp-------------------					
					/*if (bcFalg) {
						if (batchCount < 2000) // error in decoding the the "batchCount".
							break;
							
						double connectionPeriod = currentMillis - wocketParam.prevTStamp;
						if (connectionPeriod > (MAXRAW * wocketParam.period)) { 	 // This case shows the delay in the connection caused some raw data to be lost and batchCount value is 
																					 // no longer could be used to calculate distances between samples.
							rawDataDistance = wocketParam.period;
						} else {
							rawDataDistance = (connectionPeriod / batchCount);
						} 

						tempMillis = currentMillis - (long)(batchCount * rawDataDistance);
						bcFalg = false;
					} 	*/
					if (batchCount < 2000) // error in decoding the the "batchCount".
						break;
										
					rawMillis = rawMillis + rawDataDistance;
			    	temp.setTimeInMillis((long)rawMillis);
			    	
			    	// -----------------------save baf raw data--------------------------
			    	bafEncoder.encodeAndSaveData(temp, (int) x, (int) y, (int) z, wocketParam.id, subID);	
			    	
			    	// -------------------save control raw data--------------------------
			    	outputFilePath = "c:/sleepStudy/" + subID + "/sensor/control/";// + dateFormat.format(temp.getTime()) + File.separator;
			    	String location = (wocketParam.id.equals("00") ? "Wrist" : "Ankle");
			    	fileName = outputFilePath + "Wocket_" + wocketParam.id + "_RawData_Dominant_" + location + ".csv";
					msg = temp.getTimeInMillis() + "," + x + "," + y + "," + z;	
			    	writeInFile(outputFilePath, fileName, null, msg); //raw control data for old visualizer
			    	
			    	fileName = outputFilePath + "Wocket_" + wocketParam.id + "_RAW_DATA.csv";
					msg = mHealthFormat.format(temp.getTime()) + "," + x + "," + y + "," + z;	
			    	writeInFile(outputFilePath, fileName, rawLabel, msg); //raw control data for new visualizer
			    	
			    	//---------------------save orientation data-------------------
			    	/*double pitch = Math.atan(x / (Math.sqrt((y*y) + (z*z))));
			    	double roll = Math.atan(y / (Math.sqrt((x*x) + (z*z))));
			    	double theta = Math.atan((Math.sqrt((x*x) + (z*z))) / z);
			    	fileName = outputFilePath + "Wocket_" + wocketParam.id + "_Orientation.csv";
			    	msg = mHealthFormat.format(temp.getTime()) + "," + pitch + "," + roll + "," + theta;
			    	writeInFile(outputFilePath, fileName, orientationLabel, msg);*/
			    	
			    	//---------------------save 1s summary data--------------------
			    	if (sec == -1) {
						sec = temp.getTime().getSeconds();						
						//startTime = temp.getTime();
						
					} else if (sec != temp.getTime().getSeconds()) {
						sumx /= counter;
						sumy /= counter;
						sumz /= counter;
						msg = sumx + "," + sumy + "," + sumz;
				    	
						/*dateFormat_raw.setTimeZone(TimeZone.getTimeZone("GMT")); //To show on Visualizer, this setting should remain unchanged								    	
				    	fileName = outputFilePath + "Wocket_" + wocketParam.id + "_1s-RawMean_Dominant_" + location + ".csv";
						String s = temp.getTimeInMillis() + "," + dateFormat_raw.format(temp.getTime()) + "," + msg;	
				    	writeInFile(outputFilePath, fileName, null, s); //summary control data for old visualizer*/
						
				    	fileName = outputFilePath + "Wocket_" + wocketParam.id + "_1s-RawMean.csv";
				    	String s = mHealthFormat.format(temp.getTime()) + "," + msg;	
				    	writeInFile(outputFilePath, fileName, rawLabel, s);  //summary control data for new visualizer
				    	
				    	//---------------------save summary orientation data-------------------
				    	/*double pitch = Math.atan(sumx / (Math.sqrt((sumy*sumy) + (sumz*sumz))));
				    	double roll = Math.atan(sumy / (Math.sqrt((sumx*sumx) + (sumz*sumz))));
				    	double theta = Math.atan((Math.sqrt((sumx*sumx) + (sumz*sumz))) / sumz);
				    	fileName = outputFilePath + "Wocket_" + wocketParam.id + "_1s-Orientation.csv";
				    	writeInFile(outputFilePath, fileName, orientationLabel, pitch + "," + roll + "," + theta);*/
				    	//---------------------save sample orientation data--------------------
				    	double pitch = Math.atan(x / (Math.sqrt((y*y) + (z*z))));
				    	double roll = Math.atan(y / (Math.sqrt((x*x) + (z*z))));
				    	double theta = Math.atan((Math.sqrt((x*x) + (z*z))) / z);
				    	fileName = outputFilePath + "Wocket_" + wocketParam.id + "_Orientation.csv";
				    	msg = mHealthFormat.format(temp.getTime()) + "," + pitch + "," + roll + "," + theta;
				    	writeInFile(outputFilePath, fileName, orientationLabel, msg);
								    	
						sumx = sumy = sumz = counter = 0;
						sec = temp.getTime().getSeconds();
					}
			    	
			    	//-------------------------------------------------------------
					sumx += x;
					sumy += y;
					sumz += z;
					counter ++;				
					
				
				// -----------------------Decode response packets-------------------------------------
				} else if (packetType == SensorDataType.RESPONSE_PDU) {
					DecodeResponce(responseType, bytesToRead, wocketParam, subID, macID, calendar);
				}
				this.packetPosition = 0;
				this.headerSeen = false;
				bytesToRead = 0;
			}// end of processing one complete packet
			
			rawDataIndex++;
		}// End while
	
		wocketParam.uncompressedCount = uncompressedPDUCount;
		wocketParam.compressedCount = compressedPDUCount;
		wocketParam.last_seqNumber = lastRcvSeqNum; 
		wocketParam.lastConnection = currentMillis;
		wocketParam.batchCount = batchCount;		
	}
	
	
    //**************************Define Byte To Read***********************************
    public int DefineByteToRead(SensorDataType packetType, byte pack_header){
    	int bytesToRead = 0;
    	switch(packetType)
		{
			case UNCOMPRESSED_DATA_PDU:
			{
				bytesToRead = 5;
				break;
			}
			case COMPRESSED_DATA_PDU:
			{
				bytesToRead = 3;
				break;	    					
			}
			case RESPONSE_PDU :
			{	    					
				responseType = ResponseTypes.values()[((int)(pack_header & 0x1f))];
				switch(responseType)
				{
					case BP_RSP:
					case SENS_RSP:
					case SR_RSP:
					case ALT_RSP:
					case PDT_RSP:
					case WTM_RSP:
					case TM_RSP:
					case HV_RSP:
					case FV_RSP:
						bytesToRead = 2;
						break;
					case BL_RSP:
					case ACC_RSP:
					case OFT_RSP:
						bytesToRead = 3;
						break;
					case BC_RSP:
						bytesToRead = 4;
						break;
					case TCT_RSP:
						bytesToRead = 5;
						break;
					case PC_RSP:
					case AC_RSP:
						bytesToRead = 6;
						break;
					case CAL_RSP:
					case BTCAL_RSP:
						bytesToRead = 10;
						break;
					default:
						break;
				}                                   
			}
			default:
				break;
		} //End switch(packetType)
    	return bytesToRead;
    }
    
    
    //*************************Decode Responce***************************************
    public void DecodeResponce (ResponseTypes responseType, int bytesToRead, WocketParam wp, String subID, String macID, Calendar calendar){
    	
    	switch (responseType){
            case BL_RSP:
                int batteryLevel = ((packet[1] & 0x7f) << 3) | ((packet[2] & 0x70) >> 4);
                if (batteryLevel > 500){
                	wp.batteryLevel = batteryLevel;                	
                }
                break;
                
            case SR_RSP:
                int samplingRate= (this.packet[1]&0x7f);                  
                this.samplingRate = samplingRate;
                break;
                
            case FV_RSP:
                int fVersion = (this.packet[1] & 0x7f);
                WocketLogger.logErrorMessage("fVersion: "+ fVersion);
                break;
                
            case BC_RSP:
            	batchCount = ((((this.packet[1] & 0x7f) << 7) | (this.packet[2] & 0x7f)) << 2) | ((this.packet[3] & 0x60) >> 5);
            	long connectionPeriod = calendar.getTimeInMillis() - wp.lastConnection;
				if (connectionPeriod > (MAXRAW * wp.period)) { 	 // This case shows the delay in the connection caused some raw data to be lost and batchCount value is 
																			 // no longer could be used to calculate distances between samples.
					rawDataDistance = wp.period;
				} else {
					rawDataDistance = (connectionPeriod / batchCount);
					rawMillis = calendar.getTimeInMillis() - (long)(batchCount * rawDataDistance);
				}            	
                //bcFalg = true;
                break;
                
            case OFT_RSP:
                int offset = ((this.packet[1] & 0x7f) << 7) | (this.packet[2] & 0x7f);
                activityCountOffset = offset;
                //System.out.println("Offset: "+ offset);
                break;
                
            case ACC_RSP: // last seqNum sent by Wocket
            	lastSentSeqNum = (((this.packet[1] & 0x7f) << 7) | (this.packet[2] & 0x7f)) - 1;
            	if (wp.prevTstamp == 0) {
            		summaryDataDistance = ONE_MIN;
            	} else {
	            	summaryDataDistance = (calendar.getTimeInMillis() - wp.prevTstamp) / (lastSentSeqNum - lastRcvSeqNum);
            	}
            	System.out.println("summaryDataDistance:" + summaryDataDistance);
                break;                

            case AC_RSP:
                int acSeqNum = ((this.packet[1] & 0x7f) << 9) | ((this.packet[2] & 0x7f) << 2) | ((this.packet[3] >> 5) & 0x03);
                int acCount = ((this.packet[3] & 0x1f) << 11) | ((this.packet[4] & 0x7f) << 4) | ((this.packet[5] >> 2) & 0x0f);                  
                if ( ((acSeqNum - lastRcvSeqNum) == 1) || (lastRcvSeqNum == -1) ) {
                	long diff = calendar.getTimeInMillis() - (long)((lastSentSeqNum - acSeqNum) * summaryDataDistance) - (long)(activityCountOffset * DEFAULT_RAW_DISTANCE);
                	Calendar temp = Calendar.getInstance();
                	temp.setTimeInMillis(diff);               	
                	String outputFilePath = "c:/sleepStudy/" + subID + "/sensor/" + dateFormat.format(temp.getTime()) + File.separator + hourFormat.format(temp.getTime()) + File.separator;	
                	String fileName = outputFilePath + getFileNameForCurrentHour(outputFilePath, calendar.getTime(), wp.id);
		            String msg = mHealthFormat.format(temp.getTime()) + "," + acCount + "," + acSeqNum;
		            writeInFile(outputFilePath, fileName, acLabel, msg);
		            //------------------
		            outputFilePath = "c:/sleepStudy/" + subID + "/sensor/control/";// + dateFormat.format(temp.getTime()) + File.separator;	
                	fileName = outputFilePath + "Wocket." +  wp.id + ".ACTIVITY_COUNT.csv";
		            writeInFile(outputFilePath, fileName, acLabel, msg);
		            //------------------
                	lastRcvSeqNum = acSeqNum;
                	wp.prevTstamp = temp.getTimeInMillis();
                	//-------------------------
                	int id = Integer.parseInt(wp.id);
                	accValues[id][accCnt[id]] = acCount;
                	if (wp.accs.size() >= 2880) //save up to 48 hours of data
                		wp.accs.remove(0);
                	wp.accs.add(new Acc(temp.getTime(), acCount));
                	//System.out.println("cnt: " + accCnt[id]);
                	//--------------------------------------------
                	if ((!sazonovFull[id]) && (accCnt[id] == 8)) {
                		sazonovFull[id] = true;
                		System.out.println("sazonovFull: " + id);
                	}
                	/*if ((!sadehFull[id]) && (accCnt[id] == 10)) {
                		sadehFull[id] = true;
                		System.out.println("sadehFull: " + id);
                	}
                	if ((!kripkeFull[id]) && (accCnt[id] == 12)) {
                		kripkeFull[id] = true;
                		System.out.println("kripkeFull: " + id);
                	}*/
                	//------------------Sazonov---------------------                	
                	outputFilePath = "c:/sleepStudy/" + subID + File.separator + "sensors" + dateFormat.format(temp.getTime()) + File.separator ;                	
                	if (sazonovFull[id]) {
                		Date startTime = temp.getTime();
	                	int[] data = new int[9];
	                	int i = accCnt[id] - 8;
	                	if (i < 0) i += ACCSIZE;                	 
	                	for (int j = 0; j < 9; j++) {
	                		data[j] = accValues[id][i];                		
	                		i++;
	                		if (i >= ACCSIZE)
	                			i -= ACCSIZE;                		
	                	}	                	
	                	fileName = outputFilePath + "Wocket_" + wp.id + "_sazonov_annotation.csv";
	                	startTime.setMinutes(startTime.getMinutes() - 1);
	                	String status = ClassifierOnline.sazonov(data);
	            		System.out.println("sazonov: " + status);
			            msg = mHealthFormat.format(startTime) + "," + mHealthFormat.format(temp.getTime()) + "," + status;
			            writeInFile(outputFilePath, fileName, annotLabel, msg);	
			          //---------------------------------
			            if (wp.id.equals("00")) {
				            outputFilePath = "c:/sleepStudy/" + subID + File.separator + "sensors"+ File.separator + "control" + File.separator;
			                fileName = outputFilePath + "Wocket_" + wp.id + "_SW.csv";
				            if (!status.equals(wp.label)) {
				            	if (!swData.getStatus().equals("Unknown")) {
					            	msg = mHealthFormat.format(swData.getStartTime()) + "," + wp.label + "," + swData.getLength();					            
						            writeInFile(outputFilePath, fileName, "", msg);	
						            wp.swData = swData;	
				            	} 					            	
				            	if ((sleepStartFlag) && (status.equals("Sleep"))) {
				            		wakeData.add(swData);				            		
				            	}
				            	swData = new SWData(status, startTime, 1);
				            	wp.label = status;				            	
				            } else {			            	
				            	swData.setLength(swData.getLength() + 1);
				            	wp.swData = null;
				            } 				            
				            //--------calculate important statistics for survey questions----------------------
				            int hour = Calendar.getInstance().getTime().getHours();
				            if (((hour > 21) || (hour < 6)) && (!sleepStartFlag) && (swData.getLength() > 15) && (status.equals("Sleep"))) {
				                msg = mHealthFormat.format(swData.getStartTime()) + "," + "Sleep start time";
				                writeInFile(outputFilePath, fileName, "", msg);
				                sleepStartFlag = true;
				                sleepStartTime = swData.getStartTime();
			                }
				            //----------
			                if ((hour > 6) && (hour < 15) && (!wakeStartFlag) && (swData.getLength() > 20) && (status.equals("Wake"))) {
				                msg = mHealthFormat.format(swData.getStartTime()) + "," + "Wake start time";
				                writeInFile(outputFilePath, fileName, "", msg);
				                wakeStartFlag = true;
				                wakeStartTime = swData.getStartTime();
			                }
			                //-----------
			                if ((wakeStartFlag) && (!question)) {
			                	int size = wakeData.size();
			                	if (size == 0) 
			                		writeInFile(outputFilePath, fileName, "", "No considerable awake time during last night!");
			                	else {
		                			for (int m = size - 1; m > 0; m--) {
		                				if (Math.round((wakeData.get(m).getStartTime().getTime() - wakeData.get(m - 1).getStartTime().getTime()) / 60000) < (10 + wakeData.get(m - 1).getLength())) {				            				
		                					wakeData.get(m - 1).setLength(wakeData.get(m - 1).getLength() + wakeData.get(m).getLength());
		                					wakeData.remove(m);
					            		} 
		                			}				            			
			                	}
			                	size = wakeData.size();
			                	if (size > 0) {
			                		for (int m = size - 1; m >= 0; m--) {
			                			if ( (wakeData.get(m).getLength() < 5) 
			                					/*|| (wakeData.get(m).getStartTime().getTime() - sleepStartTime.getTime() < 1800000) 
			                					|| (wakeStartTime.getTime() - wakeData.get(m).getStartTime().getTime() < 1800000)*/ )
				            				wakeData.remove(m);
				            		}
			                	}
			                	size = wakeData.size();
			                	if (size > 0) {
			                		//find the 3 longest awakening time 
			                		for (int m = 0; m < Math.min(3, size); m++) {			                	
				                		for (int n = m + 1; n < size; n++) {
				                			if (wakeData.get(m).getLength() < wakeData.get(n).getLength()) {
				                				SWData sw = wakeData.get(m);
				                				wakeData.set(m, wakeData.get(n));
				                				wakeData.set(n, sw);
				                			}
				                		}
				                		msg = mHealthFormat.format(wakeData.get(m).getStartTime()) + "," + "Q" + m + "_time, " + wakeData.get(m).getLength() + " min";
						                writeInFile(outputFilePath, fileName, "", msg);
			                		}
			                	} 
			                	question = true;
			                }
			            }
			            
                	}
                	//-------------------Sadeh--------------------
                	/*if (sadehFull[id]) {
                		Date startTime = temp.getTime();
	                	int[] data = new int[11];
	                	int i = accCnt[id] - 10;
	                	if (i < 0) i += ACCSIZE;                	 
	                	for (int j = 0; j < 11; j++) {
	                		data[j] = accValues[id][i];                		
	                		i++;
	                		if (i >= ACCSIZE)
	                			i -= ACCSIZE;                		
	                	}	                	
	                	fileName = outputFilePath + "Wocket_" + wp.id + "_sadeh_annotation.csv";
	                	startTime.setMinutes(startTime.getMinutes() - 6);
	                	Date endDate = temp.getTime();
	                	endDate.setMinutes(endDate.getMinutes() - 5);
			            msg = mHealthFormat.format(startTime) + "," + mHealthFormat.format(endDate) + "," + ClassifierOnline.sadeh(data);
			            writeInFile(outputFilePath, fileName, annotLabel, msg);
                	}
                	//---------------------Kripke------------------
                	outputFilePath = "c:/sleepStudy/" + subID + "/annotations/";
                	if (kripkeFull[id]) {
                		Date startTime = temp.getTime();
	                	int[] data = new int[13];
	                	int i = accCnt[id] - 12;
	                	if (i < 0) i += ACCSIZE;                	 
	                	for (int j = 0; j < 13; j++) {
	                		data[j] = accValues[id][i];                		
	                		i++;
	                		if (i >= ACCSIZE)
	                			i -= ACCSIZE;                		
	                	}	                	
	                	fileName = outputFilePath + "Wocket_" + wp.id + "_kripke_annotation.csv";
	                	startTime.setMinutes(startTime.getMinutes() - 3);
	                	Date endDate = temp.getTime();
	                	endDate.setMinutes(endDate.getMinutes() - 2);
			            msg = mHealthFormat.format(startTime) + "," + mHealthFormat.format(endDate) + "," + ClassifierOnline.kripke(data);
			            writeInFile(outputFilePath, fileName, annotLabel, msg);			            
                	}*/
                	//--------------------------------------------
                	accCnt[id]++;
                	if (accCnt[id] == ACCSIZE) {
                		accCnt[id] = 0;
                	}
                } 
                break;
                
            case CAL_RSP:
                /*int x1G  = ((this.packet[1] & 0x7f) << 3) | ((this.packet[2] & 0x70) >> 4);
                int xN1G = ((this.packet[2] & 0x0f) << 6) | ((this.packet[3] & 0x7e) >> 1);
                int yY1G = ((this.packet[3] & 0x01) << 9) | ((this.packet[4] & 0x7f) << 2) | ((this.packet[5] & 0x60) >> 5);
                int yN1G = ((this.packet[5] & 0x1f) << 5) | ((this.packet[6] & 0x7c) >> 2);
                int z1G  = ((this.packet[6] & 0x03) << 8) | ((this.packet[7] & 0x7f) << 1) | ((this.packet[8] & 0x40) >> 6);
                int zN1G = ((this.packet[8] & 0x3f) << 4) | ((this.packet[9] & 0x78) >> 3);*/
                break;
                
            case BTCAL_RSP:
               /* int cAL100 = ((this.packet[1] & 0x7f) << 3) | ((this.packet[2] & 0x70) >> 4);
                int cAL80 = ((this.packet[2] & 0x0f) << 6) | ((this.packet[3] & 0x7e) >> 1);
                int cAL60 = ((this.packet[3] & 0x01) << 9) | ((this.packet[4] & 0x7f) << 2) | ((this.packet[5] & 0x60) >> 5);
                int cAL40 = ((this.packet[5] & 0x1f) << 5) | ((this.packet[6] & 0x7c) >> 2);
                int cAL20 = ((this.packet[6] & 0x03) << 8) | ((this.packet[7] & 0x7f) << 1) | ((this.packet[8] & 0x40) >> 6);
                int cAL10 = ((this.packet[8] & 0x3f) << 4) | ((this.packet[9] & 0x78) >> 3);*/
                break;
                
            case BP_RSP:
                //int percent= (this.packet[1] & 0x7f);
                break;
                
            case WTM_RSP:
                //TransmissionMode transmissionMode = TransmissionMode.values()[((this.packet[1] >> 4) & 0x07)];/
                break;
                
            case SENS_RSP:
            	//Sensitivity sensitivity = Sensitivity.values()[((this.packet[1] >> 4) & 0x07)];/
                break;
                
            case PC_RSP:
                //int count = ((this.packet[1] & 0x7f) << 25) | ((this.packet[2] & 0x7f) << 18) | ((this.packet[3] & 0x7f) << 11) | ((this.packet[4] & 0x7f) << 4) | ((this.packet[5] & 0x7f) >> 3);
                break;
                
            case PDT_RSP:
                //int timeout = (this.packet[1] & 0x7f); 
                break;
                
            case HV_RSP:
                //int hVersion = (this.packet[1] & 0x7f);
                break;
                
            case TCT_RSP:
                /*int TCT = (((this.packet[1] & 0x7f) << 1) | ((this.packet[2] >> 6) & 0x01));
                int REPS = (((this.packet[2] & 0x3f) << 2) | ((this.packet[3] >> 5) & 0x03));
                int LAST = (((this.packet[3] & 0x1f) << 3) | ((this.packet[4] >> 4) & 0x07));*/
                //System.out.println("TCT:" + TCT+ "REPS: " + REPS + "LAST: " + LAST);
                break;
                
            case TM_RSP:
                //int radioTransmissionMode = (this.packet[1] >> 4) & 0x07;
                //System.out.println("Radio Transmission Mode: "+ radioTransmissionMode);
                break;
                
            default:
                break;
        }         
    }    
 
   
    //------------------------getFileName------------------------
    private String getFileNameForCurrentHour(String path, Date time, final String ID){
		File dir = new File(path);
		String[] files = dir.list(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String filename) {
				if ((filename.endsWith(".csv")) && (filename.contains("Wocket." + ID)))
					return true;
				else 
					return false;
			}
		});
		if(files == null || files.length == 0) {			
	        String filename = "Wocket." + ID + ".ACTIVITY_COUNT." + fileNameFormat.format(time) + ".csv";
			return filename;
		}
		else 
			return files[0];
	}    
    
    //------------------------Write in file------------------------
    public static void writeInFile(String path, String fileName, String label, String msg) {	   	
    	
    	File outputDir = new File(path);
    	if(!outputDir.isDirectory()){
    		outputDir.mkdirs();
		}    	
        File f = new File(fileName);
        PrintWriter out = null;
        try {
            if (!(f.exists())) {
                    f.createNewFile();                    
                    out = new PrintWriter(new FileWriter(fileName));
                    if (label != null)
                    	out.append(label + "\n");
            } else
                    out = new PrintWriter(new FileWriter(fileName, true));
            out.append(msg + "\n");
	    } catch (IOException e1) {
	    	 WocketLogger.logErrorMessage("IOException while writing in" + fileName);              
	    } catch (NumberFormatException e) {
	         WocketLogger.logErrorMessage("NumberFormatException while writing in" + fileName);
	    }               

        if (out != null) {
                out.close();
        }		
	} 
     
      
   
}

