
package communication;


import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Vector; 
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import bafWocket.RawDataFileHandler;
import classification.TimSampleSensor;
import com.intel.bluetooth.BlueCoveImpl;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import wockets.data.Acc;
import wockets.data.ParticipantData;
import wockets.data.SWData;
import wockets.data.WocketParam;


public class PcClient implements DiscoveryListener{
   
    
    private static Object lock = new Object(); //object used for waiting   
    public static Vector<RemoteDevice> vecDevices = new Vector<RemoteDevice>();    //vector containing the devices discovered
    private static String connectionURL = null;
    private static PcClient client = new PcClient();
    private static LocalDevice localDevice;
    private static DiscoveryAgent agent;
    private static boolean stopFlag = false;    
    private static boolean readyToSend = false;
    private static int sendTrials = 0;
    private static Date lastRun = null;
    private static int prevHour = -1;
    static TimeSeries ts = new TimeSeries("Activity Summary data");  
		
    //*********************************Wocket Commands***************************************************************
    public final static byte[] WOCKET_60_SEC_BURST_PACKET = {(byte) 0xBA, (byte)0x20};
    public final static byte[] WOCKET_Continuous_PACKET = {(byte) 0xBA, (byte)0x00};
    public final static byte[] WOCKET_GET_WTM_PACKET = {(byte) 0xBE};
    public final static byte[] WOCKET_GET_BATTERY_LEVEL = {(byte) 0xA0};
    public final static byte[] WOCKET_BATTERY_CALIBRATION_PACKET = {(byte) 0xB5, 0,0,0,0,0,0,0,0,0};
    public final static byte[] WOCKET_SET_LED_PACKET = {(byte) 0xBC, (byte)0x02}; //Yellow_ LED on for 2 Seconds    
    public final static byte[] WOCKET_SET_TCT_PACKET = {(byte) 0xB9, (byte)0x1E, (byte) 0x80, (byte)0x1F, (byte)0x70};
    //****************************************************************************************************************
    
    //****************************Find Devices***********************************
    public static void findDevices()throws IOException {    	
        
        localDevice = LocalDevice.getLocalDevice(); //display local device address and name       
        agent = localDevice.getDiscoveryAgent(); //find devices 
        agent.startInquiry(DiscoveryAgent.GIAC, client);       
        try {
            synchronized(lock){
                lock.wait();
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }              
        
        int deviceCount=vecDevices.size();//print all devices in vecDevices
       
        if(deviceCount <= 0){
        	WocketLogger.logErrorMessage("No Devices Found.");
            System.exit(0);
        }        
    }    
       
    //****************************Connect****************************************
    public static StreamConnection connect(RemoteDevice remoteDevice) {
    	
    	StreamConnection streamConnection = null;  
    	//check for spp service        
        UUID[] uuidSet = new UUID[1];
        uuidSet[0] = new UUID("1101",true);
        int[] attrSet = {0x1101};         
        try {      
        	agent.searchServices(attrSet,uuidSet,remoteDevice,client);
        } catch (IOException e) {
    		e.printStackTrace();
        	WocketLogger.logErrorMessage("IOException for agent.searchServices" + e);
    	}
        try {
            synchronized(lock){
                lock.wait();
            }
        }
        catch (InterruptedException e) {
        	WocketLogger.logErrorMessage("InterruptedException in connct." + e);
        }
        if(connectionURL == null){
            WocketLogger.logErrorMessage("Couldn't get Connection URL.");
            return null;
        }
        try {
        	//connect to the Wocket (as the server) 
        	streamConnection = (StreamConnection)Connector.open(connectionURL);
        } catch (Exception e) {
        	e.printStackTrace();
        	WocketLogger.logErrorMessage("Couldn't get SreamConnction." + e);
        	streamConnection = null;
    	} 
        
        return streamConnection;
    } 
    
  //*********************methods of DiscoveryListener************************
    @Override
    public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
    	
        //add the device to the vector
        if(!vecDevices.contains(btDevice)) {
            vecDevices.addElement(btDevice);            
        }
    }
    //------------------------------------------------------------------------
    @Override
    public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
    	if(servRecord != null && servRecord.length > 0){
            connectionURL = servRecord[0].getConnectionURL(0,false);            
        }
        synchronized(lock){
            lock.notify();
        }
    }
    //------------------------------------------------------------------------
    public void serviceSearchCompleted(int transID, int respCode) {
        synchronized(lock){
            lock.notify();
        }
    }
    //------------------------------------------------------------------------
    public void inquiryCompleted(int discType) {
        synchronized(lock){
            lock.notify();
        }
    }
              
    //****************************collecting Data******************************    
    public static void collectData(ParticipantData pData, ArrayList<RemoteDevice> remoteDevice, JTextArea textArea) {
    	
    	int MAX_SIZE = 40000;       
        WocketDecoder myDecoder = new WocketDecoder(); 
    	int num = remoteDevice.size();
    	int[] numberReadBytes = new int[num];
    	byte[][] wocketData = new byte[num][MAX_SIZE];
		int[] seqNum = new int[num]; 
		int[] failedConnection = new int[num];
		long[] lastConnectionTime = new long[num];
		int[] succesfulConnections = new int[num];
		Calendar[] receivedTime = new Calendar[num];
		WocketParam[] wocketParam = new WocketParam[num];		
		String[] macID = new String[num];
		int[] delay = new int[num];
		long[] millis = new long[num];
		boolean[] complete = new boolean[num];
        boolean[] decodeFlag = new boolean[num];        
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    	long sTime0;
    	long sTime1;
    	int wastedData;
    	StreamConnection streamConnection;
		OutputStream outStream;
        InputStream inStream;
        byte[] data = {(byte) -1};
        int rcvPackets = 0;
        int rawPercentage = 0;
        Date timeStamp;
        String msg;
        TimSampleSensor toServer = null;
        int statusLength = 0;
        String statusStart = "";
        ArrayList<SWData> sleepData = new ArrayList<SWData>();
        ArrayList<SWData> wakeData = new ArrayList<SWData>();
		
		//-------------Initialization-----------
		for (int j=0; j < num; j++){
			seqNum[j] = -1;
			wocketParam[j] = new WocketParam();
			wocketParam[j].id = "0"+ j;	
			wocketParam[j].last_seqNumber = -1;
			failedConnection[j] = 0;
			lastConnectionTime[j] = 0;
			succesfulConnections[j] = 0;
			macID[j] = remoteDevice.get(j).getBluetoothAddress();
		}
		WocketLogger.setLogPath(pData.getpID());		
		
		new ShowStatsRunnable().run();
		
		try {
			toServer = new TimSampleSensor();			
		} catch (Exception e) {
			System.out.println("Error in connecting to server" + e);
		}
	   
	    while (!stopFlag) { 	   
	    	
	    	//---------------send data to server-----------   	
	    	//compressANDTransferZipFiles(Calendar.getInstance().getTime(), pData.getpID());	    	
	    	//---------------------------------------------	    	 
	    	for (int cnt = 0; cnt < num; cnt++) {
	    		
	    		streamConnection = null;
    			outStream = null;
                inStream = null;
                
	    		try {
		    		millis[cnt] = System.currentTimeMillis(); 
		    		delay[cnt] = (int) (millis[cnt] - lastConnectionTime[cnt]);
		    		if ( (lastConnectionTime[cnt] == 0) || (delay[cnt] > 60000) ) {
		                complete[cnt] = false;
		                decodeFlag[cnt] = false;
		    			
		    			//--------------connect to Wocket----------------------	
			        	streamConnection = connect(remoteDevice.get(cnt));        	
			        	if (streamConnection == null) {	
		                     beepRunnable.run();
			                 failedConnection[cnt]++;
			                 textArea.append("Failed to connect to Wocket-" + macID[cnt].substring(8) + "\n");
			                 if (wocketParam[cnt].prevLabel != "Absent") {
									/*try {
										toServer.sendEvent(toServer.makeEvent(2));//Absent
									} catch (Exception e) {
										e.printStackTrace();
										System.out.println("Error in sending message to server");
									}*/
									wocketParam[cnt].prevLabel = "Absent";
					            }	
			                 continue;
			        	} 
			        	receivedTime[cnt] = Calendar.getInstance();
		                textArea.append("Connect to Wocket-" + macID[cnt].substring(8) + "\n");		                
		                
		                outStream = streamConnection.openOutputStream();
		                inStream = streamConnection.openInputStream();	
		                
		                if (outStream != null) {
				            outStream.write(WOCKET_60_SEC_BURST_PACKET);
				            outStream.write(WOCKET_60_SEC_BURST_PACKET);
				            //Commands.call(CommandTypes.GetFirmwareVersion, outStream);
		                } else
		                	WocketLogger.logErrorMessage(wocketParam[cnt].id + "outStream is null");
			
			            //--------------send an ack----------------------			            
			            sendActivityCountAck(seqNum[cnt], outStream );	
			            
						//---------------read data----------------------	
						sTime0 = System.currentTimeMillis(); 
			            numberReadBytes[cnt] = 0;
			            data[0] = -1; 
			            wastedData = 0;
			            
			            if (inStream != null) {
				            data[0] = (byte) inStream.read();
				            while ((data[0] == -1) && (wastedData < 100) ) { //to ignore 100 packets of 0xff which Wocket sends at the beginning of a connection			            	
				            	wastedData ++;
				            	data[0] = (byte) inStream.read();
				            }				            
				            sTime1 = System.currentTimeMillis(); 
				            while ((sTime1 - sTime0) < 10000){
				            	if (data[0] == -46){
				            		complete[cnt] = true;
				            		break;
				            	}
				            	if (data[0] != -1){
				            		wocketData[cnt][numberReadBytes[cnt]] = (byte) data[0];
					    			numberReadBytes[cnt] ++;
					    			decodeFlag[cnt] = true;
				            	}
				    			data[0] = (byte) inStream.read();
				    			sTime1 = System.currentTimeMillis();			            			            	
				            }	
			            } else
		                	WocketLogger.logErrorMessage(wocketParam[cnt].id + "inStream is null");			            
	    	    		    	    		
	    	    		lastConnectionTime[cnt] = millis[cnt];
		    		}//if
		    		
		    		if (decodeFlag[cnt] == true) {
		            	rcvPackets = 0;
			            rawPercentage = 0;
			            msg = null;
			            decodeFlag[cnt] = false;
			            if (numberReadBytes[cnt] < 13180) { //13170 is the max number of bytes a Wocket can save
			            	
				            //---------------decoding data----------------------	
				            myDecoder.Decode(wocketData[cnt], numberReadBytes[cnt], wocketParam[cnt], macID[cnt], receivedTime[cnt], pData.getpID());		            	
			    	    	rcvPackets = wocketParam[cnt].uncompressedCount + wocketParam[cnt].compressedCount;
			    	    	if (wocketParam[cnt].batchCount != 0)
			    	    		rawPercentage = (int)(rcvPackets * 100 / wocketParam[cnt].batchCount);
			    	    	seqNum[cnt] = (int)wocketParam[cnt].last_seqNumber;			    	    	
			    	    	
			    	    	if (cnt == 0) {
			    	    		//--------------visualize accs------------------
				    	    	for (Acc acc: wocketParam[cnt].accs) {
					    	    	Minute minute = new Minute(acc.getTimeStamp());
				    	    		ts.addOrUpdate(minute, acc.getValue());
				    	    	}
				    	    	textArea.append("Status: " +  wocketParam[cnt].label + "\n");
				    	    	
				    	    	SWData swData = wocketParam[cnt].swData;
				    	    	if (swData != null) {
				    	    		if (swData.getLength() >= 5) {
					    	    		if (swData.getStatus().equals("Sleep")) {
					    	    			sleepData.add(swData);
					    	    		} else {
					    	    			wakeData.add(swData);
					    	    		}					    	    		
				    	    		}
				    	    		/*String outputFilePath = "c:/sleepStudy/" + pData.getpID() + "/sensor/control/";
				    	            String fileName = outputFilePath + "Wocket_" + wocketParam[cnt].id + "_SW.csv";
				    	            msg = swData.getStartTime() + "," + swData.getStatus() + "," + swData.getLength();
				    	            WocketDecoder.writeInFile(outputFilePath, fileName, "", msg);*/
				    	    	}				    	    		
			    	    	
				    	    	//---------------send to CIMIT server-----------
				    	    	if (wocketParam[cnt].label != wocketParam[cnt].prevLabel) {
									/*try {
										if (wocketParam[cnt].prevLabel.equals("Absent"))
											toServer.sendEvent(toServer.makeEvent(3)); //Present
										toServer.sendEvent(toServer.makeEvent(wocketParam[cnt].label.equals("Sleep") ? 0 : 1));
									} catch (Exception e) {
										e.printStackTrace();
										System.out.println("Error in sending message to server");
									}*/
				    	    		
									wocketParam[cnt].prevLabel = wocketParam[cnt].label;
					            } 
			    	    	}
				            	
			    	    	//-----------------log------------------------------
			    	    	timeStamp = receivedTime[cnt].getTime();
			    	    	msg = timeFormat.format(timeStamp) + "," +  millis[cnt] + "," + (delay[cnt] - 60000) + "," + (failedConnection[cnt] + 1) + "," + rcvPackets + "," +  
				            wocketParam[cnt].batchCount + "," + rawPercentage + "," + numberReadBytes[cnt]  + "," + complete[cnt] + "," + wocketParam[cnt].batteryLevel;
			    	    	WocketLogger.log(dateFormat.format(millis[cnt]), Integer.toString(cnt), msg);
			    			textArea.append(timeFormat.format(timeStamp) + "\n");
			    	    	textArea.append("Battery Level: " + ((double)((wocketParam[cnt].batteryLevel - 570) * 100 / (720 - 570))) + "%\n");
		                	if (wocketParam[cnt].batteryLevel < 600) {
			                	textArea.append("The Battery level of this Wocket is low. Please swap the Wockets.\n");
			                    PcClient.beepRunnable.run();
			                    PcClient.beepRunnable.run();
			                }
		                	
			            } else {
			            	WocketLogger.logErrorMessage(wocketParam[cnt].id + numberReadBytes[cnt] + "bytes received!!!");	
			            	timeStamp = receivedTime[cnt].getTime();
			    	    	msg = timeFormat.format(timeStamp) + "," +  millis[cnt] + "," + (delay[cnt] - 60000) + "," + (failedConnection[cnt] + 1) + "," + rcvPackets + "," +  
				            "---" + "," + rawPercentage + "," + numberReadBytes[cnt]  + "," + complete[cnt] + "," + "---";
			    	    	WocketLogger.log(dateFormat.format(millis[cnt]), Integer.toString(cnt), msg);
			    	    	textArea.append("Received data packets with error\n\n");
			            }
			            
	                    textArea.update(textArea.getGraphics());
	                    failedConnection[cnt] = 0;
		        	} 
	    		
	    		} catch (Exception ex) {
		    		WocketLogger.logErrorMessage(wocketParam[cnt].id + "Exception in collectData " + ex);
		    	} finally {
		    		try {
    	    			if (inStream != null) 
    	    				inStream.close();
    	    			if (outStream != null) 
    	    				outStream.close();
    	    			if (streamConnection != null) 
    	    					streamConnection.close();
					} catch (IOException e) {	
						WocketLogger.logErrorMessage(wocketParam[cnt].id + "IOException for closing streams" + e);
					}		    		
	        		connectionURL = null;
    	    		BlueCoveImpl.shutdown();			    	
		    	}
	    	}//for    		    	
	    	
	    } //while
    }
    
    //*********************************sendAck***********************************  
    public static void sendActivityCountAck(int seqNumber, OutputStream out) {
	    byte[]_Bytes = new byte[4];
		int param = seqNumber;
		byte temp = (byte)(param >> 8); 
		_Bytes[0] = (byte)0xBB;
		_Bytes[1] = (byte)((byte)(temp >>> 1) & 0x7f);
		_Bytes[2] = (byte)((byte)(temp << 6) & 0x40);
		temp = (byte)(param);
		_Bytes[2] |= (byte)((byte)(temp >> 2) & 0x3f);
		_Bytes[3] = (byte) ((byte)(temp << 5) & 0x60);
		for (int m=0; m<_Bytes.length; m++){
			try {
				out.write(_Bytes[m]);
			} catch (IOException e1) {
				
				e1.printStackTrace();
			}			
			try {
			Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
    }
    
    //*********************************close***********************************    
    public static void tryClose(Closeable c) {
        try {
        	if (c !=null)
        		c.close();
		} catch (IOException e) {
			WocketLogger.logErrorMessage("IOException for closing in/outstream" + e);
		}
    }
    
    //*********************************round***********************************    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    
    //******************************Stop***************************************
    public static void stop() {
    	BlueCoveImpl.shutdown();
        BlueCoveImpl.shutdownThreadBluetoothStack();
        stopFlag = true;
    }
    //******************************Start***************************************
    public static void start() {
        stopFlag = false;
    }
    
    //******************************play sound*********************************
    public static final Runnable beepRunnable = new Runnable() {
        public void run() {                 
            try {
                java.applet.AudioClip clip = java.applet.Applet.newAudioClip(new java.net.URL("file:src/rsc/beep.wav"));
                clip.play();
            } catch (java.net.MalformedURLException murle) {
            WocketLogger.logErrorMessage("Exception in beepRunnable"+ murle);
            }
        }            
    };      
    
  //******************************show Stats*********************************
    public static class ShowStatsRunnable implements Runnable {
    	
		public void run() {
			try {
				TimeSeriesCollection dataset = new TimeSeriesCollection(ts);
				JFreeChart chart = ChartFactory.createTimeSeriesChart(
						"GraphTest", "Time", "Value", dataset, true, true, false);
				JFrame frame = new JFrame("Graph");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				ChartPanel label = new ChartPanel(chart);
				frame.getContentPane().add(label);
				// Suppose I add combo boxes and buttons here later

				frame.pack();
				frame.setVisible(true);
			} catch (Exception e) {
				WocketLogger.logErrorMessage("Exception in showStatsRunnable" + e);
			}
		} 
    };    

    //**********************Send BAF and AC data to the server************************        
	public static void compressANDTransferZipFiles(Date now, String subID) {
		if(lastRun == null){			
	        lastRun = now;	        
		}
		RawDataFileHandler rawDataFileHandler = new RawDataFileHandler();
		//JsonDataFileHandler jsonDataFileHandler = new JsonDataFileHandler(); 
		int hour = now.getHours();
		//GregorianCalendar gc = new GregorianCalendar();
		Calendar gc = Calendar.getInstance();
		gc.setTime(now);
		gc.get(Calendar.HOUR_OF_DAY);
		//gc.get(GregorianCalendar.HOUR_OF_DAY);
		if (prevHour == -1)
			prevHour = hour;
		if ((hour > prevHour) || ((hour == 0) && (prevHour == 23)) ) {
			rawDataFileHandler.zipRawData(lastRun, subID);
			readyToSend = true;
			prevHour = hour;
			lastRun = now;
		}
		if (readyToSend){
			if (rawDataFileHandler.transmitRawData(subID)) {
				//System.out.println("data files are transmitted to server successfully.");
				readyToSend = false;
		        sendTrials = 0;
			} else
				sendTrials++;
			if (sendTrials >= 5){ //five trials per hour
				readyToSend = false;
		        sendTrials = 0;
			}
		}
		/*		 RawDataFileHandler rawDataFileHandler = new RawDataFileHandler();
		//JsonDataFileHandler jsonDataFileHandler = new JsonDataFileHandler(); 
		int hour = now.getHours(); 
		if (prevHour == -1)
			prevHour = hour;
		if (hour > prevHour) {
			isTransmitted = false;
		}
		if ((hour > prevHour)&&(!isTransmitted)){			
			rawDataFileHandler.zipRawData(lastRun, subID);
			if (rawDataFileHandler.transmitRawData(subID)) {
				System.out.println("raw data files are transmitted successfully.");
				isTransmitted = true;
				prevHour = hour;
				Calendar logDt = Calendar.getInstance();
		        lastRun = logDt.getTime(); 
			}
			
		}*/
	}
         
}