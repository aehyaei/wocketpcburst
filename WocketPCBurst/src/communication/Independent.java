package communication;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import bafWocket.BafDecoder;



public class Independent {
	
	//*******************************main**********************
    public static void main(String[] args) throws IOException {
       
    	/*findDevices();
    	
    	System.out.println("Bluetooth Devices: " + vecDevices.size());
    	int btSize = vecDevices.size();
    	for (int i = 0; i < btSize; i++) {
        	RemoteDevice remoteDevice=(RemoteDevice)vecDevices.elementAt(i);
        	String adr= remoteDevice.getBluetoothAddress();            
            if (adr.contains("0006660")){
            	System.out.println((i+1)+". "+ "Wocket-"+adr.substring(8) + " "+ remoteDevice.getBluetoothAddress());
            }else{
                vecDevices.removeElementAt(i);
                i--;
                btSize--;
            }
        }

        System.out.print("Choose the Device index you want to calibrate: ");
        
        BufferedReader bReader=new BufferedReader(new InputStreamReader(System.in)); 
    	String chosenIndex=bReader.readLine();
        int index=Integer.parseInt(chosenIndex.trim());
        StreamConnection streamConnection = connect(index-1);
        OutputStream outStream = streamConnection.openOutputStream(); 
        InputStream inStream = streamConnection.openInputStream();
        
        outStream.write(WOCKET_Continuous_PACKET);
        
        //System.out.println("start testing");
        //measureSamplingRate(outStream, inStream, 600);
    	//set_sr(195, outStream);
    	//Calibrate_SamplingRate(40, inStream,  outStream); 
    	//test_Axes('x', outStream, inStream);
        //measure_range('x', outStream, inStream);
        //measure_range('y', outStream, inStream);
        //measure_range('z', outStream, inStream);       	
        //testBattery(outStream,inStream);
        //CalibrateBattery(outStream);*/
        //---------------------- Calling BafDecoder --------------------------
    	SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    	Date startDate=null, endDate=null;
		try {
			startDate = dayFormat.parse("2012-12-12 07:45");
			endDate = dayFormat.parse("2012-12-12 09:00");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BafDecoder bafDecoder = new BafDecoder();
    	bafDecoder.decodeAndSaveDataWithStream( startDate, endDate,  "00");
    	//bafDecoder.decodeAndSaveDataWithStream( startDate, endDate,  "01");
    	//--------------------------------------------------------------------
       
    }//main
}
