package communication;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class WocketLogger {
	private static String localPath;
			
	public static void setLogPath(String subID){
		localPath = "c:/sleepStudy/" + subID + "/sensor/log/";
	}
	
	public static void log(String date, String id, String msg)
	{		
    	//String outputFilePath = "c:/sleepStudy/" + date +"/log/";
    	String name = "Wocket_" + id + "_" + date +"_log.csv";
    	File outputDir = new File(localPath);
    	if(!outputDir.isDirectory()){
    		outputDir.mkdirs();
		}
        String filename = localPath+name;
        File f = new File(filename);
        PrintWriter out = null;
        try {
                if (!(f.exists())) {
                        f.createNewFile();                    
                        out = new PrintWriter(new FileWriter(filename));
                        out.append("TIME_STAMP,MSEC,DELAY,TRIALS,RAW_RECEIVED,RAW_SENT,RAW(%),TOTAL_READ, END_OF_BATCH,BATTERY,\n");
                } else
                        out = new PrintWriter(new FileWriter(filename,true));
        } catch (IOException e1) {
                e1.printStackTrace();
        }        
        String s = msg + "\n";
        try {
                out.append(s);
        } catch (NumberFormatException e) {
                System.out.println("Error while writing log" + e);
        }

        if (out != null) {
                out.close();
        }		
	}
	
	public static void logErrorMessage(String aMsg) {
				
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss.SSS");
    	Date timeStamp = Calendar.getInstance().getTime();
    	String name = "errorLog.csv";
    	File outputDir = new File(localPath);
    	if(!outputDir.isDirectory()){
    		outputDir.mkdirs();
		}
        String filename = localPath+name;
        File f = new File(filename);
        PrintWriter out = null;
        try {
                if (!(f.exists())) {
                        f.createNewFile();                    
                        out = new PrintWriter(new FileWriter(filename));
                } else
                        out = new PrintWriter(new FileWriter(filename,true));
        } catch (IOException e1) {
                e1.printStackTrace();
        }        
        String s = timeFormat.format(timeStamp) +  "  " + aMsg + "\n";
        try {
                out.append(s);
        } catch (NumberFormatException e) {
                System.out.println("Error while writing log" + e);
        }

        if (out != null) {
                out.close();
        }		
	}

}
