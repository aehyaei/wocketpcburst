package bafActigraph;

import java.text.DecimalFormat;

public class RangeManager 
{
	
	public int[] Float2Int(float x, float y, float z)
	{
		int[] value = new int[3];
		value[0] = ReRange(x);
		value[1] = ReRange(y);
		value[2] = ReRange(z);
		return value;
	}

	private int ReRange(float floatValue)
	{
		floatValue+=6;
		floatValue*=1000;
		int value = (int) floatValue;
		return value;
	}
	
	public double Int2Float(double intValue)
	{
		DecimalFormat df = new DecimalFormat("#.###");
		Double floatValue =(intValue/1000.0);
		floatValue-=6;
		return Double.valueOf(df.format(floatValue));
	}

}
