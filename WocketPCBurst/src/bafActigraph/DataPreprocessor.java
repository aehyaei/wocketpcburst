package bafActigraph;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import ActiBaf.BafEncoder;
//import bafFloat.BafEncoder;

public class DataPreprocessor 
{
	private static BufferedOutputStream fos;
	private static File currentHour ;
	private static Calendar tStamp=Calendar.getInstance();
	private static final Calendar tCheck = Calendar.getInstance();
	private static int timeOffset;
	private static String sensor;
	private static int SamplingRate=0;
	private static String date;
	private static String startTime;
	private static int offset;
	private static String localPath;  
	
	public DataPreprocessor()
	{}
	
	public DataPreprocessor(int offset)
	{
		this.offset = offset;
	}
	
	
	public void Preprocessor(String fileName, String workSpace)
	{
		this.localPath = workSpace;
		
		try
		{
			FileInputStream fstream = new FileInputStream(fileName);
	// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			int iterator=1;
	//Read File Line By Line		
			while ((strLine = br.readLine()) != null)
			{
				if(iterator<=10)
				{
					switch(iterator)
					{
					case 1:
						SamplingRate = Integer.parseInt(DatXtrct(strLine,"at (\\d+) Hz",1));
						timeOffset=(1000/SamplingRate);
						break;
					case 2:
						String [] sName = strLine.split(" ");
						sensor=sName[2];
					case 3:
						startTime = DatXtrct(strLine,"[0-1]\\d[:][0-5]\\d[:][0-5]\\d",0);
						break;					
					case 4:
						date = DatXtrct(strLine,"\\d+/\\d+/\\d{4}",0);
						break;
					
					default :
						break;
					}
				}
				else
				{
					if(tStamp.equals(tCheck))
					{
						Calendar cal = Calendar.getInstance();
					    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyy HH:mm:ss");
					    cal.setTime(sdf.parse(date+" "+startTime));// all done
					    tStamp = cal;
					    cal.setTimeInMillis(tStamp.getTimeInMillis() + timeOffset + offset);
					    File outputFile = GenerateOutputFile(cal);
						if(outputFile.exists())
							outputFile.delete();
						outputFile.createNewFile();
						currentHour = outputFile;
						fos = new BufferedOutputStream(new FileOutputStream(outputFile,true));
					}					
					String [] coord = strLine.split(",");
					Float fx = Float.parseFloat(coord[0]);
					Float fy = Float.parseFloat(coord[1]);
					Float fz = Float.parseFloat(coord[2]);
					Calendar test = Calendar.getInstance();
					
					tStamp.setTimeInMillis(tStamp.getTimeInMillis() + timeOffset + offset);
					File outputFile = GenerateOutputFile(tStamp);
					
					
					
					
					if (!currentHour.equals(outputFile) )
					{
						fos.flush();
						fos.close();
						
						if(outputFile.exists())
							outputFile.delete();
						outputFile.createNewFile();
						currentHour = outputFile;
						fos = new BufferedOutputStream(new FileOutputStream(outputFile,true));
					}
						
					RangeManager rm = new RangeManager();
					int[] value  = rm.Float2Int(fx, fy, fz);
					BafEncoder bf= new BafEncoder();
					
					fos.write(bf.encodeAndSaveData(tStamp));
					fos.write(bf.encodeAndSaveData(value[0], value[1], value[2]));
					
					
					//bf.encodeAndSaveData(tStamp, value[0], value[1], value[2], sensor);
				}
				
				iterator++;
			}	
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
				
	}
	
	public String DatXtrct(String lineData, String pattern, int group)
	{
		Pattern ratePattern = Pattern.compile(pattern);
		Matcher pttrnMatcher = ratePattern.matcher(lineData);
		String smpRate="";
		while (pttrnMatcher.find())
		{
			smpRate = pttrnMatcher.group(group);
		}
		return smpRate;
	}
	
	private File GenerateOutputFile(Calendar time)
	{
		String outputFilePath = getFilePathByTime(time);
		boolean isNewFile = false;
		File outputDir = new File(outputFilePath);
		if(!outputDir.isDirectory()){
			outputDir.mkdirs();
		}
		String outputFileName = getFileNameForCurrentHour(outputFilePath, time, sensor);
		File outputFile = new File(outputFilePath,outputFileName);
		if(!outputFile.exists())
		{			
			isNewFile = true;
		}
		if(isNewFile)
		{
			BafEncoder.resetPrevData();
		}
		
		return outputFile;
		
	}
	
	/*private BufferedOutputStream GenerateStream(File outputFile)
	{
		BufferedOutputStream outputStream = null;
			outputFile.createNewFile();
			outputStream = new BufferedOutputStream(new FileOutputStream(outputFile,true));

			
			
		}
		finally
		{
			
		}
		
		
		
		
		return outputStream;
	}*/
	
	/**
	 * Gets the path for the raw data file by day and hour.
	 *
	 * @return the path by time
	 */
	String getFilePathByTime(Calendar time)
	{
		Date timeStamp = time.getTime();
		SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat hour = new SimpleDateFormat("HH");

		String path = localPath +day.format(timeStamp)+"/"+hour.format(timeStamp)+"/";
		return path;
	}
	
	/**
	 * Checks the file name for current hour. If there is a file exists, return it; if not, generate the file name by the time. 
	 *
	 * @param path the path
	 * @return the file name for current hour
	 */
	private String getFileNameForCurrentHour(String path, Calendar time, final String ID){
		File dir = new File(path);
		String[] files = dir.list(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String filename) {
				// TODO Auto-generated method stub
				if ((filename.endsWith(".baf"))&&(filename.contains("Wocket."+ID)))
					return true;//.contains("Wocket."+ID);
				else 
					return false;
			}
		});
		if(files == null || files.length == 0)
			return generateFileNameByTime(time, ID);
		else
			return files[0];
	}
	
	/**
	 * Generate the raw file name by time. ID is assigned by default.
	 *
	 * @return the string
	 */
	private String generateFileNameByTime(Calendar time, String ID)
	{
		Date timeStamp = time.getTime();
		SimpleDateFormat detailedTime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
		String fileName="Wocket."+ID+"."+detailedTime.format(timeStamp)+".baf";		
		return fileName;
	}
}
