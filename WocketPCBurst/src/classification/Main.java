package classification;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import wockets.data.SWData;

public class Main {
	public static void main(String[] args) {
		
		String path = "C:/Users/Aida/Data/Aida Wocket Test/";
		String inFileName = "Wocket.00066606D35D.2016-01-22-14-36-13-265.baf";
		String outFileName = "WocketRaw.csv";
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date startTimeInDate = sdf.parse("2016-01-22 14:29");
		Date endTimeInDate = sdf.parse("2016-01-22 15:00");
		long startTimeInMillis = startTimeInDate.getTime();
		long endTimeInMillis = endTimeInDate.getTime();*/
		BufferedOutputStream outputStream = null;
		try {
			outputStream = new BufferedOutputStream(new FileOutputStream(new File(path + outFileName),true));
			bafWocket.BafDecoder bd = new bafWocket.BafDecoder();
			//outputStream = (BufferedOutputStream) bd.decodeBytesWithinHour(inFileName, startTimeInMillis, endTimeInMillis, outputStream);
			BufferedOutputStream bf = (BufferedOutputStream) bd.decodeBytesForWholeFile(path + inFileName, outputStream);
			bf.flush();
			bf.close();
			outputStream.close();
			System.out.println("Finish decoding baf file.");
		} catch (IOException e){
			System.out.println("Error in file I/O :"+e.toString());
		} 	
		
		
		//dbProducer.createDBs(); 
		
		//read psg files
		//checkSWDuration();
		//getSWSegments();
		
		
		//---------------------------------------------------------------------------------------------
		//Converter.csv2Xml("C:/Users/Aida/Desktop/717/annotations/", "Wocket_00_sazonov_annotation.csv");
		/*SimpleDateFormat parser=new SimpleDateFormat("M/dd/yyyy H:mm:ss");
		Date sleepStartTime = null;
		Date wakeStartTime = null;
		ArrayList<SWData> wakeData = new ArrayList<SWData>();
		try {
			sleepStartTime = parser.parse("7/10/2013  1:12:39");
			wakeStartTime = parser.parse("7/10/2013  7:41:22");
			wakeData.add(new SWData("Wake", parser.parse("7/10/2013 2:25:51"), 10));
			wakeData.add(new SWData("Wake", parser.parse("7/10/2013 3:39:00"), 1));
			wakeData.add(new SWData("Wake", parser.parse("7/10/2013 4:2:44"), 1));
			wakeData.add(new SWData("Wake", parser.parse("7/10/2013 4:37:21"), 3));
			wakeData.add(new SWData("Wake", parser.parse("7/10/2013 4:45:16"), 1));
			wakeData.add(new SWData("Wake", parser.parse("7/10/2013 5:37:42"), 1));
			wakeData.add(new SWData("Wake", parser.parse("7/10/2013 5:40:49"), 10));
			wakeData.add(new SWData("Wake", parser.parse("7/10/2013 6:28:51"), 1));
			wakeData.add(new SWData("Wake", parser.parse("7/10/2013 6:58:49"), 12));
			wakeData.add(new SWData("Wake", parser.parse("7/10/2013 7:28:30"), 13));
			//wakeData.add(new SWData("Wake", parser.parse("7/10/2013 7:34:26"), 1));
			//wakeData.add(new SWData("Wake", parser.parse("7/10/2013 7:36:25"), 1));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int size = wakeData.size();
    	if (size == 0) 
    		System.out.println("No considerable awake time during last night!");
    	else {
			for (int m = size - 1; m > 0; m--) {
				if (Math.round((wakeData.get(m).getStartTime().getTime() - wakeData.get(m - 1).getStartTime().getTime())/60000) < (10 + wakeData.get(m - 1).getLength())) {	
				//if ((wakeData.get(m).getStartTime().getMinutes() - wakeData.get(m - 1).getStartTime().getMinutes()) < (10 + wakeData.get(m - 1).getLength())) {
					wakeData.get(m - 1).setLength(wakeData.get(m - 1).getLength() + wakeData.get(m).getLength());
					wakeData.remove(m);
        		} 
			}				            			
    	}
    	size = wakeData.size();
    	if (size > 0) {
    		for (int m = size - 1; m >= 0; m--) {
    			System.out.print(wakeData.get(m).getStartTime() + "," + wakeData.get(m).getLength());
    			if ( (wakeData.get(m).getLength() < 5) || (wakeData.get(m).getStartTime().getTime() - sleepStartTime.getTime() < 1800000) || (wakeStartTime.getTime() - wakeData.get(m).getStartTime().getTime() < 1800000) )  {
    				wakeData.remove(m);
    				System.out.print(" removed");
    			}
    			System.out.println();
    		}
    	}
    	size = wakeData.size();
    	if (size > 0) {
    		//find the 3 longest awakening time 
    		for (int m = 0; m < Math.min(3, size); m++) {			                	
        		for (int n = m + 1; n < size; n++) {
        			if (wakeData.get(m).getLength() < wakeData.get(n).getLength()) {
        				SWData sw = wakeData.get(m);
        				wakeData.set(m, wakeData.get(n));
        				wakeData.set(n, sw);
        			}
        		}
        		System.out.println(wakeData.get(m).getStartTime() + "," + "Q" + m + "_time, " + wakeData.get(m).getLength() + " min");
                
    		}
    	} */
		
		
		
		//String localPath = "c:/sleepStudy/"; //Windows 
		//String localPath = "/Users/aidaehyaei/Documents/sleepStudy/"; //MAC
		//NumberFormat numFormat = new DecimalFormat("00");
		
		//for (int j = 12; j < 13; j++) {//subject number
			
			/*String subId = numFormat.format(2); 
	    	String bafPath = localPath + "HB"+ subId + "/" + subId + "/sensor/";
			String inPath  = localPath + "HB"+ subId + "/";
	    	String outPath = localPath + "test/HB" + subId +"/";*/	    	
	    				
			//------------------------------------------------------
	    	//DataManager dataManager = new DataManager();
	    	//dataManager.wocketConvert(bafPath, outPath); 		// convert the baf wocket files to one csv file of 1-sec summary for each wocket 
	    	//dataManager.actigraphConvert(inPath, outPath);	//convert the Actigraph csv files to a time-stampped csv ones		
	    	//dataManager.psgConvert(outPath); 					// convert psg txt file to xml and csv format. also devide it per hour for classifier
	    	//dataManager.mergeAccs(bafPath, outPath, subId);	//Merge Wocket Activity Count data files
	    	//dataManager.createDB(subId, bafPath, outPath);
	    	//dataManager.mergeDBs(localPath + "test/", localPath + "test/HB23/");
			//------------------------------------------------------			
	    	/*for (int i = 0; i < 2; i++) { //for all the wockets.
				String dbName = "DB" + subId + "_0" + i + ".csv";
				int algorithm = 1; //1:Sadeh 2:Thereshold 3.Sazono 4.kripke
				Classifier.classify(outPath, dbName, "0" + i, algorithm);
	    	}*/
	    	//------------------------------------------------------
	    	/*MHealthConverter.gen2mHealth("C:/Users/Aida/Desktop/Tricia/", "GENEactiv_Example data_right wrist.csv");
	    	MHealthConverter.gps2mHealth("C:/Users/Aida/Desktop/Tricia/", "GPS Logger_example data.csv");
	    	MHealthConverter.omron2mHealth("C:/Users/Aida/Desktop/Tricia/", "omron pedometer_sample data.csv");*/
	    	
	    	
	    	/*Converter.csv2Xml("C:/Users/Aida/Desktop/30-4/", "Wocket_00_kripke_annotation.csv");
	    	Converter.csv2Xml("C:/Users/Aida/Desktop/30-4/", "Wocket_01_kripke_annotation.csv");
	    	Converter.csv2Xml("C:/Users/Aida/Desktop/30-4/", "Wocket_00_sazonov_annotation.csv");
	    	Converter.csv2Xml("C:/Users/Aida/Desktop/30-4/", "Wocket_01_sazonov_annotation.csv");*/
	    	
		//}
		
		/*try {
			File file = new File("c:/test/test.txt");
			FileOutputStream fos = new FileOutputStream(file);
			BufferedOutputStream bfos = new BufferedOutputStream(fos);
			long start = System.nanoTime();
			byte[] bytes = new byte[32 * 1024];
			for (long l = 0; l < 50 * 1000; l += bytes.length)
			    fos.write(bytes);
			byte[] bytes = new byte[3 * 1024];
			//fos.write(bytes);
			bfos.write(bytes);
			long mid = System.nanoTime();
			fos.close();
			long end = System.nanoTime();
			System.out.printf("Took %.3f milliseconds to write %,d bytes%n", (mid - start) / 1e6, file.length());
			System.out.printf("Took %.3f milliseconds to close%n", (end - mid) / 1e6);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // put your file here.
*/		
	}
	
	public static void getSWSegments() {
		String inPath = "C:/Users/Aida/Data/CIMIT sleep Study_processed/test/HB";
		NumberFormat numFormat = new DecimalFormat("00");
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		for (int j = 2; j < 13; j++) {//subject number
			String psgPath = inPath + numFormat.format(j) + "/";
			BufferedReader in;			
			int start = 0;
			int length = 0;
			String label = "";
			String msg = "";
			String prevStatus = "-1"; 
			
			try {
				in = new BufferedReader(new FileReader(psgPath + "psg.csv"));
				String line = in.readLine(); //skip label
				while ((line = in.readLine()) != null) {				
					
					if (start == 0) {
						Date sTime = parser.parse(line.split(",")[0]);					
						start = (sTime.getHours() * 60 + sTime.getMinutes())*2;
						length = start;
					}
					String status = line.split(",")[1];
					if (status.equals(prevStatus)) {
						length++;
					} else {
						label += prevStatus + ",";
						msg += length + ",";
						prevStatus = status;
						length = 1;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			System.out.println(label +"\n" + msg + "\n");				
		}
	}
	
	
	public static void checkSWDuration() {
		String inPath = "C:/Users/Aida/Data/CIMIT sleep Study_processed/test/HB";
		NumberFormat numFormat = new DecimalFormat("00");
		DecimalFormat df = new DecimalFormat("0.00");
		System.out.println("Subject ID,Total study epochs,Wake,%,s1, %,s2,%,s3,%,REM,%");
		for (int j = 2; j < 13; j++) {//subject number
			String psgPath = inPath + numFormat.format(j) + "/";
			BufferedReader in;
			
			int total = 0;
			int s1 = 0;
			int s2 = 0;
			int s3 = 0;
			int rem = 0;
			int wake = 0;
			
			try {
				in = new BufferedReader(new FileReader(psgPath + "psg.csv"));
				String line = in.readLine(); //skip label
				while ((line = in.readLine()) != null) {
					int key = Integer.parseInt(line.split(",")[1]);
					total++;
					switch (key) {
					case 0:
						s3++;
						break;
					case 1:
						s2++;
						break;
					case 2:
						s1++;
						break;
					case 3:
						rem++;
						break;
					case 4:
						wake++;
						break;
					default:
						break;
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*System.out.println(j + "," + total + "," + wake + "," + df.format(wake*100/total) + "," +
									s1 + "," + df.format(s1*100/total) + "," + s2 +  "," + df.format(s2*100/total) + "," + 
									s3 + "," + df.format(s3*100/total) + "," + rem + "," + df.format(rem*100/total));*/
			System.out.println(j + "," + total/2 + "," + wake/2 + "," + df.format(wake*100/total) + "," +
					s1/2 + "," + df.format(s1*100/total) + "," + s2/2 +  "," + df.format(s2*100/total) + "," + 
					s3/2 + "," + df.format(s3*100/total) + "," + rem/2 + "," + df.format(rem*100/total));
				
		}
	}
	
	
		
}


