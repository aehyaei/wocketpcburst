package classification;

public class Epoch {
	
	private int startIndex;
	private int endIndex;
	private int numberOfData;
	private double average;
	private double variance;
	private double acc;
	private String startTime;
	private String endTime;
	// in original Sadeh's algorithm each epoch is considered as 1 minute 
	private double mu = 0;		//Mean activity on a 11-min window centered on current epoch
	private double sigma = 0;	//standard deviation of activity for the last 6min
	private double logAct = 0;	//natural log of the current epoch activity + 1
	private double nat = 0;		//the number of epochs with (50 <= epoch activity < 100) on a 11-min window centered on current epoch. for Wocket, I consider epoch activity > 100.
	
	
	public int getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
	public int getEndIndex() {
		return endIndex;
	}
	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}
	public int getNumberOfData() {
		return numberOfData;
	}
	public void setNumberOfData(int numberOfData) {
		this.numberOfData = numberOfData;
	}	
	public double getAverage() {
		return average;
	}
	public void setAverage(double average) {
		this.average = average;
	}
	public double getVariance() {
		return variance;
	}
	public void setVariance(double variance) {
		this.variance = variance;
	}
	public double getAcc() {
		return acc;
	}
	public void setAcc(double acc) {
		this.acc = acc;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public double getMu() {
		return mu;
	}
	public void setMu(double mu) {
		this.mu = mu;
	}
	public double getSigma() {
		return sigma;
	}
	public void setSigma(double sigma) {
		this.sigma = sigma;
	}
	public double getLogAct() {
		return logAct;
	}
	public void setLogAct(double logAct) {
		this.logAct = logAct;
	}
	public double getNat() {
		return nat;
	}
	public void setNat(double nat) {
		this.nat = nat;
	}	
	
}
