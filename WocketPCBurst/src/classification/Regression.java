package classification;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

public class Regression {
	final int d = 4;		
	final int n = 18762;//20071;//
	final int k = 10;
	int trainingSize = (int)Math.floor(9*n/(double)k);
	int testingSize = (int)Math.ceil(n/(double)k);
			
	double[][] trainingData = new double[trainingSize][d+2];
	double[][] testingData = new double[testingSize][d+2];		//fold 1
	
	double[] mean = new double[d];			//overall mean for each attribute
	double[] sd = new double[d];			//overall standard deviation for each attribute
	double[] z_score = new double[d];		//Z score for each attribute
	
	//PrintWriter w = null;
	
	NumberFormat formatter = new DecimalFormat("0.#####");
	
	public void readData() {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader("C:/sleepStudy/test/HB20/DB20_03.csv"));
			String line;
			int te = 0;
			int tr = 0;
			int t = 0;
			line = in.readLine();//skip the label
			while ((line = in.readLine()) != null) {
				String[] tokens = line.trim().split(",");
				if (t%10 == 0) {		//test data, i.e. fold 1
					testingData[te][0] = 1;		//dummy
					for (int i = 1; i < d + 2; i++)
						testingData[te][i] = Double.parseDouble(tokens[i-1]);
					te++;
					
				} else {				//train data		
					trainingData[tr][0] = 1;	//dummy
					for (int i = 1; i < d + 2; i++)
						trainingData[tr][i] = Double.parseDouble(tokens[i-1]);
					//System.out.println("label: " + trainingData[tr][d+1]);
					tr++;
				}
				t++;				
			}
			System.out.println("testingData: " + te + "\ntrainingData: " + tr);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void precondition() {
		//compute overall mean for each attribute
		for (int i = 0; i < d; i++) {
			for (int j = 0; j < trainingData.length; j++) {
				mean[i] += trainingData[j][i+1];				
			}
			for (int j = 0; j < testingData.length; j++) {
				mean[i] += testingData[j][i+1];				
			}
			mean[i] /= n;
		}
		
		//compute overall standard deviation for each attribute
		for (int i = 0; i < d; i++) {
			for (int j = 0; j < trainingData.length; j++) {
				sd[i] += Math.pow(trainingData[j][i+1]-mean[i], 2);	
			}
			for (int j = 0; j < testingData.length; j++) {
				sd[i] += Math.pow(testingData[j][i+1]-mean[i], 2);				
			}
			sd[i] = Math.sqrt(sd[i]/(n-1));			
		}	
		
		//compute Z Score for each attribute
		for (int i = 0; i < d; i++) {
			for (int j = 0; j < trainingData.length; j++) {
				trainingData[j][i+1] = (trainingData[j][i+1]-mean[i])/sd[i];	
			}
			for (int j = 0; j < testingData.length; j++) {
				testingData[j][i+1] = (testingData[j][i+1]-mean[i])/sd[i];	
			}
		}	
	}
	
	public void shuffleDataPoints() {
		Random random = new Random();
		for (int i = 0; i < trainingSize; i++) {
			double[] temp = trainingData[i];
			int index = random.nextInt(trainingSize);
		    trainingData[i] = trainingData[index];
		    trainingData[index] = temp;
		}
	}
	
//---------------------------------------------------------------------------------------	
	public double[] linearRegSGD() {

		double[] selectedWeights = new double[d+1];
		
		double[] weight = new double[d+1];
		for (int i = 0; i < weight.length; i++) {
			weight[i] = 0.1;
		}
		
		//lambda is the learning rate
		double[] lambda = new double[10];
		lambda[0] = 1;
		for (int i = 1; i < lambda.length; i++) {
			lambda[i] = lambda[i-1]/10;
		}
		double sss = 0.5;
		double maxsss = 0;
		final double threshold = 1E-4;
		double rmse;
		double fp;
		double fn;
		double tp;
		double tn;	
		double delta;
		int numIterations;

			System.out.println("Learning Rate: " + lambda[6]);
			numIterations = 0;
			//while (Math.abs(prevsss - sss) < threshold) {
				
				for (int i = 0; i < trainingSize; i++) {
					//predicted value
					double h = 0;
					for (int j = 0; j < d+1; j++) {
						h += trainingData[i][j] * weight[j];
					}
					delta = h - trainingData[i][d+1];
					for (int j = 0; j < d+1; j++) {
						weight[j] = weight[j] - lambda[6] * delta * trainingData[i][j];
					}
									
					rmse = 0;
					fp = 0;
					fn = 0;
					tp = 0;
					tn = 0;					
					
					for (int j = 0; j < trainingSize; j++) {
						//new predicted value
						h = 0;
						for (int k = 0; k < d+1; k++) {
							h += trainingData[i][k] * weight[k];
						}
						delta = h - trainingData[i][d+1];
						rmse += Math.pow(delta, 2);	
						if (h > 0)
							if (trainingData[i][d+1] == 1)			
								tn++;
							else									
								fn++;
						else										
							if (trainingData[i][d+1] == 1)			
								fp++;
							else									
								tp++;
					}	
					sss = (tp / (tp + fn)) + (tn / (tn + fp));
					if (sss> maxsss) {
						maxsss = sss;
					}
					rmse = Math.sqrt(rmse/trainingSize);
					numIterations++;
					//System.out.println("h" + h + " iterations, RMSE: " + rmse);
				}
			//}
				System.out.println("maxsss: " + maxsss);
			//if (l == 6) {
				for (int i = 0; i < weight.length; i++) {
					selectedWeights[i] = weight[i];
				}
			//}
		return selectedWeights;
	}
	
	//-----------------------------------------------------------------------------------
	public double[] linearRegressionViaStoachasticGD() {

		double[] selectedWeights = new double[d+1];
		
		double[] weight = new double[d+1];
		for (int i = 0; i < weight.length; i++) {
			weight[i] = 0;
		}
		
		//lambda is the learning rate
		double[] lambda = new double[10];
		lambda[0] = 1;
		for (int i = 1; i < lambda.length; i++) {
			lambda[i] = lambda[i-1]/10;
		}
		
		
		/*
		 * convergence criterion: 
		 * store the last c RMSEs. when the current RMSE is computed, compare it 
		 * against the stored RMSEs. If the difference between the current RMSE 
		 * and all c previous ones is less than convergenceThreshold, stop.
		 */
		final int c = 100;
		final double convergenceThreshold = 1E-8;
		boolean consecutiveSmallDelta;
		double[] prevRmse = new double[c];
		int lastRmseIndex = 0;
		
		//training
		double rmse;
		double delta;
		int numIterations;
		for (int l = 0; l < 7; l++) {
			System.out.println("Learning Rate: " + lambda[l]);
			consecutiveSmallDelta = false;
			numIterations = 0;
			for (int i = 0; i < prevRmse.length; i++) {
				prevRmse[i] = Double.MAX_VALUE;
			}
			while (consecutiveSmallDelta == false) {
				rmse = 0;
				for (int i = 0; i < trainingSize; i++) {
					//predicted value
					double h = 0;
					for (int j = 0; j < d+1; j++) {
						h += trainingData[i][j] * weight[j];
					}
					delta = h - trainingData[i][d+1];
					for (int j = 0; j < d+1; j++) {
						weight[j] = weight[j] - lambda[l] * delta * trainingData[i][j];
					}
				}
				
				//rmse computation
				for (int i = 0; i < trainingSize; i++) {
					//new predicted value
					double h = 0;
					for (int k = 0; k < d+1; k++) {
						h += trainingData[i][k]*weight[k];
					}
					delta = h - trainingData[i][d+1];
					rmse += Math.pow(delta, 2);			
				}
				
				rmse = Math.sqrt(rmse/trainingSize);
				if (Double.isInfinite(rmse) || Double.isNaN(rmse)) {
					System.out.println("no convergence after " + numIterations + " iterations, because at least one of the weights has exceeded the maximum double value.");
					break;
				}
				else {
					consecutiveSmallDelta = true;

					for (int i = 0; i < prevRmse.length; i++) {
						if (Math.abs(prevRmse[i] - rmse) >= convergenceThreshold) {
							consecutiveSmallDelta = false;
							break;
						}
					}
					prevRmse[lastRmseIndex] = rmse;
				}
				lastRmseIndex = (lastRmseIndex+1)%c;
				
				numIterations++;
				/*if (numIterations % 1000 == 0)
					System.out.println("RMSE after " + numIterations + " iterations is " + rmse);
				*/
				if (consecutiveSmallDelta) {
					System.out.println("convergence after " + numIterations + " iterations, RMSE: " + rmse);
				}
			}
			
			if (l == 6) {
				for (int i = 0; i < weight.length; i++) {
					selectedWeights[i] = weight[i];
				}
			}
			
			for (int i = 0; i < weight.length; i++) {
				weight[i] = 0;
			}
		}
		return selectedWeights;
	}
	//---------------------------------------------------------------
	public double[] linearRegressionViaBatchGD() {
		
		double[] selectedWeights = new double[d+1];
		
		double[] weight = new double[d+1];
		for (int i = 0; i < weight.length; i++) {
			weight[i] = 0.1;
		}
		
		//lambda is the learning rate
		double[] lambda = new double[10];
		lambda[0] = 1;
		for (int i = 1; i < lambda.length; i++) {
			lambda[i] = lambda[i-1]/10;
		}
		
		final int c = 200;
		final double convergenceThreshold = 1E-4;
		boolean consecutiveSmallDelta;
		double[] prevRmse = new double[c];
		int lastRmseIndex = 0;
		
		//training
		double rmse;
		//double[] delta;
		int numIterations;
		for (int l = 5; l < 8; l++) {
			System.out.println("Learning Rate: " + lambda[l]);
			consecutiveSmallDelta = false;
			numIterations = 0;
			for (int i = 0; i < prevRmse.length; i++) {
				prevRmse[i] = Double.MAX_VALUE;
			}
			while (consecutiveSmallDelta == false) {
				rmse = 0;
				double[] delta = new double[trainingSize];
				for (int i = 0; i < trainingSize; i++) {
					//predicted value
					double h = 0;
					for (int k = 0; k < d+1; k++) {
						h += trainingData[i][k]*weight[k];
					}
					delta[i] = h - trainingData[i][d+1];
				}
				for (int j = 0; j < d+1; j++) {
					double term = 0;
					for (int i = 0; i < trainingSize; i++) {
						term += delta[i] * trainingData[i][j];
					}
					weight[j] = weight[j] - lambda[l] * term;
				}
				
				//rmse computation
				for (int i = 0; i < trainingSize; i++) {
					//new predicted value
					double h = 0;
					for (int k = 0; k < d+1; k++) {
						h += trainingData[i][k]*weight[k];
					}
					delta[i] = h - trainingData[i][d+1];
					rmse += Math.pow(delta[i], 2);			
				}
				
				rmse = Math.sqrt(rmse/trainingSize);
				if (Double.isInfinite(rmse) || Double.isNaN(rmse)) {
					System.out.println("no convergence after " + numIterations + " iterations, because at least one of the weights has exceeded the maximum double value.");
					break;
				}
				else {
					consecutiveSmallDelta = true;

					for (int i = 0; i < prevRmse.length; i++) {
						if (Math.abs(prevRmse[i] - rmse) >= convergenceThreshold) {
							consecutiveSmallDelta = false;
							break;
						}
					}
					prevRmse[lastRmseIndex] = rmse;
				}
				lastRmseIndex = (lastRmseIndex+1)%c;
				
				numIterations++;
				if (consecutiveSmallDelta) {
					System.out.println("convergence after " + numIterations + " iterations, RMSE: " + rmse);
				}
			}
			
			if (l == 6) {
				for (int i = 0; i < weight.length; i++) {
					selectedWeights[i] = weight[i];
				}
			}
			
			for (int i = 0; i < weight.length; i++) {
				weight[i] = 0;
			}
		}
		return selectedWeights;
	}
	
	public double[] logisticRegressionViaStoachasticGD() {
		
		double[] selectedWeights = new double[d+1];
		
		double[] weight = new double[d+1];
		for (int i = 0; i < weight.length; i++) {
			weight[i] = 0;
		}
		
		//lambda is the learning rate
		double[] lambda = new double[10];
		lambda[0] = 1;
		for (int i = 1; i < lambda.length; i++) {
			lambda[i] = lambda[i-1]/10;
		}
		
		
		/*
		 * convergence criterion: 
		 * store the last c RMSEs. when the current RMSE is computed, compare it 
		 * against the stored RMSEs. If the difference between the current RMSE 
		 * and all c previous ones is less than convergenceThreshold, stop.
		 */
		final int c = 100;
		final double convergenceThreshold = 1E-4;
		boolean consecutiveSmallDelta;
		double[] prevRmse = new double[c];
		int lastRmseIndex = 0;
		
		//training
		double rmse;
		double delta;
		int numIterations;
		for (int l = 6; l < 7; l++) {
			System.out.println("Learning Rate: " + lambda[l]);
			consecutiveSmallDelta = false;
			numIterations = 0;
			for (int i = 0; i < prevRmse.length; i++) {
				prevRmse[i] = Double.MAX_VALUE;
			}
			while (consecutiveSmallDelta == false) {
				rmse = 0;
				for (int i = 0; i < trainingSize; i++) {
					//predicted value
					double h = 0;
					for (int j = 0; j < d+1; j++) {
						h += trainingData[i][j]*weight[j];
					}
					h = 1.0/(1+Math.pow(Math.E, -h));
					delta = h - trainingData[i][d+1];
					for (int j = 0; j < d+1; j++) {
						weight[j] = weight[j] - lambda[l] * delta * h * (1-h) * trainingData[i][j];
					}
				}
				
				//rmse computation
				for (int i = 0; i < trainingSize; i++) {
					//new predicted value
					double h = 0;
					for (int k = 0; k < d+1; k++) {
						h += trainingData[i][k]*weight[k];
					}
					h = 1.0/(1+Math.pow(Math.E, -h));
					delta = h - trainingData[i][d+1];
					rmse += Math.pow(delta, 2);			
				}
				
				rmse = Math.sqrt(rmse/trainingSize);
				if (Double.isInfinite(rmse) || Double.isNaN(rmse)) {
					System.out.println("no convergence after " + numIterations + " iterations, because at least one of the weights has exceeded the maximum double value.");
					break;
				}
				else {
					consecutiveSmallDelta = true;

					for (int i = 0; i < prevRmse.length; i++) {
						if (Math.abs(prevRmse[i] - rmse) >= convergenceThreshold) {
							consecutiveSmallDelta = false;
							break;
						}
					}
					prevRmse[lastRmseIndex] = rmse;
				}
				lastRmseIndex = (lastRmseIndex+1)%c;
				
				numIterations++;
				/*if (numIterations % 1000 == 0)
					System.out.println("RMSE after " + numIterations + " iterations is " + rmse);
				*/
				if (consecutiveSmallDelta) {
					System.out.println("convergence after " + numIterations + " iterations, RMSE: " + rmse);
				}
			}
			
			if (l == 6) {
				for (int i = 0; i < weight.length; i++) {
					selectedWeights[i] = weight[i];
				}
			}
			
			for (int i = 0; i < weight.length; i++) {
				weight[i] = 0;
			}
		}
		return selectedWeights;
	}
	

	public double[] logisticRegressionViaBatchGD() {
		
		double[] selectedWeights = new double[d+1];
		
		double[] weight = new double[d+1];
		for (int i = 0; i < weight.length; i++) {
			weight[i] = 0;
		}
		
		//lambda is the learning rate
		double[] lambda = new double[10];
		lambda[0] = 1;
		for (int i = 1; i < lambda.length; i++) {
			lambda[i] = lambda[i-1] / 10;
		}
		
		
		/*
		 * convergence criterion: 
		 * store the last c RMSEs. when the current RMSE is computed, compare it 
		 * against the stored RMSEs. If the difference between the current RMSE 
		 * and all c previous ones is less than convergenceThreshold, stop.
		 */
		final int c = 100;
		final double convergenceThreshold = 1E-4;
		boolean consecutiveSmallDelta;
		double[] prevRmse = new double[c];
		int lastRmseIndex = 0;
		
		//training
		double rmse;
		//double[] delta;
		int numIterations;
		for (int l = 5; l < 7; l++) {
			System.out.println("Learning Rate: " + lambda[l]);
			consecutiveSmallDelta = false;
			numIterations = 0;
			for (int i = 0; i < prevRmse.length; i++) {
				prevRmse[i] = Double.MAX_VALUE;
			}
			while (consecutiveSmallDelta == false) {
				rmse = 0;
				double[] delta = new double[trainingSize];
				for (int i = 0; i < trainingSize; i++) {
					//predicted value
					double h = 0;
					for (int k = 0; k < d+1; k++) {
						h += trainingData[i][k]*weight[k];
					}
					h = 1.0/(1+Math.pow(Math.E, -h));
					delta[i] = h - trainingData[i][d+1];
				}
				for (int j = 0; j < d+1; j++) {
					double term = 0;
					for (int i = 0; i < trainingSize; i++) {
						term += delta[i] * (delta[i]+trainingData[i][d+1]) * (1-(delta[i]+trainingData[i][d+1])) * trainingData[i][j];
					}
					weight[j] = weight[j] - lambda[l] * term;
				}
				
				//rmse computation
				for (int i = 0; i < trainingSize; i++) {
					//new predicted value
					double h = 0;
					for (int k = 0; k < d+1; k++) {
						h += trainingData[i][k]*weight[k];
					}
					h = 1.0/(1+Math.pow(Math.E, -h));
					delta[i] = h - trainingData[i][d+1];
					rmse += Math.pow(delta[i], 2);			
				}
				
				rmse = Math.sqrt(rmse/trainingSize);
				if (Double.isInfinite(rmse) || Double.isNaN(rmse)) {
					System.out.println("no convergence after " + numIterations + " iterations, because at least one of the weights has exceeded the maximum double value.");
					break;
				}
				else {
					consecutiveSmallDelta = true;

					for (int i = 0; i < prevRmse.length; i++) {
						if (Math.abs(prevRmse[i] - rmse) >= convergenceThreshold) {
							consecutiveSmallDelta = false;
							break;
						}
					}
					prevRmse[lastRmseIndex] = rmse;
				}
				lastRmseIndex = (lastRmseIndex+1)%c;
				
				numIterations++;
				if (consecutiveSmallDelta) {
					System.out.println("convergence after " + numIterations + " iterations, RMSE: " + rmse);
				}
			}
			
			if (l == 6) {
				for (int i = 0; i < weight.length; i++) {
					selectedWeights[i] = weight[i];
				}
			}
			
			for (int i = 0; i < weight.length; i++) {
				weight[i] = 0;
			}
		}
		return selectedWeights;
	}
	//--------------------------------------------------------------
	public void kripke() {
		double[] selectedWeights = new double[13];
		
		double[] weight = new double[13];
		
		//lambda is the learning rate
		double[] lambda = new double[10];
		lambda[0] = 1;
		for (int i = 1; i < lambda.length; i++) {
			lambda[i] = lambda[i-1]/10;
		}
		
		final int c = 100;
		final double convergenceThreshold = 1E-5;
		boolean consecutiveSmallDelta;
		double[] prevRmse = new double[c];
		int lastRmseIndex = 0;
		
		//training
		double rmse;
		//double[] delta;
		int numIterations;
		for (int l = 6; l < 7; l++) {

			for (int i = 0; i < weight.length; i++) {
				weight[i] = 0.5;
			}
			System.out.println("Learning Rate: " + lambda[l]);
			consecutiveSmallDelta = false;
			numIterations = 0;
			for (int i = 0; i < prevRmse.length; i++) {
				prevRmse[i] = Double.MAX_VALUE;
			}
			while (consecutiveSmallDelta == false) {
				rmse = 0;
				double[] delta = new double[trainingSize];
				for (int i = 10; i < trainingSize - 2; i++) {	
					double h = 0;
					for (int k = -10; k < 3; k++) {
						h += trainingData[i+k][1] * weight[k+10];
					}
					delta[i] = h - trainingData[i][d+1];
				}	
				
				for (int j = -10; j < 3; j++) {
					double term = 0;
					for (int i = 10; i < trainingSize - 2; i++) {
						term += delta[i] * trainingData[i+j][1];
					}
					weight[j+10] = weight[j+10] - lambda[l] * term;
				}
				
				//rmse computation
				for (int i = 10; i < trainingSize - 2; i++) {
					//new predicted value
					double h = 0;
					for (int k = -10; k < 3; k++) {
						h += trainingData[i+k][1]*weight[k+10];
					}
					delta[i] = h - trainingData[i][d+1];
					rmse += Math.pow(delta[i], 2);			
				}
				
				rmse = Math.sqrt(rmse/trainingSize);
				if (Double.isInfinite(rmse) || Double.isNaN(rmse)) {
					System.out.println("no convergence after " + numIterations + " iterations, because at least one of the weights has exceeded the maximum double value.");
					break;
				}
				else {
					consecutiveSmallDelta = true;

					for (int i = 0; i < prevRmse.length; i++) {
						if (Math.abs(prevRmse[i] - rmse) >= convergenceThreshold) {
							consecutiveSmallDelta = false;
							break;
						}
					}
					prevRmse[lastRmseIndex] = rmse;
				}
				lastRmseIndex = (lastRmseIndex+1)%c;
				
				numIterations++;
				if (consecutiveSmallDelta) {
					System.out.println("convergence after " + numIterations + " iterations, RMSE: " + rmse);
				}
			}
			
			if (l == 6) {
				for (int i = 0; i < weight.length; i++) {
					selectedWeights[i] = weight[i];
				}
			}			
		}
		for (int j = 0; j < weight.length; j++)
			System.out.println("weight " + j + ": " + selectedWeights[j]);
		//----evaluate----
		double fp = 0;
		double fn = 0;
		double tp = 0;
		double tn = 0;
				
		ArrayList<Instance> rocResults = new ArrayList<Instance>();
		for (int i = 10; i < testingSize - 2; i++) {
			double h = 0;
			for (int k = -10; k < 3; k++) {
				h += testingData[i+k][1] * weight[k+10];
			}
			//System.out.println("testing:" + h);
			double result = (h < 0.5) ? 0: 1;				
			rocResults.add(new Instance((int)testingData[i][d+1], result));
			if (result == 1)
				if (testingData[i][d+1] == 1)			
					tn++;
				else									
					fn++;
			else										
				if (testingData[i][d+1] == 1)			
					fp++;
				else									
					tp++;
		}
		
		double Sen = tp / (tp + fn);
		double Spe = tn / (tn + fp);
		double Acc = (tp + tn) / (fp + fn + tp + tn);
		//double PPV = tp / (tp + fp);
		//double NPV = tn / (tn + fn);
		System.out.println("Sen: " + Sen + "Spe: " + Spe + "Acc: " + Acc);
		System.out.println("tp: " + tp + "fn: " + fn);
		System.out.println("fp: " + fp + "tn: " + tn);
		Collections.sort(rocResults);
		double[] tp_roc = new double[rocResults.size()+1];
		double[] fp_roc = new double[rocResults.size()+1];
		double[] tn_roc = new double[rocResults.size()+1];
		double[] fn_roc = new double[rocResults.size()+1];
		double[] tpr_roc = new double[rocResults.size()+1];
		double[] fpr_roc = new double[rocResults.size()+1];
			
		for (Iterator<Instance> iterator = rocResults.iterator(); iterator.hasNext();) {
			int r = iterator.next().actual;
			if (r == 1)
				tp_roc[0]++;
			else
				fp_roc[0]++;			
		}
		
		fpr_roc[0] = fp_roc[0]/(fp_roc[0]+tn_roc[0]);
		tpr_roc[0] = tp_roc[0]/(tp_roc[0]+fn_roc[0]);
		
		int i = 1;
		for (Iterator<Instance> iterator = rocResults.iterator(); iterator.hasNext();) {
			int r = iterator.next().actual;
			if (r == 1) {
				tp_roc[i] = tp_roc[i-1]-1;
				fp_roc[i] = fp_roc[i-1];
				tn_roc[i] = tn_roc[i-1];
				fn_roc[i] = fn_roc[i-1]+1;
			}
			else {
				tp_roc[i] = tp_roc[i-1];
				fp_roc[i] = fp_roc[i-1]-1;
				tn_roc[i] = tn_roc[i-1]+1;
				fn_roc[i] = fn_roc[i-1];
			}
			fpr_roc[i] = fp_roc[i]/(fp_roc[i]+tn_roc[i]);
			tpr_roc[i] = tp_roc[i]/(tp_roc[i]+fn_roc[i]);
			
			i++;
		}
			
		//compute AUC
		double AUC = 0;
		for (int j = 0; j < fpr_roc.length; j++) {
			if (j < fpr_roc.length-1)
				AUC += (fpr_roc[j]-fpr_roc[j+1])*(tpr_roc[j]+tpr_roc[j+1]);
			//System.out.println(fpr_roc[j]+","+tpr_roc[j]);			
		}
		
		AUC *= 0.5;
		System.out.println("AUC: " + AUC);
		
	}
	
	//--------------------------------------------------------------
	public double testLinearRegression(double[] weight, double[] x) {
		double h = 0;
		for (int j = 0; j < weight.length; j++) {
			h += x[j] * weight[j];
			//System.out.println("h: " + h);
		}
		return (h < 0.5) ? 0: 1;	
	}
	//--------------------------------------------------------------
	public double testLogisticRegression(double[] weight, double[] x) {
		double h = 0;
		for (int j = 0; j < weight.length; j++) {
			h += x[j] * weight[j];
		}
		h = 1.0/(1+Math.pow(Math.E, -h));		
		return (h < 0.5) ? 0: 1;	
	}
	//--------------------------------------------------------------
	public void evaluate(double[] weight, int model) {
		double fp = 0;
		double fn = 0;
		double tp = 0;
		double tn = 0;
				
		ArrayList<Instance> rocResults = new ArrayList<Instance>();
		for (int i = 0; i < testingSize; i++) {
			double result = 0;
			if (model == 1)
				result = testLinearRegression(weight, testingData[i]);
			else if (model == 2)
				result = testLogisticRegression(weight, testingData[i]);				
			rocResults.add(new Instance((int)testingData[i][d+1], result));
			if (result == 1)
				if (testingData[i][d+1] == 1)			
					tn++;
				else									
					fn++;
			else										
				if (testingData[i][d+1] == 1)			
					fp++;
				else									
					tp++;
		}
		
		double Sen = tp / (tp + fn);
		double Spe = tn / (tn + fp);
		double Acc = (tp + tn) / (fp + fn + tp + tn);
		double PPV = tp / (tp + fp);
		double NPV = tn / (tn + fn);
		System.out.println("Sen: " + Sen + "Spe: " + Spe + "Acc: " + Acc);
		System.out.println("tp: " + tp + "fn: " + fn);
		System.out.println("fp: " + fp + "tn: " + tn);
		Collections.sort(rocResults);
		double[] tp_roc = new double[rocResults.size()+1];
		double[] fp_roc = new double[rocResults.size()+1];
		double[] tn_roc = new double[rocResults.size()+1];
		double[] fn_roc = new double[rocResults.size()+1];
		double[] tpr_roc = new double[rocResults.size()+1];
		double[] fpr_roc = new double[rocResults.size()+1];
			
		for (Iterator<Instance> iterator = rocResults.iterator(); iterator.hasNext();) {
			int r = iterator.next().actual;
			if (r == 1)
				tp_roc[0]++;
			else
				fp_roc[0]++;			
		}
		
		fpr_roc[0] = fp_roc[0]/(fp_roc[0]+tn_roc[0]);
		tpr_roc[0] = tp_roc[0]/(tp_roc[0]+fn_roc[0]);
		
		int i = 1;
		for (Iterator<Instance> iterator = rocResults.iterator(); iterator.hasNext();) {
			int r = iterator.next().actual;
			if (r == 1) {
				tp_roc[i] = tp_roc[i-1]-1;
				fp_roc[i] = fp_roc[i-1];
				tn_roc[i] = tn_roc[i-1];
				fn_roc[i] = fn_roc[i-1]+1;
			}
			else {
				tp_roc[i] = tp_roc[i-1];
				fp_roc[i] = fp_roc[i-1]-1;
				tn_roc[i] = tn_roc[i-1]+1;
				fn_roc[i] = fn_roc[i-1];
			}
			fpr_roc[i] = fp_roc[i]/(fp_roc[i]+tn_roc[i]);
			tpr_roc[i] = tp_roc[i]/(tp_roc[i]+fn_roc[i]);
			
			i++;
		}
		
		System.out.println("Points for ROC");		
		//compute AUC
		double AUC = 0;
		for (int j = 0; j < fpr_roc.length; j++) {
			if (j < fpr_roc.length-1)
				AUC += (fpr_roc[j]-fpr_roc[j+1])*(tpr_roc[j]+tpr_roc[j+1]);
			//System.out.println(fpr_roc[j]+","+tpr_roc[j]);			
		}
		
		AUC *= 0.5;
		System.out.println("AUC: " + AUC);
		
	}
	
	public static void main(String[] args) {		
		Regression prog = new Regression();
		prog.readData();
		prog.precondition();
		prog.shuffleDataPoints();
		double weight[];
		/*weight = prog.linearRegSGD();
		prog.evaluate(weight, 1);
		weight = prog.linearRegressionViaStoachasticGD();
		prog.evaluate(weight, 1);*/
		weight = prog.linearRegressionViaBatchGD();
		prog.evaluate(weight, 1);
		/*weight = prog.logisticRegressionViaStoachasticGD();
		prog.evaluate(weight, 2);*/
		/*weight = prog.logisticRegressionViaBatchGD();
		prog.evaluate(weight, 2);*/
		
		for (int i = 0; i < weight.length; i++)
			System.out.println("weight " + i + ": " + weight[i]);
		//prog.kripke();
	}
	
	private class Instance implements Comparable<Instance> {
		int actual;
		double predicted;
		public Instance(int actual, double predicted) {
			this.actual = actual;
			this.predicted = predicted;
		}
		
		@Override
		public int compareTo(Instance inst) {
			if (this.predicted < inst.predicted)
				return -1;
			else if (this.predicted > inst.predicted)
				return 1;
			return 0;
		}		
	}
			
	
}
