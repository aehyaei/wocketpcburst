package classification;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DataManager {
	
	final int MAXSIZE = 146000;
	final int PSGMAXSIZE = 130;
	final int ACCMAXSIZE = 70;
	final int SAMPLINGRATE = 40;
	final int THRESHOLD = 100;			
	double[][] inputData;
	String [] times;
	static int[] PSGlabels;
	static String[] PSGtimes;
	String[] accTimes;
	double[] accValues;
//	String[] agTimes;
//	double[][] agValues;
	private static ArrayList<Epoch> epoch30List;		
	int dataSize = MAXSIZE;
	int psgSize = PSGMAXSIZE;
	int accSize = ACCMAXSIZE; 
//	int agSize = AGMAXSIZE;
	SimpleDateFormat mHealthFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	SimpleDateFormat oldAccFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
	static NumberFormat numFormat = new DecimalFormat("00");
	
	
//****************************FUNCTIONS*************************************
	public void readData(String path) throws Exception {
		inputData = new double[3][MAXSIZE];
		times = new String[MAXSIZE];
		BufferedReader in = null;
		in = new BufferedReader(new FileReader(path));
		String line;
		int t = 0;	
		line = in.readLine(); // for the header line
		while ((line = in.readLine()) != null) {
			String[] tokens = line.trim().split(",");
			times[t] = tokens[0];
			for (int i = 1; i < tokens.length - 1; i++){
				//inputData[i][t] = Integer.parseInt(tokens[i]);
				inputData[i][t] = Double.parseDouble(tokens[i]);
			}
			t++;
		}
		dataSize = t;
		in.close();						
	}
	//--------------------------------------------------------------------------
	/*public void readAg(String path) throws Exception {
		agValues = new double[3][AGMAXSIZE];
		agTimes = new String[AGMAXSIZE];
		BufferedReader in = null;
		in = new BufferedReader(new FileReader(path));
		String line;
		int t = 0;	
		line = in.readLine(); // for the header line
		while ((line = in.readLine()) != null) {
			String[] tokens = line.trim().split(",");
			agTimes[t] = tokens[0];
			for (int i = 1; i < tokens.length - 1; i++) {
				agValues[i][t] = Double.parseDouble(tokens[i]);
			}
			t++;
		}
		agSize = t;
		in.close();						
	}*/
	//--------------------------------------------------------------------------
	public void readPSG(String path) throws Exception {
		PSGlabels = new int[PSGMAXSIZE];
		PSGtimes = new String[PSGMAXSIZE];
		BufferedReader in = null;
		int t = 0;
		in = new BufferedReader(new FileReader(path));
		String line;						
		while ((line = in.readLine()) != null) {
			String[] tokens = line.split(",");
			PSGtimes[t] = tokens[0].trim();
			PSGlabels[t] = Integer.parseInt(tokens[1].trim());	
			t++;
		}
		psgSize = t;
		in.close();			
	}
	//--------------------------------------------------------------------------
	public void readACC(String path, String subID) throws Exception {
		accTimes = new String[ACCMAXSIZE];
		accValues = new double[ACCMAXSIZE];
		BufferedReader in = null;
		int t = 0;
		//try {
			in = new BufferedReader(new FileReader(path));
			String line;						
			while ((line = in.readLine()) != null) {
				String[] tokens = line.split(",");
				
				if (Converter.isDouble(tokens[1].trim())) {
					if (Integer.parseInt(subID) < 9) { // the format of acc files is modified after patient 8
						Date d = oldAccFormat.parse(tokens[0].trim());
						accTimes[t] = mHealthFormat.format(d);
						accValues[t] = Double.parseDouble(tokens[2].trim());//Integer.parseInt(tokens[2].trim());	
					} else {
						accValues[t] = Double.parseDouble(tokens[1].trim());//Integer.parseInt(tokens[1].trim());	
						accTimes[t] = tokens[0].trim();
					}
					t++;
				}
			}
			accSize = t;
			in.close();	/*
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("t: " + t);
		}*/
	}	
	//--------------------------------------------------------------------------	
	public void define30SecEpochs(){
		
		Date date = null;
		int startSec = 0;
		int endSec = 0;
		int startIndex = 0;
		boolean found = false;
		int index = 0;
		int startMin = 0;
		int endMin = 0; 
		int cnt = 0; // counter for epochs with psg labels and without Wocket raw data
		int accIndex = 0;
		epoch30List = new ArrayList<Epoch>();
		
		//find the first PSG time stamp in sensor data file			
		while ( (!found) && (cnt < psgSize)) {		
			try {
				index = 0;
				date = mHealthFormat.parse(PSGtimes[cnt]);
				startSec = date.getSeconds();
				startMin = date.getMinutes();
				if (dataSize == 0)
					break;
				
				String[] tokens = times[index].trim().split(":");
				int min = Integer.parseInt(tokens[1]);
				int sec = Integer.parseInt(tokens[2].substring(0, 2));// get second
				while ((min != startMin) || (sec != startSec)) {				
					index++;
					tokens = times[index].trim().split(":");
					min = Integer.parseInt(tokens[1]);
					sec = Integer.parseInt(tokens[2].substring(0, 2));
					if  ((min > startMin) || (index == dataSize - 1)) {
						cnt++;
						break;
					}
				}
				if ((min == startMin) && (sec == startSec)) {
					found = true;
				}
			} catch (Exception e) {
				System.out.println(cnt + "    " + PSGtimes[cnt] + "   " + index + "   " + dataSize);
				e.printStackTrace();
			}				
			
		}
		
		//create epochs with psg labels and without Wocket raw data
		for (int m = 0; m < cnt; m++) {
			Epoch epoch = new Epoch();
			epoch.setStartTime(PSGtimes[m]);
			epoch.setNumberOfData(0);
			epoch.setAverage(-1);
			epoch.setVariance(-1);
			accIndex = findAccIndex(m, accIndex);
			epoch.setAcc(accValues[accIndex]);			
			epoch30List.add(epoch);
		}	
		
		//create epochs with psg labels and Wocket raw data
		startIndex = index;
		String[] tokens = times[startIndex].trim().split(":");
		endSec = Integer.parseInt(tokens[2].substring(0, 2));
		endMin = Integer.parseInt(tokens[1]);
		int i = startIndex + 1;
		while ((i < dataSize) && (epoch30List.size() < psgSize)) {
			Epoch epoch = new Epoch();
			epoch.setStartTime(PSGtimes[cnt]);
			while ( ((startSec < 30) && (endSec - startSec < 30) && (endMin == startMin)) ||
					((startSec >= 30) && ((endSec <= 59) && (endMin == startMin)) || ((endSec < startSec - 30) && (endMin == startMin + 1))) ) {
				i++;
				if (i == dataSize) {
					break;
				}
				tokens = times[i].trim().split(":");
				endSec = Integer.parseInt(tokens[2].substring(0, 2));// get second
				endMin = Integer.parseInt(tokens[1]);				
			}						
			epoch.setNumberOfData(i - startIndex);
			epoch.setStartIndex(startIndex);
			epoch.setEndIndex(i - 1);			
			if (epoch.getNumberOfData() != 0) {
				epoch.setAverage((calculateAve(startIndex, i - 1, inputData[0]) + calculateAve(startIndex, i - 1, inputData[1]) + calculateAve(startIndex, i - 1, inputData[2]))/3);
				epoch.setVariance(epochVar(startIndex, i - 1));
			}
			else {
				epoch.setAverage(-1);
				epoch.setVariance(-1);
			}				
			accIndex = findAccIndex(cnt, accIndex);
			epoch.setAcc(accValues[accIndex]);			
			epoch30List.add(epoch);
			cnt++;
			if (startSec < 30) {
				startSec = startSec + 30;
			} else {
				startSec = startSec - 30;
				startMin++;
			}
			startIndex = i;		
		}
		
		//create remaining epochs with psg labels and without Wocket raw data
		for (int m = cnt; m < psgSize; m++) {
			Epoch epoch = new Epoch();
			epoch.setStartTime(PSGtimes[m]);
			epoch.setNumberOfData(0);
			epoch.setAverage(-1);
			epoch.setVariance(-1);
			accIndex = findAccIndex(m, accIndex);
			epoch.setAcc(accValues[accIndex]);				
			epoch30List.add(epoch);
		}
	}
	//--------------------------------------------------------------------------
	public int findAccIndex(int psgIndex, int accIndex) {
		long psgMillis = 0;
		long accMillis = 0;
		long nextAccMillis = 0;
		int m = psgIndex;
		int l = accIndex;
		if (l < accSize - 1) {
			 
			try {
				do {
					psgMillis = mHealthFormat.parse(PSGtimes[m]).getTime();
					accMillis = mHealthFormat.parse(accTimes[l]).getTime();
					nextAccMillis = mHealthFormat.parse(accTimes[l+1]).getTime();
					l++;
				} while ((Math.abs(psgMillis - accMillis) > Math.abs(psgMillis - nextAccMillis)) && (l < accSize - 1));
			} catch (Exception e) {
				System.out.println(m + "  " +accIndex + "  " + l);
				e.printStackTrace();
			}				
			
			
			if (Math.abs(psgMillis - accMillis) > Math.abs(psgMillis + 30000 - accMillis)) {
				return l - 1;
			} else {
				return l;
			}
		} else
			return l;
	}
	//--------------------------------------------------------------------------		
	public double calculateAve(int start, int end, double[] data) {
		double sum = 0;
		for (int i = start; i <= end; i++){
			sum += data[i];
		}
		return sum / (end - start + 1);
	}
	//--------------------------------------------------------------------------
	public double calculateVar(int start, int end, double average,  double[] data) {
		double var = 0;
		for (int i = start; i <= end; i++){
			var += Math.pow(data[i] - average, 2);
		}
		return Math.sqrt(var);
	}
	//--------------------------------------------------------------------------
	public double epochVar(int start, int end) { //sum of variance for three axes
		
		double avex = calculateAve(start, end, inputData[0]);
		double avey = calculateAve(start, end, inputData[1]);
		double avez = calculateAve(start, end, inputData[2]);
		double var = calculateVar(start, end, avex, inputData[0]) + 
				calculateVar(start, end, avey, inputData[1]) +
				calculateVar(start, end, avez, inputData[2]);
		return var;
	}
	//--------------------------------------------------------------------------
	public void calculateSadeh() {
		double mu = 0;		//Mean activity on a 11-min window centered on current epoch
		double sigma = 0;	//standard deviation of activity for the last 6min
		double logAct = 0;	//natural log of the current epoch activity + 1
		double nat = 0;		//the number of epochs with (50 <= epoch activity < 100) on a 11-min window centered on current epoch. for Wocket, I consider epoch activity > 100.
		
		for (int i = 0; i < epoch30List.size(); i++) {
			Epoch epoch = epoch30List.get(i);
			int cnt = 0;
			mu = 0;
			nat = 0;
			for (int j = -5; j <= 5; j++) {
				if ((i + j >= 0) && (i + j < epoch30List.size())) {					
					double acc = epoch30List.get(i + j).getAcc();
					mu += acc / 5;
					if (acc > 480)					
						nat++;	
					cnt++;
				}
			}
			mu /= cnt;
			//-------------------------------------
			double mean = 0;
			cnt = 0;
			for (int j = -5; j <= 0; j++) {
				if (i + j >= 0) {
					mean += epoch30List.get(i + j).getAcc() / 5;					
					cnt++;
				}
			}
			if (cnt != 0) { 					
				mean /= cnt;
				//-------------------------------------
				for (int j = -5; j <= 0; j++) {
					if (i + j >= 0) {											
						sigma += Math.pow((epoch30List.get(i + j).getAcc() / 5 - mean), 2);
						//sigma += Math.pow((epoch30List.get(i + j).getAcc() - mean), 2);
					}
				}
				sigma = Math.sqrt(sigma);
			} else 
				sigma = 0;
			//--------------------------------------			
			logAct = Math.log(epoch.getAcc()/5) + 1;
			//logAct = Math.log(epoch.getAcc()) + 1;
			//--------------------------------------			
			epoch.setMu(mu);
			epoch.setSigma(sigma);
			epoch.setLogAct(logAct);
			epoch.setNat(nat);
		}
	}
	//---------------------------- Wocket Baf  to csv --------------------------
	public void wocketConvert(String bafPath, String outPath) {
		SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date startDate = null;
		Date endDate = null;
		BafDecoder bafDecoder;		
		File folderFile = new File(bafPath);
		String id; //Wocket id	
		String[] days = folderFile.list(new FilenameFilter() {							
			@Override
			public boolean accept(File dir, String filename) {
				return filename.matches("\\d{4}-\\d{2}-\\d{2}");
			}
		});
		bafDecoder = new BafDecoder();		
		//___________________for classifier_________________
		/*
		for (int i = 0; i < 2; i++) { //number of Wockets
			id = numFormat.format(i);
			for (int j = 0; j < days.length; j++) {
				for (int k = 0; k < 24; k++) {
					try {
						startDate = dayFormat.parse(days[j] + " " + numFormat.format(k) + ":00");
						System.out.println(days[j] + " " + numFormat.format(k) + ":00");
						endDate = dayFormat.parse(days[j] +  " " + numFormat.format(k) + ":59");
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd");
					String outFileName = sFormat.format(startDate) + "/" + k + "/" + "Wocket." + id + ".RAW_DATA.csv";
					//String outFileName = "Wocket." + id + ".RAW_DATA.csv";
					bafDecoder.decodeAndSaveDataWithStream(startDate, endDate, id, bafPath, outPath + sFormat.format(startDate) + "/" + k + "/", outFileName); 
			    	
				}
			}
		}*/
		//___________________for visualizer___________________
		/*
		for (int i = 0; i < 2; i++) { //number of Wockets
			id = numFormat.format(i);
			for (int j = 0; j < days.length; j++) {
				try {
					startDate = dayFormat.parse(days[j] + " 00:00");
					endDate = dayFormat.parse(days[j] + " 23:59");
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
		    	bafDecoder.decodeAndSaveDataWithStream(startDate, endDate, id, bafPath, outPath,"Wocket." + id + "_" + j + ".csv"); 
		    	Converter.makeSummary(outPath, "Wocket." + id + "_" + j + ".csv", "Wocket." + id + "_" + j + ".1s.csv");
			}		
			Converter.merge(outPath, "Wocket." + id + "_0.1s.csv", "Wocket." + id + "_1.1s.csv", "Wocket." + id + ".1s.csv");
		}*/
		//_________________test
		try {
			startDate = dayFormat.parse("2013-05-21 00:00");
			endDate = dayFormat.parse("2013-05-21 23:59");
		} catch (ParseException e) {
			e.printStackTrace();
		}	
    	
		bafDecoder.decodeAndSaveDataWithStream(startDate, endDate, "01", "C:/Users/Aida/Desktop/test/", "C:/Users/Aida/Desktop/test/","Wocket." + 1 + ".csv"); 
	}
	//-----------------------------Actigraph Converter -------------------------------------
	public void actigraphConvert(String inPath, String outPath) {
		File folder = new File(inPath);
		String[] files = folder.list(new FilenameFilter() {							
			@Override
			public boolean accept(File dir, String filename) {
				return filename.endsWith(".csv");
			}
		});
		for (int i = 0; i < files.length; i++) {			
			//Converter.agRaw2agTimed(inPath, files[i], outPath, "modified_" + files[i]);	//raw to timed csv
			//Converter.makeSummary(outPath, "modified_" + files[i], "ACRAW." + numFormat.format(i) +".1s.csv");
			//Converter.fileDivide(outPath, "ACRAW." + numFormat.format(i) +".1s.csv"); //1 sec summary raw data
			Converter.fileDivide(outPath, "agRaw_" + numFormat.format(i) +".csv"); //timed raw data
			Converter.agTimed2agCount(outPath, "agRaw_" + numFormat.format(i) +".csv", outPath, "agACC_" + numFormat.format(i) +".csv", 30);
			Converter.fileDivide(outPath, "agACC_" + numFormat.format(i) +".csv"); //activity count data			
		}
		System.out.println("converting actigraph files is done!");
		//---------------------------------actigraph raw to baf-------------------------------
		/*String inputFile = "C:/sleepStudy/test/actigraph/NEO1C06110086 (2012-12-04)RAW.csv";
		String actiOutPathBaf = "C:/sleepStudy/test/actigraph/baf/";
		DataPreprocessor hfHandler = new DataPreprocessor(0);
		hfHandler.Preprocessor(inputFile, actiOutPathBaf);*/
		//--------------------------------------acti baf to csv-------------------------------
		/*String actiOutPathCsv = "C:/sleepStudy/test/actigraph/baf/";
		String actiInPath = "C:/sleepStudy/test/actigraph/baf/";
		try	{
			Date startTime = dateFormat.parse("2012-12-03 11:30");
			Date endTime = dateFormat.parse("2012-12-03 13:00");
			BafDecoder bd =  new BafDecoder(actiInPath, actiOutPathCsv);
			bd.decodeAndSaveDataWithStream(startTime, endTime, "03");
		}
		catch(Exception e) {
			e.printStackTrace();
		}*/	
			
	}
	//---------------------------------psg converter----------------------------
	public void psgConvert(String outPath) {
		File folderFile = new File(outPath);
		String[] days = folderFile.list(new FilenameFilter() {							
			@Override
			public boolean accept(File dir, String filename) {
				return filename.matches("\\d{4}-\\d{2}-\\d{2}");
			}
		});
		//Converter.psg2Xml(outPath, days);		
		//Converter.psg2Csv(outPath, days);		
		Converter.fileDivide(outPath, "psg.csv");
	}
	//---------------------------Create Sensor Data Bases-----------------------
	public void createDB(String subId, String bafPath, String outPath) {
						
		File folderFile = new File(bafPath);
		String[] days = folderFile.list(new FilenameFilter() {							
			@Override
			public boolean accept(File dir, String filename) {
				return filename.matches("\\d{4}-\\d{2}-\\d{2}");
			}
		});
		String[] hours;
		for (int i = 0; i < 2; i++) { //number of Wockets			
			for (int j = 0; j < days.length; j++) { //number of days
				String datePath = outPath + days[j] + "/";
				File hourFile = new File(datePath);
				hours = hourFile.list(new FilenameFilter() {							
					@Override
					public boolean accept(File dir, String filename) {
						return (filename.matches("\\d{2}") || filename.matches("\\d{1}"));						
					}
				});
				//----Insertion sort of hours
				int key;
				int q;
				for (int l = 1; l < hours.length; l++) {
					key = Integer.parseInt(hours[l]);
					for (q = l-1; (q >= 0) && (Integer.parseInt(hours[q]) > key); q--) { 
						hours[q + 1] = hours[q];
					}
					hours[q + 1] = Integer.toString(key);
				}
				//-----------------------------
				for (int k = 0; k < hours.length; k++) {
					String filePath = datePath + hours[k] + "/";
			        PrintWriter out = null;
					try {
						File file = new File(filePath);
						String name = null;
						final String id = numFormat.format(i);
						if(file.isDirectory()) {
							String[] fileNames = file.list(new FilenameFilter() {								
								@Override
								public boolean accept(File dir, String filename) {
									return filename.contains("Wocket_" + id);// for #2 & #0
									//return filename.contains("Wocket." + id) && filename.contains("RAW"); 
									//return filename.contains("ACRAW." + id); 
									//return filename.contains("agRaw_" + id);
								}
							});
							if (fileNames.length == 0)
								continue;
							name = fileNames[0];
						}
						
						String accPath = bafPath + days[j] + "/" + String.format("%02d", Integer.parseInt(hours[k])) + "/";
						File accFile = new File(accPath);
						String[] accNames = accFile.list(new FilenameFilter() {								
							@Override
							public boolean accept(File dir, String filename) {
								return filename.contains("Wocket." + id) && filename.contains(".csv");
								//return filename.contains("Wocket." + id) && filename.contains("ACTIVITY"); //for real time test
							}
						});
						
						readData(filePath + name);
						readPSG(filePath + "psg.csv");						
						readACC(accPath + accNames[0], subId);
						//readACC(filePath + "agACC_" + id + ".csv", subId);
						define30SecEpochs();
						calculateSadeh();
						//-------------------
						/*double x = 0.1;// Math.sqrt(x*x + y*y + z*z) = 1
						double y = 0.1;
						double z = 0.1;
						double pitch = Math.atan(x/Math.sqrt(y*y + z*z)); 
						double roll = Math.atan(y/Math.sqrt(x*x + z*z));  
						double yaw = Math.atan(Math.sqrt(y*y + x*x)/z); */
						
						//-------------------------save database--------------------------
						String dbName = outPath + "DB" + subId + "_" + numFormat.format(i) + ".csv";
						//String dbName = outPath + "DB" + subId + "_" + numFormat.format(i) + "_ag.csv";
						String dbLabel = "TIME_STAMP,NUMDATA,AVERAGE,VARIANCE,ACC,MU,SIGMA,LOGACT,NAT,PSGLABEL";
						File dbDir = new File(outPath);
				    	if (!dbDir.isDirectory()) {
				    		dbDir.mkdirs();
						}    	
				        File dbFile = new File(dbName);
				        if (!dbFile.exists()) {	
				        	dbFile.createNewFile();                    
	                    	out = new PrintWriter(new FileWriter(dbName));
	                    	out.append(dbLabel + "\n");
				        } else {
				        	out = new PrintWriter(new FileWriter(dbName, true));
				        	//out.append("\n");
				        }
	                    for (int m = 0; m < epoch30List.size(); m++) {
	                    	Epoch epoch = epoch30List.get(m);
							String msg =  PSGtimes[m] + "," + epoch.getNumberOfData() + "," +
									epoch.getAverage() + "," + epoch.getVariance() + "," +
									epoch.getAcc() + "," + (int)epoch.getMu() + "," + (int)epoch.getSigma()  + "," +
									(int)epoch.getLogAct() + "," + (int)epoch.getNat()  + "," + PSGlabels[m];
							out.append(msg + "\n");
						}
	                    out.flush();
					} catch (Exception e) {	
						System.out.println("hour: " + hours[k]);
						e.printStackTrace();
					} finally {									    
				        if (out != null) {
				        	out.flush();
				            out.close();
				        }	
					}
				}//k
			}//j
			System.out.println("done with a DB");
		}//i
		
	}	
	//---------------------------merge ACC Wocket files-------------------------
	public void mergeAccs(String bafPath, String outPath, String subId) {
		File folderFile = new File(bafPath);
		String[] days = folderFile.list(new FilenameFilter() {							
			@Override
			public boolean accept(File dir, String filename) {
				return filename.matches("\\d{4}-\\d{2}-\\d{2}");
			}
		});
		String[] hours;
		for (int i = 0; i < 2; i++) { //number of Wockets			
			for (int j = 0; j < days.length; j++) {
				String datePath = bafPath + days[j] + "/";
				File hourFile = new File(datePath);
				hours = hourFile.list(new FilenameFilter() {							
					@Override
					public boolean accept(File dir, String filename) {
						return (filename.matches("\\d{2}") || filename.matches("\\d{1}"));						
					}
				});
				//----Insertion sort of hours
				int key;
				int q;
				for (int l = 1; l < hours.length; l++) {
					key = Integer.parseInt(hours[l]);
					for (q = l-1; (q >= 0) && (Integer.parseInt(hours[q]) > key); q--) { 
						hours[q + 1] = hours[q];
					}
					hours[q + 1] = Integer.toString(key);
				}
				//-----------------------------
				for (int k = 0; k < hours.length; k++) {				
			        PrintWriter out = null;
					try {						
						final String id = numFormat.format(i);
						String accPath = bafPath + days[j] + "/" + String.format("%02d", Integer.parseInt(hours[k])) + "/";
						File accFile = new File(accPath);
						String[] accNames = accFile.list(new FilenameFilter() {								
							@Override
							public boolean accept(File dir, String filename) {
								return filename.contains("Wocket." + id) && filename.contains(".csv");
							}
						});
						readACC(accPath + accNames[0], subId);
						
						String dbName = outPath + "ACC_" + id + ".csv";
						String dbLabel = "TIME_STAMP,ACCValue";
						File dbDir = new File(outPath);
				    	if (!dbDir.isDirectory()) {
				    		dbDir.mkdirs();
						}    	
				        File dbFile = new File(dbName);
				        if (!dbFile.exists()) {	
				        	dbFile.createNewFile();                    
	                    	out = new PrintWriter(new FileWriter(dbName));
	                    	out.append(dbLabel + "\n");
				        } else {
				        	out = new PrintWriter(new FileWriter(dbName, true));
				        }
	                    for (int m = 0; m < accSize; m++) {
							String msg =  accTimes[m] + "," + accValues[m];
							out.append(msg + "\n");
						}
				        /*BufferedReader in = new BufferedReader(new FileReader(accPath + accNames[0]));
						String line;						
						while ((line = in.readLine()) != null) {
							out.append(line + "\n");
						}*/
	                    out.flush();
					} catch (Exception e) {	
						System.out.println("hour: " + hours[k]);
						e.printStackTrace();
					} finally {									    
				        if (out != null) {
				        	out.flush();
				            out.close();
				        }	
					}
				}//for k
			} //for j
		} //for i
		System.out.println("Merging Acc files is done.");
	}
	//---------------------------merge database files---------------------------
	public void mergeDBs(String inPath, String outPath) {
		File folderFile = new File(inPath);
		String[] subjects = folderFile.list(new FilenameFilter() {							
			@Override
			public boolean accept(File dir, String filename) {
				return filename.contains("HB");
			}
		});
		for (int i = 0; i < 2; i++) { //number of Wockets			
			for (int j = 0; j < subjects.length; j++) {	//number of patients
				//if (!(subjects[j].contains("4") || subjects[j].contains("5") || subjects[j].contains("8")|| subjects[j].contains("11"))) {
				if (!subjects[j].contains("8")) {
			        PrintWriter out = null;
					try {						
						final String id = numFormat.format(i);
						String dbName = outPath + "DB_" + id + ".csv";
						File dbDir = new File(outPath);
				    	if (!dbDir.isDirectory()) {
				    		dbDir.mkdirs();
						}    	
				        File dbFile = new File(dbName);
				        if (!dbFile.exists()) {	
				        	dbFile.createNewFile();                    
	                    	out = new PrintWriter(new FileWriter(dbName));
				        } else {
				        	out = new PrintWriter(new FileWriter(dbName, true));
				        }
				        
				        //String dbPath = inPath + subjects[j] + "/";
				        String dbPath = inPath + subjects[j] + "/Wocket/";
						
				        File accFile = new File(dbPath);
						String[] dbfiles = accFile.list(new FilenameFilter() {								
							@Override
							public boolean accept(File dir, String filename) {
								return filename.contains("DB") && filename.contains("_"+id);
							}
						});
	                    BufferedReader in = new BufferedReader(new FileReader(dbPath + dbfiles[0]));
						String line = in.readLine();	//ignore the label line					
						while ((line = in.readLine()) != null) {
							out.append(line + "\n");
						}
	                    out.flush();
					} catch (Exception e) {	
						System.out.println("subject: " + subjects[j]);
						e.printStackTrace();
					} finally {									    
				        if (out != null) {
				        	out.flush();
				            out.close();
				        }	
					}
				} //if
			} //for j
		} //for i
		System.out.println("Merging DB files is done.");
	}
	
	
}
	

