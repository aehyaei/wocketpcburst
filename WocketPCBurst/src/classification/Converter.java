package classification;


import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

public class Converter {

	private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private static SimpleDateFormat mHealthFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	//private static String localPath = "/Users/aidaehyaei/Documents/sleepStudy/"; //MAC
	//private static String localPath = "c:/sleepStudy/test/"; //Windows

	public static void psg2Xml(String path, String[] days) {
		String outPath = path;
		String inPath = path + "PSG.txt";
		String fileName = outPath + "psg.xml";
		BufferedReader in = null;
		String line = null;
		int dayCount = 0;
		boolean dayChange = false;

		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance(
					null, null);
			XmlSerializer xmlSerializer = factory.newSerializer();

			File outputDir = new File(outPath);
			if (!outputDir.isDirectory()) {
				outputDir.mkdirs();
			}
			File wocketFile = new File(fileName);
			/*
			 * if(!wocketFile.exists()){ wocketFile.createNewFile(); }
			 */
			if (wocketFile.exists())
				wocketFile.delete();
			wocketFile.createNewFile();
			BufferedWriter writer = new BufferedWriter(new FileWriter(
					wocketFile));
			xmlSerializer.setOutput(writer);

			xmlSerializer.startDocument("UTF-8", true);
			xmlSerializer.startTag(null, "SLEEPDATA");
			xmlSerializer.attribute(null, "xmlns", "urn:mites-schema");
			xmlSerializer.startTag(null, "ANNOTATIONS");
			xmlSerializer.attribute(null, "DATASET", "HospitalSleep");
			xmlSerializer.attribute(null, "ANNOTATOR", "Simple-Algorithm");
			xmlSerializer.attribute(null, "EMAIL", "aidaeh@ccs.neu.edu");
			xmlSerializer
					.attribute(
							null,
							"DESCRIPTION",
							"An on-line annotation of the data which is done while collecting data from Wocket by a simple algorithm");
			xmlSerializer.attribute(null, "METHOD",
					"based on pre-defined theresholds");
			xmlSerializer.attribute(null, "NOTES", "");

			in = new BufferedReader(new FileReader(inPath));

			while ((line = in.readLine()) != null) {
				String[] tokens = line.split("    ");
				if ((tokens.length != 0) && isInteger(tokens[0].trim())) {

					Date date = null;
					try {
						date = timeFormat.parse(tokens[1].trim());
					} catch (ParseException e) {
						System.out.println(e);
					}
					// date.setSeconds(date.getSeconds() + 29);
					if ((date.getHours() == 0) && (!dayChange)) {
						dayChange = true;
						dayCount++;
					}
					if ((date.getHours() == 1) && (dayChange)) {
						dayChange = false;
					}

					xmlSerializer.startTag(null, "ANNOTATION");
					xmlSerializer.attribute(null, "GUID", "5432");
					xmlSerializer.startTag(null, "LABEL");
					xmlSerializer.attribute(null, "GUID", "5544");
					//xmlSerializer.text((tokens[2].contains("W")) ? "1" : "0");
					xmlSerializer.text(tokens[2].trim()) ;
					xmlSerializer.endTag(null, "LABEL");
					xmlSerializer.startTag(null, "START_DT");
					xmlSerializer.text(days[dayCount] + " " + tokens[1].trim()
							+ ".000");
					xmlSerializer.endTag(null, "START_DT");

					date.setSeconds(date.getSeconds() + 29);
					if ((date.getHours() == 0) && (!dayChange)) {
						dayChange = true;
						dayCount++;
					}

					xmlSerializer.startTag(null, "STOP_DT");
					xmlSerializer.text(days[dayCount] + " "
							+ timeFormat.format(date) + ".999");
					xmlSerializer.endTag(null, "STOP_DT");
					xmlSerializer.startTag(null, "PROPERTIES");
					xmlSerializer.attribute(null, "ANNOTATION_SET",
							"Sleep_Study");
					xmlSerializer.attribute(null, "LAST_MODIFIED",
							mHealthFormat.format(Calendar.getInstance()
									.getTime()));
					xmlSerializer.attribute(null, "DATE_CREATED", mHealthFormat
							.format(Calendar.getInstance().getTime()));
					xmlSerializer.endTag(null, "PROPERTIES");
					xmlSerializer.endTag(null, "ANNOTATION");

				}
			}

			xmlSerializer.endTag(null, "ANNOTATIONS");
			xmlSerializer.endTag(null, "SLEEPDATA");
			xmlSerializer.endDocument();

			xmlSerializer.flush();
			writer.flush();
			writer.close();
			System.out.println("The psg.txt is converted to xml file!");
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	// -----------------------------------------------------------------
	public static void psg2Csv(String path, String[] days) {
		
		String outFileName = path + "psg.csv";
		String label = "TIME_STAMP,LABEL";
		BufferedReader in = null;
		String line = null;
		int dayCount = 0;
		boolean dayChange = false;

		File outputDir = new File(path);
		if (!outputDir.isDirectory()) {
			outputDir.mkdirs();
		}
		File f = new File(outFileName);
		PrintWriter out = null;

		try {
			if (f.exists())
				f.delete();
			f.createNewFile();
			out = new PrintWriter(new FileWriter(outFileName, true));
			out.append(label + "\n");
			in = new BufferedReader(new FileReader(path + "PSG.txt"));

			while ((line = in.readLine()) != null) {
				String msg = null;
				int value = 0;
				String[] tokens = line.split("    ");
				if ((tokens.length != 0) && isInteger(tokens[0].trim())) {
					Date date = null;
					try {
						date = timeFormat.parse(tokens[1].trim());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if ((date.getHours() == 0) && (!dayChange)) {
						dayChange = true;
						dayCount++;
					}
					if ((date.getHours() == 1) && (dayChange)) {
						dayChange = false;
					}
					msg = days[dayCount] + " " + tokens[1].trim() + ".000";
					
					if (tokens[2].trim().equalsIgnoreCase("W")) {
						value = 4;
					} else if (tokens[2].trim().equalsIgnoreCase("R")) {
						value = 3;
				    } else if (tokens[2].trim().equalsIgnoreCase("1")) {
						value = 2;
					} else if (tokens[2].trim().equalsIgnoreCase("2")) {
						value = 1;
					} else if (tokens[2].trim().equalsIgnoreCase("3")) {
						value = 0;					
					}
					msg += "," + value;
					out.append(msg + "\n");
				}
			}
			if (in != null) {
				in.close();
			}
			out.flush();
			if (out != null) {
				out.close();
			}
			System.out.println("The psg.txt is converted to csv file!");
		} catch (IOException e) {			
			e.printStackTrace();
		}

	}

	// -----------------------------------------------------------------
	public static void fileDivide(String path, String fName) {

		String msg;
		String[] dayFolders = new String[2];
		BufferedReader in = null;
		PrintWriter out = null;
		int day = 0;
		int prevHour = -1;
		try {
			in = new BufferedReader(new FileReader(path + fName));
			String line;
			String label = in.readLine(); // the first line of the file should be the label
			while ((line = in.readLine()) != null) {
				String[] tokens = line.split(",");
				if ((isInteger(tokens[1].trim())) || (isDouble(tokens[1].trim()))) {
					msg = line + "\n";	
					File folder = new File(path);
					if (folder.isDirectory()) {
						dayFolders = folder.list(new FilenameFilter() {
							@Override
							public boolean accept(File dir, String filename) {
								return filename.matches("\\d{4}-\\d{2}-\\d{2}");
							}
						});
					}
					int hour = mHealthFormat.parse(tokens[0]).getHours();	
					if ((hour == 0) && (prevHour != -1))
						day++;
					if (prevHour != hour) {
						if (out != null)
							out.flush();
						System.out.println("hour: " + hour);
						String outputFilePath = path + dayFolders[day] + "/" + hour + "/";
						File outputDir = new File(outputFilePath);
						if (!outputDir.isDirectory()) {
							outputDir.mkdirs();
						}
						String filename = outputFilePath + fName;
						File f = new File(filename);
						if (f.exists())
							f.delete();
						f.createNewFile();
						out = new PrintWriter(new FileWriter(filename, true));
						out.append(label + "\n");
						prevHour = hour;
					}					
					out.append(msg);					
				}
			} // while
			System.out.println("Dividing " + fName + " is done.");
			if (out != null) {
				out.close();
			}
			if (in != null) {
				in.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// }
	}

	// -----------------------------------------------------------------
	public static void csv2Xml(String path, String fName) {
		String outPath = path;
		String inPath = path + fName;
		String fileName = outPath + fName.substring(0, fName.length() - 3) + "xml";
		BufferedReader in = null;
		String line = null;

		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance(
					null, null);
			XmlSerializer xmlSerializer = factory.newSerializer();

			File outputDir = new File(outPath);
			if (!outputDir.isDirectory()) {
				outputDir.mkdirs();
			}
			File wocketFile = new File(fileName);
			if (wocketFile.exists())
				wocketFile.delete();
			wocketFile.createNewFile();
			BufferedWriter writer = new BufferedWriter(new FileWriter(
					wocketFile));
			xmlSerializer.setOutput(writer);

			xmlSerializer.startDocument("UTF-8", true);
			xmlSerializer.startTag(null, "SLEEPDATA");
			xmlSerializer.attribute(null, "xmlns", "urn:mites-schema");
			xmlSerializer.startTag(null, "ANNOTATIONS");
			xmlSerializer.attribute(null, "DATASET", "HospitalSleep");
			xmlSerializer.attribute(null, "ANNOTATOR", "Simple-Algorithm");
			xmlSerializer.attribute(null, "EMAIL", "aidaeh@ccs.neu.edu");
			xmlSerializer
					.attribute(
							null,
							"DESCRIPTION",
							"An on-line annotation of the data which is done while collecting data from Wocket by a simple algorithm");
			xmlSerializer.attribute(null, "METHOD",
					"based on pre-defined theresholds");
			xmlSerializer.attribute(null, "NOTES", "");

			in = new BufferedReader(new FileReader(inPath));
			line = in.readLine(); // ignore the label row
			while ((line = in.readLine()) != null) {
				String[] tokens = line.split(",");

				xmlSerializer.startTag(null, "ANNOTATION");
				xmlSerializer.attribute(null, "GUID", "5432");
				xmlSerializer.startTag(null, "LABEL");
				xmlSerializer.attribute(null, "GUID", "5544");
				xmlSerializer.text((tokens[2].contains("W")) ? "Wake" : "Sleep");
				//xmlSerializer.text(tokens[1].equals("1") ? "Wake" : "Sleep") ;
				xmlSerializer.endTag(null, "LABEL");
				xmlSerializer.startTag(null, "START_DT");
				xmlSerializer.text(tokens[0]);
				xmlSerializer.endTag(null, "START_DT");
				/*Date date = null;
				try {
					date = mHealthFormat.parse(tokens[0]);
				} catch (ParseException e) {
					System.out.println(e);
				}
				date.setSeconds(date.getSeconds() + 29);*/
				xmlSerializer.startTag(null, "STOP_DT");
				//xmlSerializer.text(mHealthFormat.format(date));
				xmlSerializer.text(tokens[1]);
				xmlSerializer.endTag(null, "STOP_DT");
				xmlSerializer.startTag(null, "PROPERTIES");
				xmlSerializer.attribute(null, "ANNOTATION_SET",
						"Sleep_Study");
				xmlSerializer.attribute(null, "LAST_MODIFIED",
						mHealthFormat.format(Calendar.getInstance()
								.getTime()));
				xmlSerializer.attribute(null, "DATE_CREATED", mHealthFormat
						.format(Calendar.getInstance().getTime()));
				xmlSerializer.endTag(null, "PROPERTIES");
				xmlSerializer.endTag(null, "ANNOTATION");
			}

			xmlSerializer.endTag(null, "ANNOTATIONS");
			xmlSerializer.endTag(null, "SLEEPDATA");
			xmlSerializer.endDocument();

			xmlSerializer.flush();
			writer.flush();
			writer.close();
			System.out.println("The psg.txt is converted to xml file!");
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	// -----------------------------------------------------------------	
	public static void make1SecSummary(String path, String inName, String outName) {
		SimpleDateFormat mHealthFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String label = "TIME_STAMP,X,Y,Z";
		
		int sec = -1;
		double[] sum = {0, 0, 0};
		int counter = 0;
		
		BufferedOutputStream outputStream = null;
		BufferedReader in = null;
		
		try {
	        File outputFile = new File(path + outName);
	        if (!outputFile.exists()) {
	        	outputFile.createNewFile();
			}
			outputStream = new BufferedOutputStream(new FileOutputStream(outputFile,true));
			outputStream.write((label + "\n").getBytes());
			
			File inputFile = new File(path + inName);
			if(!inputFile.exists()) {
				System.out.println("Input file doesn't exist.");
			}
			in = new BufferedReader(new FileReader(path + inName));
			String line = in.readLine(); // ignore the label row
			String msg = null;
			while ((line = in.readLine()) != null) {
				String[] tokens = line.split(",");
				Date date = mHealthFormat.parse(tokens[0]);
				if (sec == -1) {
					sec = date.getSeconds();					
				} else if (sec != date.getSeconds()) {
					if (counter != 0) {
						 msg = tokens[0] + "," + sum[0]/counter + "," + sum[1]/counter + "," + sum[2]/counter;
						 outputStream.write((msg + "\n").getBytes());
					}
					sum[0] = sum[1] = sum[2] = counter = 0;
					sec = date.getSeconds();
				} else {
					for (int i = 0; i < 3; i++)
						sum[i] += Double.parseDouble(tokens[i+1]);
					counter++;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
        			

		try {
			if(in != null)
				in.close();
			if(outputStream != null) {
				outputStream.flush();
				outputStream.close();
			}
		} catch (IOException e) {
			System.out.println("Can not close file stream." + e.toString());
		}	
	}   
	
	// -----------------------------------------------------------------
	public static void merge(String path, String file1, String file2, String outPut) {
		
		BufferedReader bufferdReader1 = null;
		BufferedReader bufferdReader2 = null;
		BufferedOutputStream outputStream = null;		
		
		try {
			bufferdReader1 = new BufferedReader(new FileReader(path + file1));
			bufferdReader2 = new BufferedReader(new FileReader(path + file2));
			outputStream = new BufferedOutputStream(new FileOutputStream(path + outPut,true));
			
			String line = null;
			while ((line = bufferdReader1.readLine()) != null) {
				outputStream.write((line + "\n").getBytes());
			} 
			outputStream.flush();
			//line = bufferdReader2.readLine(); //ignore the label line
			while ((line = bufferdReader2.readLine()) != null) {
				outputStream.write((line + "\n").getBytes());
			}
			outputStream.flush();
		} catch (IOException e) { 
			System.out.println("Input file doesn't exist.");
			System.exit(1);
		}
		
		try {
			if(bufferdReader1 != null)
				bufferdReader1.close();
			if(bufferdReader2 != null)
				bufferdReader2.close();
			if(outputStream != null) {
				outputStream.flush();
				outputStream.close();
			}
			System.out.println("Marging is done!");
		} catch (IOException e) {
			System.out.println("Can not close file stream." + e.toString());
		}					
	}
	
	// -----------------------------------------------------------------
	public static void mergeAcc(String inPath, String outPath) {
		NumberFormat numFormat = new DecimalFormat("00");
		BufferedReader bufferdReader = null;
		BufferedOutputStream outputStream = null;
		String[] hours;
		
		File folderFile = new File(inPath);
		String[] days = folderFile.list(new FilenameFilter() {							
			@Override
			public boolean accept(File dir, String filename) {
				return filename.matches("\\d{4}-\\d{2}-\\d{2}");
			}
		});
		
		try {
			for (int i = 0; i < 2; i++) { //number of Wockets	
				outputStream = new BufferedOutputStream(new FileOutputStream(outPath + "acc_" + numFormat.format(i) + ".csv" ,true));
				for (int j = 0; j < days.length; j++) {
					String datePath = outPath + days[j] + "/";
					File hourFile = new File(datePath);
					hours = hourFile.list(new FilenameFilter() {							
						@Override
						public boolean accept(File dir, String filename) {
							return (filename.matches("\\d{2}") || filename.matches("\\d{1}"));						
						}
					});	
					for (int k = 0; k < hours.length; k++) {
						final String id = numFormat.format(i);													
						String accPath = inPath + days[j] + "/" + String.format("%02d", Integer.parseInt(hours[k])) + "/";
						File accFile = new File(accPath);
						String[] accNames = accFile.list(new FilenameFilter() {								
							@Override
							public boolean accept(File dir, String filename) {
								return filename.contains("Wocket." + id) && filename.contains(".csv"); 
								//return filename.contains("Wocket_" + id) && filename.contains(".csv"); //9 and after
							}
						});
						bufferdReader = new BufferedReader(new FileReader(accPath + accNames[0]));
						String line = null;
						while ((line = bufferdReader.readLine()) != null) {
							outputStream.write((line + "\n").getBytes());
						} 
						outputStream.flush();
						if(bufferdReader != null)
							bufferdReader.close();						
					}
				}
				
				if(outputStream != null) {
					outputStream.flush();
					outputStream.close();
				}
				System.out.println("Marged acc files of Wocket 0" + i);					
			}
		} catch (Exception e) {
			// TODO: handle exception
		}			
							
	}
	
	// -----------------------------------------------------------------
	public static void agRaw2agTimed(String inPath, String inName, String outPath, String outName) {

		SimpleDateFormat mHealthFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss.SSS");
		SimpleDateFormat dateFormat = new SimpleDateFormat("M/d/yyyy HH:mm:ss");
		String label = "TIME_STAMP,X,Y,Z";
		BufferedOutputStream outputStream = null;
		BufferedReader in = null;
		String line = null;
		Date tStamp;
		String startTime = null;
		String[] tokens;
		int samplingRate;
		int offset = 0;

		try {
			File outputFile = new File(outPath + outName);
			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}

			outputStream = new BufferedOutputStream(new FileOutputStream(outputFile, true));
			outputStream.write((label + "\n").getBytes());
			File inputFile = new File(inPath + inName);
			if (!inputFile.exists()) {
				System.out.println("Input file doesn't exist.");
			}
			in = new BufferedReader(new FileReader(inPath + inName));
			for (int i = 0; i < 10; i++) {
				line = in.readLine();
				if (i == 0) {
					samplingRate = Integer.parseInt(line.substring(
							line.indexOf("Hz") - 3, line.indexOf("Hz") - 1));
					System.out.println("samplingRate: " + samplingRate);
					offset = 1000 / samplingRate;
				}
				if (i == 2) { // Start Time
					tokens = line.split(" ");
					startTime = tokens[2];
				}
				if (i == 3) { // Start Date
					tokens = line.split(" ");
					startTime = tokens[2] + " " + startTime;
				}
			}

			tStamp = dateFormat.parse(startTime);
			while ((line = in.readLine()) != null) {
				line = mHealthFormat.format(tStamp) + "," + line;
				outputStream.write((line + "\n").getBytes());
				tStamp.setTime(tStamp.getTime() + offset);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			if (in != null)
				in.close();
			if (outputStream != null) {
				outputStream.flush();
				outputStream.close();
			}
		} catch (IOException e) {
			System.out.println("Can not close file stream." + e.toString());
		}
	}   
	
	// -----------------------------------------------------------------
	public static void agTimed2agCount(String inPath, String inName, String outPath, String outName, int sr) {

		SimpleDateFormat mHealthFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String label = "TIME_STAMP,VALUE";
		BufferedOutputStream outputStream = null;
		BufferedReader in = null;
		String line = null;
		String[] tokens;
		double[][] tempData = new double [3][sr];
		int index = 0;
		double acCount = 0;
		int prevMin = -1;
		int currentMin = -1;
		boolean full = false; //true if tempData is full; it will be true after reading sr samples
		
		try {
			File outputFile = new File(outPath + outName);
			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}

			outputStream = new BufferedOutputStream(new FileOutputStream(outputFile, true));
			outputStream.write((label + "\n").getBytes());
			File inputFile = new File(inPath + inName);
			if (!inputFile.exists()) {
				System.out.println("Input file doesn't exist.");
			}
			in = new BufferedReader(new FileReader(inPath + inName));
			
			while ((line = in.readLine()) != null) {
				tokens = line.split(",");
				if (!isDouble(tokens[1]))
					continue;
				currentMin = mHealthFormat.parse(tokens[0]).getMinutes();
				if (prevMin == -1)
					prevMin = currentMin;
				if (currentMin == prevMin) {
					
					if (index == sr) {
						index = 0;
						if (!full) 
							full = true;
					}
					if (full) {
						double mean = 0;				
						for (int i = 0; i < 3; i++) {
							for (int j = 0; j < sr; j++)
								mean += tempData[i][j];
							mean /= sr;	
							acCount += Math.abs(mean - Double.parseDouble(tokens[i+1]));
						}						
					}
					for (int i = 0; i < 3; i++) {
						tempData[i][index] = Double.parseDouble(tokens[i+1]);
					}
					index++;
					
				} else {
					acCount = (acCount + 6) * 3.5;
					outputStream.write((tokens[0] + "," + acCount + "\n").getBytes());
					acCount = 0;
					prevMin = currentMin;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			if (in != null)
				in.close();
			if (outputStream != null) {
				outputStream.flush();
				outputStream.close();
			}
		} catch (IOException e) {
			System.out.println("Can not close file stream." + e.toString());
		}
	}   
	
	// -----------------------------------------------------------------
	static boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
		}
		return false;
	}
	// -----------------------------------------------------------------
	static boolean isDouble(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (NumberFormatException e) {
		}
		return false;
	}

}
