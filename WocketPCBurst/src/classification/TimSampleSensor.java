package classification;

import java.io.*;
import java.net.*;
import java.sql.Timestamp;

public class TimSampleSensor {
	public static final String SERVER = "localhost";
	public static final int PORT = 6969;

	OutputStream outs;

	public TimSampleSensor() throws Exception {
		// Connect to server.
		Socket server = new Socket(SERVER, PORT);
		outs = server.getOutputStream();
		sendEvent("<READY/>");
	}

	public String makeEvent(int eventType) {
		java.util.Date date = new java.util.Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		switch (eventType) {
		case 0:
			return "<WOCKET_EVENT  TYPE=�SLEEP� TIME=\"" + timestamp + "\"/>";
		case 1:
			return "<WOCKET_EVENT  TYPE=�AWAKE� TIME=\"" + timestamp + "\"/>";
		case 2:
			return "<WOCKET_EVENT  TYPE=�ABSENT� TIME=\"" + timestamp + "\"/>";
		case 3:
			return "<WOCKET_EVENT  TYPE=�PRESENT� TIME=\"" + timestamp + "\"/>";
		default:
			return "";
		}
		
	}

	public void sendEvent(String event) throws Exception {
		outs.write((event.replace('\n', ' ') + "\n").getBytes());
		outs.flush();
	}

	public void serialEvent() throws Exception {
		// ...stuff...
		short eventType = 0;
		sendEvent(makeEvent(eventType));
	}

	/*public static void main(String[] argv) {
		try {
			TimSampleSensor sensor = new TimSampleSensor();
			try {
				Thread.sleep(1000);
			} catch (Exception x) {
			}
			;
			sensor.serialEvent();
		} catch (Exception e) {
			System.out.println("Error starting: " + e);
		}
	}*/

}
