package classification;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Classifier {
	
	static int SIZE = 21000;//3000;
	static String[] timeStamps = new String[SIZE];
	static double[][] data = new double [SIZE][13]; //8 NUMDATA,AVERAGE,VARIANCE,ACC,MU,SIGMA,LOGACT,NAT
	static int[] psgLabels = new int[SIZE];
	static int dSize;	
	//--------------------------------------------------------------------------
	public static void readDB(String path) {		
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(path));
			String line;
			line = in.readLine();
			int t = 0;
			while ((line = in.readLine()) != null) {
				String[] tokens = line.trim().split(",");
				timeStamps[t] = tokens[0];
				for (int i = 1; i < tokens.length - 1; i++){
					data[t][i-1] = Double.parseDouble(tokens[i]);
				}
				psgLabels[t] = Integer.parseInt(tokens[tokens.length - 1]);
				t++;
			}
			dSize = t;
			in.close();	
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	//--------------------------------------------------------------------------
	public static int[] threshold() {
		int[] labels = new int[dSize];
		for (int i = 0; i < dSize; i++) {
			int label = -1;
			/*if (data[i][2] == -1) //variance
				label = (data[i][3] < 500) ? 0 : 1; //unknown
			else */if (data[i][3] < 285)
				label = 0; //sleep
			else 
				label = 1; //Wake
			labels[i] = label;
		}
		return labels;
	}
	//--------------------------------------------------------------------------
	public static int[] sadeh() {
		int[] labels = new int[dSize];
		//constant, MU, SIGMA, LOGACT,NAT
		//original
		//double[] COEF = { 7.601, -0.065, -0.056, -0.0703, -1.08};
		//Wocket wrirt weka
		double[] COEF = {4.5259, -0.0079, -0.0025, -0.6936, -0.2398};
		//Wocket Ankle Weka
		//double[] COEF = {3.0396, -0.0049, -0.0029, -0.5444, -0.3629};
		//Actigraph wrist
		//double[] COEF = {6.4524, -0.0037, -0.003, -1.19, -0.3001};
		//Actigraph ankle
		//double[] COEF = {6.5209, -0.023, -0.0049, -1.3208, 0.0865};

		for (int i = 0; i < dSize; i++) {
			double si = COEF[0];
			for (int j = 1; j < 5; j++) {
				si += COEF[j] * data[i][j+3];
			}
			double	psi = 1 / (1 + Math.exp(-si));
			labels[i] = (psi < 0.5) ? 1 : 0;
			//labels[i] = (si < 0.5) ? 0 : 1; //linear
		}
		
		return labels;
	}
		
	//--------------------------------------------------------------------------
	public static int[] sazonov() { //-8..0
		int[] labels = new int[dSize];
		//original					cons	-8		-7		-6			-5			-4			-3		-2		-1			0
		//double[] COEFFICIENT = {1.99604, -0.10207, -0.073, -0.07494, -0.08108, -0.08917, -0.10194, -0.09975, -0.09746, -0.1945};
		//Weka Wocket Wrist
		double[] COEFFICIENT = {2.6493, -0.0009, -0.0001, -0.0005, -0.0003, -0.0006, -0.0005, -0.0011, -0.0013, -0.002};
		//Weka Wocket Ankle
		//double[] COEFFICIENT = {3.1253, -0.001, -0.0001, -0.0006, -0.0004, -0.0009, -0.0008, -0.0017, -0.0024, -0.0026};
		//double[] COEFFICIENT = {5.1929,-0.0016,0.0005,-0.0009,0.0001,-0.0015,-0.0005,-0.0023,-0.0022,-0.0053}; //on DB09_01
		double si = 0;
		double psi = 0;
		
		for (int i = 8; i < dSize; i++) {
		//for (int i = 0; i < dSize; i++) {
			si = COEFFICIENT[0];
			for (int j = 1; j < 10; j++) 
				si += COEFFICIENT[j] * data[i+j-9][3];
				//si += COEFFICIENT[j] * data[i][j+1]; 
			psi = 1 / (1 + Math.exp(-si));
			labels[i] = (psi < 0.5) ? 1 : 0;
		}	
				
		return labels;
	}	
	
	//--------------------------------------------------------------------------
		public static int[] kripke() { //-10..2
			int[] labels = new int[dSize];
			//sazonov W
			//double[] COEFFICIENT = {2.6493, 0, 0, -0.0009, -0.0001, -0.0005, -0.0003, -0.0006, -0.0005, -0.0011, -0.0013, -0.002, 0, 0}; 
			//sazonov A
			//double[] COEFFICIENT = {3.1253, 0, 0, -0.001, -0.0001, -0.0006, -0.0004, -0.0009, -0.0008, -0.0017, -0.0024, -0.0026, 0, 0};
			//original					-10 	-9		-8		-7		-6		-5			
			/*double[] COEFFICIENT = {0.0064, 0.0074, 0.0112, 0.0112, 0.0118, 0.0118,
			 // 						-4		-3		-2		-1		0	1    	2
									0.0128, 0.0188, 0.0280, 0.0664, 0.0300, 0.0112, 0.100};*/
			//Wocket Wrist
			double[] COEFFICIENT = {2.8873, -0.0007, -0.0001, -0.0005, -0.0001, -0.0004, -0.0003,
					-0.0005, -0.0005, -0.0009, -0.0013, -0.0015, -0.0002, -0.0006};
			//Wocket Ankle
			/*double[] COEFFICIENT = {3.3773, -0.0008, -0.0002, -0.0004, -0.0002, -0.0004, -0.0005,
					-0.0006, -0.001, -0.0014, -0.0026, -0.0017, -0.0006, -0.0005};*/
			//Acti Wrist
			/*double[] COEFFICIENT = {1.3266, -0.0035, 0, -0.0003, -0.0002, -0.0004, -0.0002, -0.0001,
					0, -0.0001, -0.0001, -0.0001, -0.0001, 0};*/
			//Acti Ankle
			/*double[] COEFFICIENT = {3.3434, -0.0005, -0.0003, -0.0004, -0.0001, -0.0005, -0.0008, 
					-0.0005, -0.0004, -0.0026, -0.0053, -0.0015, 0.0002, -0.001};*/
			//for (int i = 0; i < dSize; i++) {
			for (int i = 10; i < dSize - 2; i++) {
				double si = COEFFICIENT[0];
				for (int j = 1; j < 14; j++) 
					//si += COEFFICIENT[j] * data[i][j-1]; 
					si += COEFFICIENT[j] * data[i+j-11][3];
				double	psi = 1 / (1 + Math.exp(-si));
				labels[i] = (psi < 0.5) ? 1 : 0;
			}		
			//Kriple postscoring 1
			/*int cnt = 0;
			for (int i = 0; i < dSize; i++) {
				if (labels[i] == 1) {
					for (int j = 1; j <= cnt; j++) {
						labels[i-j] = 1;
					}					
					cnt = 0;
				} else {
					cnt++;
				}
				if (cnt == 10)
					break;
			}*/
			//Kriple postscoring 2
			int cnt = 0;
			 for (int i = 0; i < dSize; i++) {
				if (labels[i] == 1) {
					if ((cnt > 0) && (cnt < 10)) {
						for (int j = 1; j <= cnt; j++) {
							labels[i-j] = 1;
						}		
					}
					
					cnt = 0;
				} else {
					cnt++;
				}
			}
			//Cole (Webster) postscoring
			/*int scnt = 0;
			int wcnt = 0;
			int dcnt = 0;
			int ecnt = 0;
			boolean dFlag = false;
			boolean eFlag = false;
			for (int i = 0; i < dSize; i++) {
				if (labels[i] == 1) {
					wcnt++;
					scnt = 0;
				} 
				if (labels[i] == 0) {
					scnt++;
					//e
					if (wcnt >= 40) {
						if (eFlag) {
							for (int j = 1; j <= ecnt; j++) {
								labels[i - wcnt - j] = 1;
							}
							ecnt = 0;
							eFlag = false;
							wcnt = 0;
						}
						else if (scnt > 20) {
								wcnt = 0;
								scnt = 0;
								eFlag = false;
						} else {
							eFlag = true;
							ecnt = scnt;
						}
					}
					//c
					if ((wcnt >= 30) && (scnt < 9)) {
						labels[i] = 1;
						if (scnt == 8) {
							wcnt = 0;
							scnt = 0;
						}
					}
					//b
					else if (wcnt >= 20) {
						//d 
						if (dFlag) {
							for (int j = 1; j <= dcnt; j++) {
								labels[i - wcnt - j] = 1;
							}
							dcnt = 0;
							dFlag = false;
							wcnt = 0;
						}
						else if (scnt < 7) {					
							labels[i] = 1;
						} else if (scnt >= 13) {
								wcnt = 0;
								scnt = 0;
								dFlag = false;
						} else {
							dFlag = true;
							dcnt = scnt;
						}
					}
					//a
					else if ((wcnt >= 8) && (scnt < 3)) {
						labels[i] = 1;
						if (scnt == 2) {
							wcnt = 0;
							scnt = 0;
						}
					}					
				}				
			}*/
			
			return labels;
		}	
			
	//--------------------------------------------------------------------------	
	public static void evaluate(int[] myLabels, int[] goldLabels) {
		double fp = 0;
		double fn = 0;
		double tp = 0;
		double tn = 0;
		
		for (int i = 0; i < myLabels.length; i++) {
			
			if (goldLabels[i] == 4) {//wake	//4					
				if (myLabels[i] == 1) {
					tn++;	//true Wake
				} else {
					fp++;	//false Sleep
				} 
			} else {
				if (myLabels[i] == 0) {
					tp++;	//true Sleep
				} else {
					fn++;	//false Wake
				}
			}
						
		}			
				
		double Sen = tp / (tp + fn);
		double Spe = tn / (tn + fp);
		double Acc = (tp + tn) / (fp + fn + tp + tn);
		double PPV = tp / (tp + fp);
		//double NPV = tn / (tn + fn);
		System.out.println("Sen(Recall), Spe, Acc, (sen + spe), precision, F-Measure, MCC");
		NumberFormat nFormat = new DecimalFormat("0.#####");
		System.out.println(nFormat.format(Sen) + ", " + nFormat.format(Spe) + 
				", " + nFormat.format(Acc) + ", " + nFormat.format(Sen + Spe) +
				", " + nFormat.format(PPV) + ", " + nFormat.format(2 * PPV * Sen / (PPV + Sen)) +
				", " + nFormat.format(((tp * tn) - (fp * fn)) / Math.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))));
		System.out.println("\ntp: " + tp + "\tfn: " + fn);
		System.out.println("fp: " + fp + "\ttn: " + tn + "\n");
	}
	//-----------------------------------------------------------------------------------
	public static void classify(String path, String dbName, String wId, int algorithm) {		
		readDB(path + dbName);
		int[] labels = null;
		
		switch (algorithm) {
		case 1:
			labels = sadeh();
			break;
		case 2:
			labels = threshold();
			break;
		case 3:			
			labels = sazonov();
			break;
		case 4:			
			labels = kripke();
			break;			
		default:
			break;
		}
		
		//Modifying labels
		/*for (int i = 1; i < dSize - 1; i++) {
			if ((labels[i] == 0) && (labels[i - 1] == 1) && (labels[i + 1] == 1)) {
				labels[i] = 1;
			}
		}*/
		evaluate(labels, psgLabels);

		String fileName = path + "Result_" + wId + ".csv";
		String fileLabel = "TIME_STAMP,LABEL";		 
	    PrintWriter out = null;
	    try {
	    	File outputDir = new File(path);
			if (!outputDir.isDirectory()) {
				outputDir.mkdirs();
			} 
		    File f = new File(fileName);
		    if (f.exists()) {	
		    	f.delete(); 
		    }
		    f.createNewFile();
	    	out = new PrintWriter(new FileWriter(fileName,true));
	    	out.append(fileLabel + "\n");
		    
	        for (int m = 0; m < dSize; m++) {
				String msg =  timeStamps[m] + "," + labels[m];
				out.append(msg + "\n");
			}	        
	    } catch (Exception e) {
	    	out.append("\n");
	    } finally {
	    	out.flush();
	        if (out != null)
	        	out.close();
	    }
	    
		//Converter.csv2Xml(path, "Result.csv");
	}
	
}
