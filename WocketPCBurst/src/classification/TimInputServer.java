package classification;

import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;

public class TimInputServer extends Frame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int PORT = 6969;

	public static void write(String s, OutputStream outs) throws Exception {
		System.out.println("<<<WRITE: " + s);
		outs.write((s.replace('\n', ' ') + "\n").getBytes());
		outs.flush();
	}

	public static String read(BufferedReader ins) throws Exception {
		String line = ins.readLine();
		System.out.println(">>>READ: " + line);
		return line;
	}

	public TextArea inputText;
	public Button sendButton;

	public TimInputServer() {
		super("EA Input Server");
		inputText = new TextArea(4, 50);
		sendButton = new Button("SEND");
		// setLayout(new BorderLayout());
		add("Center", inputText);
		add("South", sendButton);

		sendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (outs != null)
					try {
						write(inputText.getText(), outs);
					} catch (Exception e) {
						System.out.println("write ex: " + e);
					}
				;
			}
		});

		pack();
	}

	static OutputStream outs = null;

	public static void main(String[] a) {
		if (a.length == 0)
			PORT = 6969;
		else
			PORT = Integer.parseInt(a[0]);
		ServerSocket server = null;
		try {
			server = new ServerSocket(PORT);
		} catch (Exception e) {
			System.out.println("Ex: " + e);
			System.exit(42);
		}
		;
		new TimInputServer();
		while (true) {
			try {
				System.out.println("Waiting for connection on " + PORT);
				Socket client = server.accept();
				System.out.println("Got a client...");
				outs = client.getOutputStream();
				InputStream ins = client.getInputStream();
				InputStreamReader reader = new InputStreamReader(ins);
				BufferedReader bufr = new BufferedReader(reader);
				// Just eat inputs...
				String line;
				while (true) {
					line = read(bufr);
					if (line == null)
						break;
				}
				;
			} catch (Exception e) {
				System.out.println("Ex: " + e);
				outs = null;
			}
		}
	}
}
