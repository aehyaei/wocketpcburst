package classification;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class dbProducer {
	
	static String sadehlabel = "acc,mu,sigma,logAct,nat";
	static String kripkelabel = "minus10,minus9,minus8,minus7,minus6,minus5,minus4,minus3,minus2,minus1,self,plus1,plus2";
	static String sazanovlabel = "minus8,minus7,minus6,minus5,minus4,minus3,minus2,minus1,self";
	static String[] featureTypes = {"sadeh","kripke","sazonov"};
	static String[] features = {sadehlabel, kripkelabel, sazanovlabel};
	static String[] tasks = {"Sleep","Wake"};
	
	static NumberFormat numFormat = new DecimalFormat("00");	
	
	public static void createDBs() {
		String path = "C:/Users/Aida/Data/CIMIT sleep Study_processed/test/Result_leaveOut/";
		String csvPath = path + "csv/";
		String arffPath = path + "arff/";
		//create leave one out DBs
		//createTestDB(csvPath);
		for (int i = 0; i < featureTypes.length; i++) {
			//createTrainDB(featureTypes[i],features[i] + ",s/w",csvPath);
			String label = arrfLabeler(features[i].split(","), tasks);
			createTestArffDB(label, featureTypes[i], csvPath, arffPath);
			createTrainArffDB(label, featureTypes[i], csvPath, arffPath);			
		}
	}
	
	public static void createTestDB(String path) {
		
		for (int j = 2; j < 13; j++) {//subject number
			if (j != 8) {
				String inPath = "C:/Users/Aida/Data/CIMIT sleep Study_processed/test/HB" + numFormat.format(j) + "/Wocket/";			
				PrintWriter out = null;
				try {
					for (int k = 0; k < 2; k++) {
						String fileName =  "DB" + numFormat.format(j) +  "_" + numFormat.format(k);
						ArrayList<String> dataArray = new ArrayList<String>();
						readFile(inPath + fileName + ".csv", dataArray);
						
				    	out = new PrintWriter(new FileWriter(path + fileName + "_sadeh_test.csv"));	
				    	out.println(sadehlabel + ",s/w");
						for (int cnt = 10; cnt < dataArray.size() - 2; cnt++) {	
							String[] temp = dataArray.get(cnt).split(",");
							String msg = "";
							for (int m = 4; m < 9; m++) {
								msg += temp[m] + ",";								
							}
							msg += (dataArray.get(cnt).split(",")[9].equals("4") ? "Wake" : "Sleep");
							out.println(msg);
						}
						out.flush();
						out.close();
						
						out = new PrintWriter(new FileWriter(path + fileName + "_kripke_test.csv"));	
						out.println(kripkelabel + ",s/w");
						for (int cnt = 10; cnt < dataArray.size() - 2; cnt++) {	
							String msg = "";
							for (int m = -10; m < 3; m++) {
								msg += dataArray.get(cnt + m).split(",")[4] + ",";						
							}
							msg += (dataArray.get(cnt).split(",")[9].equals("4") ? "Wake" : "Sleep");
							out.println(msg);
						}
						out.flush();
						out.close();
						
						out = new PrintWriter(new FileWriter(path + fileName + "_sazonov_test.csv"));
						out.println(sazanovlabel + ",s/w");
						for (int cnt = 10; cnt < dataArray.size() - 2; cnt++) {	
							String msg = "";
							for (int m = -8; m < 1; m++) {
								msg += dataArray.get(cnt + m).split(",")[4] + ",";								
							}
							msg += (dataArray.get(cnt).split(",")[9].equals("4") ? "Wake" : "Sleep");
							out.println(msg);
						}
						out.flush();
						out.close();	
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (out != null) {
						out.close();
					}
				}				
				
			} //if				
		} // for		
	}
	
	
	public static void createTrainDB(String key, String label, String path) {
		NumberFormat numFormat = new DecimalFormat("00");
		for (int j = 2; j < 13; j++) {//subject number
			if (j != 8) {
				PrintWriter out = null;
				try {
					for (int k = 0; k < 2; k++) {
						String fileName =  "DB" + numFormat.format(j) +  "_" + numFormat.format(k);						
						out = new PrintWriter(new FileWriter(path + fileName + "_" + key +"_train.csv"));
						out.println(label);
						for (int m = 2; m < 13; m++) {
							if (m != j && m!= 8) {
								fileName =  "DB" + numFormat.format(m) +  "_" + numFormat.format(k);	
								ArrayList<String> dataArray = new ArrayList<String>();
								readFile(path + fileName + "_" + key + "_test.csv", dataArray);
								for (int cnt = 0; cnt < dataArray.size(); cnt++) {								
									out.append(dataArray.get(cnt) + "\n");
								}
								out.flush();
							}
						}
						out.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (out != null) {
						out.close();
					}
				}
			} //if				
		} // for			
	}
	
	
	public static String arrfLabeler(String[] features, String[] tasks){
		String label = "@RELATION iris\n\n";
		for (String s: features) {
			label += "@ATTRIBUTE " + s + " NUMERIC\n";
		}
		label += "@ATTRIBUTE Task {" + tasks[0];
		for (int j = 1; j < tasks.length; j++) {
			label += "," + tasks[j];
		}
		label += "}\n\n@DATA\n";
		return label;
	}
	
	
	public static void createTestArffDB(String label, String fType, String inPath, String outPath) {
		PrintWriter out = null;
		try {	
			for (int j = 2; j < 13; j++) {//subject number
				if (j != 8) {
					for (int k = 0; k < 2; k++) {
						String fileName =  "DB" + numFormat.format(j) +  "_" + numFormat.format(k) + "_" + fType + "_test";
				    	out = new PrintWriter(new FileWriter(outPath + fileName + ".arff"));	
				    	out.println(label);
				    	ArrayList<String> dataArray = new ArrayList<String>();
						readFile(inPath + fileName + ".csv", dataArray);
						for (int cnt = 0; cnt < dataArray.size(); cnt++) {								
							out.println(dataArray.get(cnt));
						}
						//out.println();
						out.flush();
						out.close();
					}
				}
			}						
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}	
	}
	
	
	public static void createTrainArffDB(String label, String fType, String inPath, String outPath) {	
		PrintWriter out = null;
		try {	
			for (int j = 2; j < 13; j++) {//subject number
				if (j != 8) {
					for (int k = 0; k < 2; k++) {
						String fileName =  "DB" + numFormat.format(j) +  "_" + numFormat.format(k) + "_" + fType + "_train";	
						out = new PrintWriter(new FileWriter(outPath + fileName + ".arff"));
						out.println(label);
						for (int m = 2; m < 13; m++) {
							if (m != j && m!= 8) {
								ArrayList<String> dataArray = new ArrayList<String>();
								readFile(inPath + fileName + ".csv", dataArray);
								for (int cnt = 0; cnt < dataArray.size(); cnt++) {								
									out.println(dataArray.get(cnt));
								}
								out.flush();
							}
						}
						out.close();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}		
	}
	
	public static void createTestMergedArffDB(String label, String fType, String inPath, String outPath) {
		PrintWriter out = null;
		ArrayList<ArrayList<String>> dataArray = new ArrayList<ArrayList<String>>(2);
		try {	
			for (int j = 2; j < 13; j++) {//subject number
				if (j != 8) {
					for (int k = 0; k < 2; k++) {
						String fileName =  "DB" + numFormat.format(j) +  "_" + numFormat.format(k) + "_" + fType + "_test";
				    	out = new PrintWriter(new FileWriter(outPath + fileName + ".arff"));	
				    	out.println(label);				    	
						readFile(inPath + fileName + ".csv", dataArray.get(k));
					}

					for (int cnt = 0; cnt < dataArray.size(); cnt++) {								
						out.println(dataArray.get(cnt));
					}
					//out.println();
					out.flush();
					out.close();
				}
			}						
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}	
	}
	
	
	
	
	
	
			
	public static void readFile(String fileName, ArrayList<String> dataArray) {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(fileName));
			if (in != null) {
				String line= in.readLine();//ignor the first line which is label						
				while ((line = in.readLine()) != null) {
					dataArray.add(line);
				}
				in.close();
			}
		} catch (FileNotFoundException e) {
			System.out.println("File does not exist: " + fileName);
		}	catch (IOException e) {
			e.printStackTrace();
		}			
	}
	
}
