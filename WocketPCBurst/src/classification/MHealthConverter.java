package classification;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;


public class MHealthConverter {

	//private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private static SimpleDateFormat mHealthFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private static SimpleDateFormat genFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
	private static SimpleDateFormat gpsFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	private static SimpleDateFormat omronFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat tempFormat = new SimpleDateFormat("MM/dd/yyyy HH");
	private static NumberFormat hourFormat = new DecimalFormat("00");
	//private static String localPath = "/Users/aidaehyaei/Documents/sleepStudy/"; //MAC
	//private static String localPath = "c:/sleepStudy/test/"; //Windows

	
	// -----------------------------------------------------------------
	public static void gps2mHealth(String path, String fileName) {
		
		String outFileName = path + "mhealth_" + fileName;
		String label;
		BufferedReader in = null;
		String line = null;

		File outputDir = new File(path);
		if (!outputDir.isDirectory()) {
			outputDir.mkdirs();
		}
		File f = new File(outFileName);
		PrintWriter out = null;

		try {
			if (f.exists())
				f.delete();
			f.createNewFile();
			
			out = new PrintWriter(new FileWriter(outFileName, true));
			in = new BufferedReader(new FileReader(path + fileName));
			label = "TIME_STAMP,RCR,UTC_DATE,UTC_TIME,LOCAL_DATE,LOCAL_TIME,MS,VALID,LATITUDE,N/S,LONGITUDE,E/W,HEIGHT,SPEED,Distance";
			out.append(label + "\n");
			
			while ((line = in.readLine()) != null) {
				String msg = null;
				String[] tokens = line.split(",");
				if ((tokens.length != 0) && isInteger(tokens[0].trim())) {
					String tStamp = null;
					try {
						tStamp = mHealthFormat.format(gpsFormat.parse(tokens[4].trim() + " " + tokens[5].trim()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}					
					msg = tStamp + "," + line;
					out.append(msg + "\n");
				}
			}
			if (in != null) {
				in.close();
			}
			out.flush();
			if (out != null) {
				out.close();
			}
			System.out.println("The gps file is converted to mHealth format!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// -----------------------------------------------------------------
	public static void gen2mHealth(String path, String fileName) {
		
		String outFileName = path + "mhealth_" + fileName;
		String label = "TIME_STAMP,X,Y,Z,LUX,EVENT,TEMPERATURE" ;
		BufferedReader in = null;
		String line = null;

		File outputDir = new File(path);
		if (!outputDir.isDirectory()) {
			outputDir.mkdirs();
		}
		File f = new File(outFileName);
		PrintWriter out = null;

		try {
			if (f.exists())
				f.delete();
			f.createNewFile();
			
			out = new PrintWriter(new FileWriter(outFileName, true));
			in = new BufferedReader(new FileReader(path + fileName));
			out.append(label + "\n");
			
			while ((line = in.readLine()) != null) {
				String msg = null;
				String[] tokens = line.split(",");
				if ((tokens.length != 0) && isGenDate(tokens[0].trim())) {
					String tStamp = null;
					try {
						tStamp = mHealthFormat.format(genFormat.parse(tokens[0].trim()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					msg = tStamp;
					for(int i = 1; i < tokens.length; i++)
				        msg = msg + "," + tokens[i];
					out.append(msg + "\n");
				}
			}
			if (in != null) {
				in.close();
			}
			out.flush();
			if (out != null) {
				out.close();
			}
			System.out.println("The gen file is converted to mHealth format!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	// -----------------------------------------------------------------
		public static void omron2mHealth(String path, String fileName) {
			
			String outFileName = path + "mhealth_" + fileName;
			String label = "TIME_STAMP,STEPS,AEROBIC_STEPS,USED,EVENT,TOTAL_STEPS,AEROBIC_STEPS,AEROBIC_WALKING_ TIME,CALORIES,DISTANCE,FAT_BURNED";
			BufferedReader in = null;
			String line = null;

			File outputDir = new File(path);
			if (!outputDir.isDirectory()) {
				outputDir.mkdirs();
			}
			File f = new File(outFileName);
			PrintWriter out = null;

			try {
				if (f.exists())
					f.delete();
				f.createNewFile();
				
				out = new PrintWriter(new FileWriter(outFileName, true));
				in = new BufferedReader(new FileReader(path + fileName));
				out.append(label + "\n");
				
				while ((line = in.readLine()) != null) {
					String msg = "";
					String[] tokens = line.split(",");
					if ((tokens.length != 0) && isOmronDate(tokens[0].trim())) {
						String tStamp = null;
						
						for(int k = 0; k < 24; k++) {							
							try {
								tStamp = mHealthFormat.format(tempFormat.parse(tokens[0].trim() + " " + hourFormat.format(k)));
							} catch (ParseException e) {
								e.printStackTrace();
							}
							msg = tStamp;							
							for(int i = 0; i < 4; i++) {
								msg += "," + tokens[(24*i)+7+k];
							}
							if (k == 0) 
								for(int i = 1; i < 7; i++)
							        msg += "," + tokens[i];
							out.append(msg + "\n");
						}
					}
				}
				if (in != null) {
					in.close();
				}
				out.flush();
				if (out != null) {
					out.close();
				}
				System.out.println("The omron file is converted to mHealth format!");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	// -----------------------------------------------------------------
	static boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
		}
		return false;
	}
	// -----------------------------------------------------------------
	static boolean isDouble(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (NumberFormatException e) {
		}
		return false;
	}
	// -----------------------------------------------------------------
	static boolean isGenDate(String str) {
		try {
			genFormat.parse(str);
			return true;		
		} catch (ParseException e) {
		}
		return false;
	}
	// -----------------------------------------------------------------
	static boolean isOmronDate(String str) {
		try {
			omronFormat.parse(str);
			return true;		
		} catch (ParseException e) {
		}
		return false;
	}
}
