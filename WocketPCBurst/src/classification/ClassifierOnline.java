package classification;


public class ClassifierOnline {

	public static String threshold(int[] data) {
		String label;
		if (data[0] < 285)
			label = "Sleep";
		else 
			label = "Wake";
		return label;
	}
	//--------------------------------------------------------------------------
	public static String sadeh(int[] data) { //-5..5
		String label;
		double mu = 0;		//Mean activity on a 11-min window centered on current epoch
		double sigma = 0;	//standard deviation of activity for the last 6min
		double logAct = 0;	//natural log of the current epoch activity + 1
		double nat = 0;		//the number of epochs with (50 <= epoch activity < 100) on a 11-min window centered on current epoch. for Wocket, I consider epoch activity > 100.
		double mean = 0;
		for (int j = 0; j < 11; j++) {			 
			System.out.println("Sadeh data" + j + ":" + data[j]);
			mu += data[j]/5;
			if (data[j] > 480)					
				nat++;
			if (j == 5)
				mean = mu / 6;
		}
		mu /= 11;		
		for (int j = 0; j < 6; j++) {
			sigma += Math.pow((data[j]/5 - mean), 2);
		}
		sigma = Math.sqrt(sigma);
		logAct = Math.log(data[5]/5) + 1;

		double[] sadehParam = {1, mu, sigma, logAct, nat};
		//System.out.println("mu: "+ mu + " sigma: "+ sigma + " logAct: " + logAct + " nat: " + nat);
		//original		constant, MU, 	SIGMA, 	LOGACT,		NAT
		double[] COEF = { 7.601, -0.065, -0.056, -0.0703, -1.08};
		//Wocket wrist weka
		//double[] COEF = {4.5259, -0.0079, -0.0025, -0.6936, -0.2398};
		//Wocket Ankle Weka
		//double[] COEF = {3.0396, -0.0049, -0.0029, -0.5444, -0.3629};
		//Actigraph wrist
		//double[] COEF = {6.4524, -0.0037, -0.003, -1.19, -0.3001};
		//Actigraph ankle
		//double[] COEF = {6.5209, -0.023, -0.0049, -1.3208, 0.0865};

		double si = 0;
		for (int j = 1; j < 5; j++) {
			si += COEF[j] * sadehParam[j];
		}
		//System.out.println("si: "+si);
		double	psi = 1 / (1 + Math.exp(-si));
		//System.out.println("psi: " + psi);
		label = (psi < 0.5) ? "Wake" : "Sleep";
		System.out.println("sadeh: " + label);		
		
		return label;
	}
		
	//--------------------------------------------------------------------------
	public static String sazonov(int[] data) { //-8..0
		String label;
		//original					cons	-8		-7		-6			-5			-4			-3		-2		-1			0
		//double[] COEFFICIENT = {1.99604, -0.10207, -0.073, -0.07494, -0.08108, -0.08917, -0.10194, -0.09975, -0.09746, -0.1945};
		//Weka Wocket Wrist
		double[] COEFFICIENT = {2.6493, -0.0009, -0.0001, -0.0005, -0.0003, -0.0006, -0.0005, -0.0011, -0.0013, -0.002};
		//Weka Wocket Ankle
		//double[] COEFFICIENT = {3.1253, -0.001, -0.0001, -0.0006, -0.0004, -0.0009, -0.0008, -0.0017, -0.0024, -0.0026};
		double si = 0;
		double psi = 0;
		si = COEFFICIENT[0];
		for (int j = 1; j < 10; j++) {
			si += COEFFICIENT[j] * data[j-1];
		}
		psi = 1 / (1 + Math.exp(-si));
		label = (psi < 0.5) ? "Wake" : "Sleep";		
		return label;
	}	
	
	//--------------------------------------------------------------------------
	public static String kripke(int[] data) { //-10..2
		String label;
		//sazonov W
		//double[] COEFFICIENT = {2.6493, 0, 0, -0.0009, -0.0001, -0.0005, -0.0003, -0.0006, -0.0005, -0.0011, -0.0013, -0.002, 0, 0}; 
		//sazonov A
		//double[] COEFFICIENT = {3.1253, 0, 0, -0.001, -0.0001, -0.0006, -0.0004, -0.0009, -0.0008, -0.0017, -0.0024, -0.0026, 0, 0};
		//original					-10 	-9		-8		-7		-6		-5			
		/*double[] COEFFICIENT = {0.0064, 0.0074, 0.0112, 0.0112, 0.0118, 0.0118,
		 // 						-4		-3		-2		-1		0	1    	2
								0.0128, 0.0188, 0.0280, 0.0664, 0.0300, 0.0112, 0.100};*/
		//Wocket Wrist
		double[] COEFFICIENT = {2.8873, -0.0007, -0.0001, -0.0005, -0.0001, -0.0004, -0.0003,
				-0.0005, -0.0005, -0.0009, -0.0013, -0.0015, -0.0002, -0.0006};
		//Wocket Ankle
		/*double[] COEFFICIENT = {3.3773, -0.0008, -0.0002, -0.0004, -0.0002, -0.0004, -0.0005,
				-0.0006, -0.001, -0.0014, -0.0026, -0.0017, -0.0006, -0.0005};*/
		//Acti Wrist
		/*double[] COEFFICIENT = {1.3266, -0.0035, 0, -0.0003, -0.0002, -0.0004, -0.0002, -0.0001,
				0, -0.0001, -0.0001, -0.0001, -0.0001, 0};*/
		//Acti Ankle
		/*double[] COEFFICIENT = {3.3434, -0.0005, -0.0003, -0.0004, -0.0001, -0.0005, -0.0008, 
				-0.0005, -0.0004, -0.0026, -0.0053, -0.0015, 0.0002, -0.001};*/
		double si = COEFFICIENT[0];
		for (int j = 1; j < 14; j++) 
			si += COEFFICIENT[j] * data[j-1];
		double	psi = 1 / (1 + Math.exp(-si));
		label = (psi < 0.5) ? "Wake" : "Sleep";	
		System.out.println("kripke: " + label);
		return label;
	}				
	
	//-----------------------------------------------------------------------------------
	public static String classify(int[] data, int algorithm) {		
		String label = null;
		
		switch (algorithm) {
		case 1:
			label = sadeh(data);
			break;
		case 2:
			label = threshold(data);
			break;
		case 3:			
			label = sazonov(data);
			break;
		case 4:			
			label = kripke(data);
			break;			
		default:
			break;
		}
		
		return label;		
	}
	
}
