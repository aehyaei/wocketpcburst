package classification;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.NominalPrediction;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.PART;
import weka.classifiers.trees.DecisionStump;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.core.FastVector;
import weka.core.Instances;
 
public class WekaTest {
	public static BufferedReader readDataFile(String filename) {
		BufferedReader inputReader = null;
 
		try {
			inputReader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException ex) {
			System.err.println("File not found: " + filename);
		}
 
		return inputReader;
	}
 
	public static Evaluation classify(Classifier model,
			Instances trainingSet, Instances testingSet) throws Exception {
		Evaluation evaluation = new Evaluation(trainingSet);
 
		model.buildClassifier(trainingSet);
		evaluation.evaluateModel(model, testingSet);
 
		return evaluation;
	}
 
	public static double calculateAccuracy(FastVector predictions) {
		double correct = 0;
 
		for (int i = 0; i < predictions.size(); i++) {
			NominalPrediction np = (NominalPrediction) predictions.elementAt(i);
			if (np.predicted() == np.actual()) {
				correct++;
			}
		}
 
		return 100 * correct / predictions.size();
	}
	
	public static int[] calculateConfusionMatrix(FastVector predictions) {
		int[] cm = new int[4];
 
		for (int i = 0; i < predictions.size(); i++) {
			NominalPrediction np = (NominalPrediction) predictions.elementAt(i);
			if (np.predicted() == np.actual()) {
				if (np.predicted() == 0) {
					cm[0] += 1; //tp
				} else {
					cm[3] += 1; //tn
				}
			} else {
				if (np.predicted() == 1) {
					cm[1] += 1; //fp
				} else {
					cm[2] += 1; //fn
				}
			}
		}
		return cm;
	}
 
	public static Instances[][] crossValidationSplit(Instances data, int numberOfFolds) {
		Instances[][] split = new Instances[2][numberOfFolds];
 
		for (int i = 0; i < numberOfFolds; i++) {
			split[0][i] = data.trainCV(numberOfFolds, i);
			split[1][i] = data.testCV(numberOfFolds, i);
		}
 
		return split;
	}
 
	public static void main(String[] args) throws Exception {
		NumberFormat numFormat = new DecimalFormat("00");
		// Use a set of classifiers
		Classifier[] models = { 
				//new Logistic(),
				//new MultilayerPerceptron(),
				//new J48(), // a decision tree
				new RandomForest(),
				//new PART(), 
				//new DecisionTable(),//decision table majority classifier
				//new DecisionStump() //one-level decision tree
		};
		//int[] sensors = {1};
		int[] sensors = {0,1};
		int[] subjects = {2,3,4,5,6,7,9,10,11,12};
		//String[] featureTypes = {"sadeh","kripke","sazonov"};
		String[] featureTypes = {"kripke"};
		
		
		for (String fType: featureTypes) {
			for (Classifier model: models) {
				for (int sen: sensors) {
					int [] cmTotal = new int[4];
					for (int sbj: subjects) {
						String path = "C:/Users/Aida/Data/CIMIT sleep Study_processed/test/Result_leaveOut/arff/";
						String fileName = "DB" + numFormat.format(sbj) +  "_" + numFormat.format(sen) + "_" + fType ;	
						Instances train = new Instances(readDataFile(path + fileName + "_train.arff"));
						train.setClassIndex(train.numAttributes() - 1);
						Instances test = new Instances(readDataFile(path + fileName + "_test.arff"));
						test.setClassIndex(test.numAttributes() - 1);
						Evaluation validation = classify(model, train, test);
						FastVector predictions = new FastVector();
						predictions.appendElements(validation.predictions());		
						int [] cm = calculateConfusionMatrix(predictions);
						//System.out.println(cm[0] + "\t" + cm[1] + "\n" + cm[2] + "\t" + cm[3]);
						//System.out.println((double) cm[0]/(cm[0] + cm[1]) + "\t" + (double) cm[1]/(cm[0] + cm[1]) + "\n" + (double) cm[2]/(cm[2] + cm[3]) + "\t" + (double) cm[3]/(cm[2] + cm[3]));
						//double accuracy = calculateAccuracy(predictions);
						//System.out.println(accuracy);
						for (int i = 0; i < 4; i++) {
							cmTotal[i] += cm[i];
						}
					}
					System.out.println(fType + " " + (sen == 0 ? "Wrist" : "Ankle"));
					System.out.println(cmTotal[0] + "\t" + cmTotal[1] + "\n" + cmTotal[2] + "\t" + cmTotal[3]);
					System.out.println((double) cmTotal[0]/(cmTotal[0] + cmTotal[1]) + "\t" + (double) cmTotal[1]/(cmTotal[0] + cmTotal[1])
							+ "\n" + (double) cmTotal[2]/(cmTotal[2] + cmTotal[3]) + "\t" + (double) cmTotal[3]/(cmTotal[2] + cmTotal[3]));
					System.out.println((double) (cmTotal[0] + cmTotal[3])/(cmTotal[0] + cmTotal[1] + cmTotal[2] + cmTotal[3]));
				}
			}
		}
		
		
		
		/*FilteredClassifier fc = new FilteredClassifier();
		 fc.setClassifier(model);
		 // train and make predictions
		 fc.buildClassifier(train);
		 for (int i = 0; i < test.numInstances(); i++) {
		   double pred = fc.classifyInstance(test.instance(i));
		   System.out.print("ID: " + test.instance(i).value(0));
		   System.out.print(", actual: " + test.classAttribute().value((int) test.instance(i).classValue()));
		   System.out.println(", predicted: " + test.classAttribute().value((int) pred));
		 }*/
		
		/*BufferedReader datafile = readDataFile("weather.txt");
 
		Instances data = new Instances(datafile);
		data.setClassIndex(data.numAttributes() - 1);
 
		// Do 10-split cross validation
		Instances[][] split = crossValidationSplit(data, 10);
 
		// Separate split into training and testing arrays
		Instances[] trainingSplits = split[0];
		Instances[] testingSplits = split[1];
 
		
 
		// Run for each model
		for (int j = 0; j < models.length; j++) {
 
			// Collect every group of predictions for current model in a FastVector
			FastVector predictions = new FastVector();
 
			// For each training-testing split pair, train and test the classifier
			for (int i = 0; i < trainingSplits.length; i++) {
				Evaluation validation = classify(models[j], trainingSplits[i], testingSplits[i]);
 
				predictions.appendElements(validation.predictions());
 
				// Uncomment to see the summary for each training-testing pair.
				//System.out.println(models[j].toString());
			}
 
			// Calculate overall accuracy of current classifier on all splits
			double accuracy = calculateAccuracy(predictions);
 
			// Print current classifier's name and accuracy in a complicated,
			// but nice-looking way.
			System.out.println("Accuracy of " + models[j].getClass().getSimpleName() + ": "
					+ String.format("%.2f%%", accuracy)
					+ "\n---------------------------------");
		}*/
 
	}
}
