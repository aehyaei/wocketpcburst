package wockets.data;

import java.util.Date;

public class Acc {
	Date timeStamp;
	int value;
	public Acc(Date timeStamp, int value) {
		super();
		this.timeStamp = timeStamp;
		this.value = value;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	
}