package wockets.data.types;

public enum SensorDataType {
UNCOMPRESSED_DATA_PDU,
COMMAND_PDU,
RESPONSE_PDU,
COMPRESSED_DATA_PDU
}
