package wockets.data;

import java.util.ArrayList;

public class WocketParam {
	
	public int uncompressedCount = 0;
    public int last_seqNumber = 0;
    public int compressedCount = 0;
    public long lastConnection = 0;
    public long prevTstamp = 0;
    public int batchCount = 0;
    public String id = "00";
    public int batteryLevel = 0;
    public double period = 25;
    public String label = "";
    public String prevLabel = "";  
    public ArrayList<Acc> accs = new ArrayList<Acc>();
    public SWData swData = null;
    public SWData prevSwData = null;
    
 }

