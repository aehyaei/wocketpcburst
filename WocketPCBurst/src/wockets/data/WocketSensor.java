package wockets.data;

public class WocketSensor {
	private String macID;
	private String position;
	private String location;
	private String color;
	
	public String getMacID() {
		return macID;
	}
	public String getPosition() {
		return position;
	}
	public String getLocation() {
		return location;
	}
	public String getColor() {
		return color;
	}
	public void setMacID(String macID) {
		this.macID = macID;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	
	
	

}
