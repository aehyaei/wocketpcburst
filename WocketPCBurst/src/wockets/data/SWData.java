package wockets.data;

import java.util.Date;

public class SWData {
	private String status;
	private Date startTime;
	private int length;
	
	public SWData(String status, Date startTime, int length) {
		super();
		this.status = status;
		this.startTime = startTime;
		this.length = length;
	}

	public String getStatus() {
		return status;
	}

	public Date getStartTime() {
		return startTime;
	}

	public int getLength() {
		return length;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	
	
}
