package wockets.data;

import java.io.BufferedInputStream;
import java.util.ArrayList;
import xmlManager.XMLReader;

/**
 *
 * @author Aida
 */
public class ParticipantData {
   private String pID = "111"; //default
   private ArrayList<WocketSensor> wockets; 
   
   

   public ParticipantData() {
	   wockets = new ArrayList<WocketSensor>();
   }

   public String getpID() {
       return pID;
   }

   public void setpID(String pID) {
       this.pID = pID;
   }
   
   public ArrayList<WocketSensor> getWockets() {
		return wockets;
   }
   public void setWockets(ArrayList<WocketSensor> wockets) {
		this.wockets = wockets;
   }
   
   public void initialize() {
	  
	   /*BufferedInputStream iStream = XMLReader.fileFinder(pID);	   
	   if (iStream != null){
		   ParticipantData pDataCopy = XMLReader.xmlParser(iStream);
		   this.pID = pDataCopy.getpID();
		   this.wockets = pDataCopy.getWockets();
	   } else {*/
	   
		   WocketSensor wocket;
	   
		   // My test set at home
		   
		   wocket = new WocketSensor();
		   wocket.setMacID("00066606D442");
		   wocket.setColor("Red");
		   wocket.setPosition("Wrist");
		   wocket.setLocation("Dominant");
		   wockets.add(wocket);
		   
		   wocket = new WocketSensor();
		   wocket.setMacID("00066606D329");
		   wocket.setColor("Red");
		   wocket.setPosition("Ankle");
		   wocket.setLocation("Dominant");
		   wockets.add(wocket);
		   
		   wocket = new WocketSensor();
		   wocket.setMacID("00066606D3AD");
		   wocket.setColor("Green");
		   wocket.setPosition("Wrist");
		   wocket.setLocation("Dominant");
		   wockets.add(wocket);
		   
		   wocket = new WocketSensor();
		   wocket.setMacID("00066606D3BE");
		   wocket.setColor("Green");
		   wocket.setPosition("Ankle");
		   wocket.setLocation("Dominant");
		   wockets.add(wocket);	 
		   
		 //My Test Set at lab
		   /*wocket = new WocketSensor();
		   wocket.setMacID("00066606D3E8");
		   wocket.setColor("Red");
		   wocket.setPosition("Wrist");
		   wocket.setLocation("Dominant");
		   wockets.add(wocket);
		   
		   wocket = new WocketSensor();
		   wocket.setMacID("00066606D3FB");
		   wocket.setColor("Red");
		   wocket.setPosition("Ankle");
		   wocket.setLocation("Dominant");
		   wockets.add(wocket);
		   
		   wocket = new WocketSensor();
		   wocket.setMacID("00066606D433");
		   wocket.setColor("Green");
		   wocket.setPosition("Wrist");
		   wocket.setLocation("Dominant");
		   wockets.add(wocket);
		   
		   wocket = new WocketSensor();
		   wocket.setMacID("00066604B46E");
		   wocket.setColor("Green");
		   wocket.setPosition("Ankle");
		   wocket.setLocation("Dominant");
		   wockets.add(wocket);		*/   
	   //}
	   
   }
   
   
}
