package bafActivePAL;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.text.SimpleDateFormat; 
import java.util.Date;
import java.io.BufferedOutputStream;
import java.util.Calendar;
 public class Encoder {
  private Calendar firstTime=Calendar.getInstance();//to get the time first appeared in CSV file
  private Calendar AccTime=Calendar.getInstance();//to store the current time
  
  private  int x;
  private  int y;
  private  int z;
  private static String localPath= "D:/sleepStudy/";
  private static int previousX;
  private static int previousY;
  private static int previousZ;
  private static long prevTimeStamp;
  
  private long upperBoundHr=0;//upper bound in milliseconds for this hour
  private long lowerBoundHr=0;//lower bound in milliseconds for this hour
  private long tStamp;//time in milliseconds of UTC 
  private static byte[] data;
  private long ref_time = 0;//reference time
  private String ID;
  private BufferedOutputStream outputStream = null;
  private BufferedOutputStream PreoutputStream = null;//previous outputStream
  private int isNew=0;//to identify whether a output stream is the first one
  private final String srcFile;
  private final int    freq;//the sampling rate of activPAL
  public Encoder(String src,int freq){
	  resetPrevData();
	  this.srcFile=src;
	  this.freq=freq;
  }
  
   

//ID locates in a spot,begins with   "Monitor serial number:"
public  void  getIDFromCSV(){ 
	  String id=null;
	   try { 
		   BufferedReader br = new BufferedReader(new FileReader(srcFile));
		   String line = "";
		   StringTokenizer st = null;
	   		while ((line = br.readLine()) != null) {
	   			st = new StringTokenizer(line, ",");
	   			while (st.hasMoreTokens()) {
	   				String tempStr=st.nextToken();
	   				if (tempStr.contains("Monitor serial number:")){
				  
	   					String[] str=tempStr.split(":");
	   					id=str[1].replaceAll("\\W", "");//delete the space
	   				}
	   			}
	   			if (id!=null){
	   				break;
	   			}
		   }
	   			ID=id;
	   }
    	
   	catch (Exception e) {
   			e.printStackTrace();
   	}
  
   }

//get each data,from the CSV and write it line by line to the bufferedOutputstrem
 public void getAndWritedata(){
	   
	   try { 
		   BufferedReader br = new BufferedReader(new FileReader(srcFile));
		   String line = "";
		   StringTokenizer st = null;
		   //read comma separated file line by line
		   while ((line = br.readLine()) != null) {
			   st = new StringTokenizer(line, ",");
			   while (st.hasMoreTokens()) {
				   String tempStr=st.nextToken();
				   //the 1st column,it contains date
				   if (tempStr.contains("#")){
					   
					    st.nextToken();//the 2nd 
					    x=Integer.parseInt(st.nextToken());//the 3rd
					    y=Integer.parseInt(st.nextToken());//the 4th
					    z=Integer.parseInt(st.nextToken());//the 5th
					    tStamp=AccTime.getTimeInMillis();
					    encodeOneLine();
					    AccTime.add(Calendar.MILLISECOND,1000/freq);
				   }
			   }
		   }
		   
		   System.out.println("the encoding successfully finished");// 
	   }
	    	
	   	catch (Exception e) {
	   		e.printStackTrace();
	   	}
	   finally{
		   		try{//during the last hour,the outputstream may close before the the last seconds of the hour.
		   			//so close it here;instead in encodeAndSaveData
		   			if( outputStream != null){
		   				outputStream.flush();
		   				outputStream.close();
					}
		   		} catch (IOException e) {
				
				e.printStackTrace();
		   		}
		   
		   	}   
   }
   //raw data only accurate in seconds,"Acctime" make time has accuracy in milliseconds
   // and will be used as stamp for encoding.We get it,by finding the first turning point of 
   //one second to another,then back tracing to the first appearing time
  public void getAccurateTime(){
	   firstTime.add(Calendar.MILLISECOND, 1000);//add one second to firstTime
	   try { 
		   BufferedReader br = new BufferedReader(new FileReader(srcFile));
		   String line = "";
		   StringTokenizer st = null;

		   int lineNumber = 0; 
		   Date temp=firstTime.getTime();
		   Date date=AccTime.getTime();//just initialize it
		   while ((line = br.readLine()) != null) {
			   st = new StringTokenizer(line, ",");
			   while (st.hasMoreTokens()) {
				   String tempStr=st.nextToken();
				   if (tempStr.contains("#")){
					   lineNumber++;//count which line the 1st second change to 2nd one
					   date=getTimeFromString(tempStr);
					  	}
			   }
			   if (temp.equals(date)){
				   break; 
			   }
		   }
		   AccTime.setTime(date);
		   AccTime.add(Calendar.MILLISECOND, -1000/freq*(lineNumber-1));
		
	   }
	    	
	   	catch (Exception e) {
	   		e.printStackTrace();
	   	}
	  
	 }
  //the time is in this format #yyyy-MM-dd HH:mm:ss# 
  public Date getTimeFromString(String tempStr){
	  Date date=new Date();
	  try{ 
		  	char[]tempCharA=tempStr.toCharArray();//get time from the string
	   		char[]copyToChar=new char[19];
	   		System.arraycopy(tempCharA, 1, copyToChar, 0, copyToChar.length );
	   		String caldResult=new String(copyToChar); 
	   		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   		date =dateFormat.parse(caldResult);
	   	}catch(Exception e){
			  e.printStackTrace();
	   	}
	  
	   return date;
  } 
  //get the first appearing time,by find first appearing "#"
   public void getTheFirstTime(){
	   try { 
		   BufferedReader br = new BufferedReader(new FileReader(srcFile));
		   String line = "";
		   StringTokenizer st = null;
		   int tokenNumber = 0;
		   while ((line = br.readLine()) != null) {
			   st = new StringTokenizer(line, ",");
			   while (st.hasMoreTokens()) {
	      
				   String tempStr=st.nextToken();
				   if (tempStr.contains("#")){
					   tokenNumber++;
					   firstTime.setTime(getTimeFromString(tempStr));
					   break;
	    	   
				   }
			   }
			   if (tokenNumber!=0){
				   break; 
			   }
		   }
		   		
	  }catch (Exception e) {
	   		e.printStackTrace();
	   	}
	 
	 }
   /**
	 * giving the lower bound and upper bound in milliseconds for the hour,then we only check the outputDir
	 * once an hour,which speed up a lot.
	 * 
	 */
 public void encodeOneLine( ){
	    
		boolean isNewFile = false;
		try {
			
				if (upperBoundHr==0&&lowerBoundHr==0){
					String outputFilePath = getFilePathByTime();
					File outputDir = new File(outputFilePath);
					if(!outputDir.isDirectory()){
						outputDir.mkdirs();
					}
					String outputFileName = getFileNameForCurrentHour(outputFilePath);
					File outputFile = new File(outputFilePath+outputFileName);
					if(!outputFile.exists()){
						outputFile.createNewFile();
						isNewFile = true;
						outputStream = new BufferedOutputStream(new FileOutputStream(outputFile,true));
						if(isNew==0){//the first loop
							PreoutputStream=outputStream;
						}
						isNew++;
					}

					if(isNewFile){
						resetPrevData();
					}
					lowerBoundHr=tStamp;
			//calculate the upper bound, it is  yyyy-MM-dd hh-59-59-950
					Calendar temp=(Calendar) AccTime.clone();
					temp.set(Calendar.MINUTE,59);
					temp.set(Calendar.SECOND,59);
					temp.set(Calendar.MILLISECOND,1000-1000/freq);
					upperBoundHr=temp.getTimeInMillis();
					
					
				}
			
				if((lowerBoundHr<=tStamp)&&(tStamp<=upperBoundHr)){
			
					outputStream.write(encodeTime());
					outputStream.write(encoRawData());
				}else{
						lowerBoundHr=0;
						upperBoundHr=0;
						encodeOneLine();
				//for the case like 12:00:00:000,make the call again
				}

		} catch (IOException e) {
		
				e.printStackTrace();
		} finally{//if the first stream does not equals to the second
				if(!PreoutputStream.equals(outputStream)){
					try {
						if( PreoutputStream != null){
							PreoutputStream.flush();
							PreoutputStream.close();
							PreoutputStream=outputStream;
						}
					}catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
}
	
	//*************************************************************************************************************************************
	private void resetPrevData(){
		previousX = 0;
		previousY = 0;
		previousZ = 0;
		prevTimeStamp = 0;
		//ref_time = 0;
	}
	private byte[] encodeTime(){
		if ((prevTimeStamp == 0)|| (tStamp-ref_time > 60000)||(tStamp -prevTimeStamp>127)){
			ref_time = tStamp;
			return encodeFullTimeStamp();			
		} else{
			return encodeDiffTimeStamp(tStamp-prevTimeStamp);
		}
	}
	public byte[] encodeFullTimeStamp(){

		byte[] timeInBytes;
		prevTimeStamp = tStamp;
		Calendar timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(tStamp);
		int year = timeStamp.get(Calendar.YEAR);
		int month = timeStamp.get(Calendar.MONTH);
		month++;
		int day = timeStamp.get(Calendar.DAY_OF_MONTH);
		int mills = (int) (timeStamp.get(Calendar.HOUR_OF_DAY)*60*60*1000 + timeStamp.get(Calendar.MINUTE)*60*1000 + timeStamp.get(Calendar.SECOND)*1000 + timeStamp.get(Calendar.MILLISECOND));
		timeInBytes = new byte[8];
		timeInBytes[0] = (byte) ((byte)((year >>> 8))|0x80);
		timeInBytes[1] = (byte)(year);
		timeInBytes[2] = (byte)(month);
		timeInBytes[3] = (byte)(day);
		timeInBytes[4] = (byte)(mills >>> 24);
		timeInBytes[5] = (byte)(mills >>> 16);
		timeInBytes[6] = (byte)(mills >>> 8);
		timeInBytes[7] = (byte)(mills);
		return timeInBytes;
	}
	public byte[] encodeDiffTimeStamp(long diff){
		byte[] timeInBytes;
		prevTimeStamp += diff;
		timeInBytes = new byte[1];
		timeInBytes[0] = (byte)diff;
		return timeInBytes;
	}
	
	public byte[] encoRawData(){
		if(previousX == 0 && previousY == 0 && previousZ == 0){
			previousX = x;
			previousY = y;
			previousZ = z;
			uncompressedData();
		}
		else{
			int dx,dy,dz;
			dx=x-previousX;
			dy=y-previousY;
			dz=z-previousZ;
			previousX=x;
			previousY=y;
			previousZ=z;
			if(dx >= -15 && dx <= 15 && dy >= -15 && dy <= 15 && dz >= -15 && dz <= 15){
				compressedData(dx, dy, dz);
			}else
				uncompressedData();
		}
		return data;
	}
	
	private void compressedData(int dx, int dy, int dz){
		data = new byte[2];
		if(dx < 0){
			dx *= -1;
			data[0] = (byte) (((byte) (dx << 2) & 0x3c) | 0x40);
		}
		else
			data[0] = (byte) (((byte) (dx << 2) & 0x3c));
		if(dy < 0){
			dy *= -1;
			data[0] = (byte) (data[0] | 0x02 | ((byte)dy >> 3));
			data[1] = (byte) (dy << 5);
		}
		else{
			data[0] = (byte) (data[0] | ((byte)dy >> 3));
			data[1] = (byte) (dy << 5);
		}
		if(dz < 0){
			dz *= -1;
			data[1] = (byte) (data[1] | 0x10 | (byte)dz);
		}
		else
			data[1] = (byte) (data[1] | (byte)dz);
	}
	
	private void uncompressedData(){
		data = new byte[4];
		data[0]=(byte) ((x >>> 4) | 0xc0);
		data[1]=(byte) (((x << 4) & 0xf0) | ((y >>> 6) & 0x0f));
		data[2]=(byte) (((y << 2) & 0xfc) | ((z >>> 8) & 0x03));		
		data[3]=(byte) (z);
	}
	
	/**
	 * Gets the path for the raw data file by day and hour.
	 *
	 * @return the path by time
	 */
	private String getFilePathByTime()
	{
		Date timeStamp = AccTime.getTime();
		SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat hour = new SimpleDateFormat("HH");

		String path = localPath +day.format(timeStamp)+"/"+hour.format(timeStamp)+"/";
		return path;
	}
	/**
	 * Checks the file name for current hour. If there is a file exists, return it; if not, generate the file name by the time. 
	 *
	 * @param path the path
	 * @return the file name for current hour
	 */
	private String getFileNameForCurrentHour(String path){
		File dir = new File(path);
		String[] files = dir.list(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String filename) {
				// TODO Auto-generated method stub
				if ((filename.endsWith(".baf"))&&(filename.contains("activPAL."+ID)))
					return true;//.contains("activPAL."+ID);
				else 
					return false;
			}
		});
		if(files == null || files.length == 0)
			return generateFileNameByTime();
		else
			return files[0];
	}

	/**
	 * Generate the raw file name by time. ID is assigned by default.
	 *
	 * @return the string
	 */
	private String generateFileNameByTime()
	{
		Date timeStamp = AccTime.getTime();
		SimpleDateFormat detailedTime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
		String fileName="activPAL."+ID+"."+detailedTime.format(timeStamp)+".baf";		
		return fileName;
	}
	
	public  void clearOldBAF(){
		SimpleDateFormat dayformat = new SimpleDateFormat("yyyy-MM-dd");
		String dataStamp=dayformat.format(firstTime.getTime());
		
		File dataFolder = new File(localPath);
		String[] days = dataFolder.list();	
		if(days == null || days.length == 0)
			return;
		for (String day : days) {
			if(day.equals(dataStamp)){
				File dayFolder = new File(localPath+day);
				if(dayFolder.isDirectory()){
					String[] hours = dayFolder.list(new FilenameFilter() {
						@Override
						public boolean accept(File dir, String filename) {							
							return filename.matches("\\d{2}");
						}
					});
					int[] hoursNum = new int[hours.length];
					for (int i = 0; i < hours.length; i++) {
						hoursNum[i] = Integer.parseInt(hours[i]);
					}
					for (int hourNum : hoursNum) {
						
							String hour = String.format("%02d", hourNum);
							File hourFolder = new File(localPath+day+"/"+hour);
							if(hourFolder.isDirectory()){
								String[] files = hourFolder.list(new FilenameFilter() {
									
									@Override
									public boolean accept(File dir, String filename) {
										// TODO Auto-generated method stub
										return filename.contains("activPAL."+ID);
									}
								});
								for (String fileName : files) {
									File tempFile=new File(localPath+day+"/"+hour+"/"+fileName);
									tempFile.delete();
								}
							}
						
					}
				}

			}
		}
	}
 }