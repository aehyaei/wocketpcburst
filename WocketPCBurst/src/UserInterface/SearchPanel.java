package userInterface;

import java.awt.CardLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.bluetooth.RemoteDevice;
import javax.swing.*;

import communication.PcClient;

import wockets.data.ParticipantData;

/**
 *
 * @author Aida
 */
public class SearchPanel extends javax.swing.JPanel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JPanel userProcessContainer;
    ParticipantData pData;
    int RD_NUMBER = 2;
    ArrayList<RemoteDevice> rRemoteDevice =  new ArrayList<RemoteDevice>();
    ArrayList<RemoteDevice> gRemoteDevice =  new ArrayList<RemoteDevice>();
    
    public SearchPanel(JPanel upc, ParticipantData pd) {
        initComponents();
        userProcessContainer = upc;
        pData = pd;
        redWocketsButton.setVisible(false);
        greenWocketsButton.setVisible(false); 
        jLabel5.setVisible(false);
        jLabel6.setVisible(false);
        
        this.addComponentListener(new ComponentListener() {

            @Override
            public void componentResized(ComponentEvent e) {
            }

            @Override
            public void componentMoved(ComponentEvent e) {
            }

            @Override
            public void componentShown(ComponentEvent e) {
                //System.out.println("Shown");
                
                new SwingWorker<String, Object>() {
                    @Override
                    public String doInBackground() {
                        findRunnable.run(); 
                        return "";
                    }
                }.execute();
                
                //SwingWorker.invokeLater(connectRunnable);
            }

            @Override
            public void componentHidden(ComponentEvent e) {
            }
        });
        
        //SwingUtilities.invokeLater(findRunnable);
    }

    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        redWocketsButton = new javax.swing.JButton();
        greenWocketsButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("PC-Wocket Application");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Data Collection");

        redWocketsButton.setText("Use Red Wockets  ");
        redWocketsButton.setMaximumSize(new java.awt.Dimension(150, 23));
        redWocketsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redWocketsButtonActionPerformed(evt);
            }
        });

        greenWocketsButton.setText("Use Green Wockets");
        greenWocketsButton.setMaximumSize(new java.awt.Dimension(150, 23));
        greenWocketsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                greenWocketsButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Searching for Wockets..");

        jLabel5.setText("Your default sensors are not been detected. Please make sure your");

        jLabel6.setText("sensors have enough charge and they are separaed from the charger.");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(140, 140, 140)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(109, 109, 109)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(44, 44, 44)
                                .addComponent(jLabel3))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(8, 8, 8))
                            .addComponent(jLabel6)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(118, 118, 118)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(greenWocketsButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(redWocketsButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(33, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addGap(26, 26, 26)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addComponent(redWocketsButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(greenWocketsButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(72, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void redWocketsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redWocketsButtonActionPerformed
        greenWocketsButton.setVisible(false);        
        JPanel connectingPanel =  new ConnectingPanel(userProcessContainer, rRemoteDevice);
        userProcessContainer.add("ConnectingPanel",connectingPanel);
        CardLayout c1 = (CardLayout)userProcessContainer.getLayout();
        c1.next(userProcessContainer);
    }//GEN-LAST:event_redWocketsButtonActionPerformed

    private void greenWocketsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_greenWocketsButtonActionPerformed
        redWocketsButton.setVisible(false);
        JPanel connectingPanel =  new ConnectingPanel(userProcessContainer, gRemoteDevice);
        userProcessContainer.add("ConnectingPanel",connectingPanel);
        CardLayout c1 = (CardLayout)userProcessContainer.getLayout();
        c1.next(userProcessContainer);
        
    }//GEN-LAST:event_greenWocketsButtonActionPerformed

    Runnable findRunnable = new Runnable() {
        public void run() { 
            try {
                PcClient.findDevices();                    
                Vector<?> btDevices = PcClient.vecDevices;
                int btSize = btDevices.size();
                for (int k=0; k<btSize; k++){
                    RemoteDevice remoteDevice = (RemoteDevice)btDevices.elementAt(k);
                    String adr= remoteDevice.getBluetoothAddress();  
                    
                    if (adr.contains("0006660")){
                        //System.out.println(adr);
                        if (adr.equals(pData.getWockets().get(0).getMacID())){
                            rRemoteDevice.add(remoteDevice);
                        }else if (adr.equals(pData.getWockets().get(1).getMacID())){
                        	rRemoteDevice.add(remoteDevice);
                        }else if (adr.equals(pData.getWockets().get(2).getMacID())){
                            gRemoteDevice.add(remoteDevice);
                        }else if (adr.equals(pData.getWockets().get(3).getMacID())){
                    		gRemoteDevice.add(remoteDevice);
                        }
                
                    }
                }

                int rCounter = rRemoteDevice.size();
                int gCounter = gRemoteDevice.size();
                if (rCounter > 0){
                    redWocketsButton.setVisible(true);                    
                    jLabel2.setVisible(false);
                }
                if (gCounter > 0){
                    greenWocketsButton.setVisible(true);
                    jLabel2.setVisible(false);
                }
                if ((rCounter == 0) && (gCounter == 0)) {
                    jLabel2.setVisible(false);
                    jLabel5.setVisible(true);
                    jLabel6.setVisible(true);
                }
            } catch (IOException ex) {
                Logger.getLogger(SearchPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }            
    };
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton greenWocketsButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JButton redWocketsButton;
    // End of variables declaration//GEN-END:variables
}
