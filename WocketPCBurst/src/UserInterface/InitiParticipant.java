package userInterface;

import java.awt.CardLayout;
import java.io.BufferedInputStream;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.xmlpull.v1.XmlPullParserException;
import wockets.data.ParticipantData;
import xmlManager.XMLReader;
import xmlManager.XMLWriter;

/**
 *
 * @author Aida
 */
public class InitiParticipant extends javax.swing.JPanel {

    private static final long serialVersionUID = 1L;
	JPanel userProcessContainer;
    ParticipantData pData;
    
    
    public InitiParticipant(JPanel upc, ParticipantData pd) {
        initComponents();
        userProcessContainer = upc;
        pData = pd;
    }

    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        participantButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        pIDTextField = new javax.swing.JTextField();
        cancelButton = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("PC-Wocket Application");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Initialization");

        participantButton.setText("Done");
        participantButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                participantButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Enter participant ID:");

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(68, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(57, 57, 57)
                                .addComponent(jLabel3)))
                        .addGap(91, 91, 91))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(74, 74, 74)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(participantButton, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                            .addComponent(pIDTextField)
                            .addComponent(cancelButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(62, 62, 62))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(pIDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(participantButton)
                .addGap(30, 30, 30)
                .addComponent(cancelButton)
                .addContainerGap(120, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void participantButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_participantButtonActionPerformed
        if (!pIDTextField.getText().trim().isEmpty()){
            String id = pIDTextField.getText();        
            try {
                Integer.parseInt(id);
            }catch(NumberFormatException nFE) {
                JOptionPane.showMessageDialog(null, "Please enter a numeric subject ID. "
                    + "If you want to use the default subject ID, click Cancel.");
                return;
            }
            pData.setpID(id);
            BufferedInputStream iStream = XMLReader.fileFinder(id);
     	    if (iStream == null) {
	            try {
	                    XMLWriter.write(pData);
	            } catch (XmlPullParserException e) {
	                    System.out.println("XmlPullParserException:"+e);
	            } catch (IOException e) {
	                    System.out.println("IOException:"+e);
	            }
     	    }
        } else {
            JOptionPane.showMessageDialog(null, "You didn't enter the subject ID. "
                    + "If you want to use the default subject ID, click Cancel.");
            return;
        }
        userProcessContainer.remove(this);
        CardLayout c1 = (CardLayout)userProcessContainer.getLayout();
        c1.previous(userProcessContainer);
    }//GEN-LAST:event_participantButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout c1 = (CardLayout)userProcessContainer.getLayout();
        c1.previous(userProcessContainer);
    }//GEN-LAST:event_cancelButtonActionPerformed

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField pIDTextField;
    private javax.swing.JButton participantButton;
    // End of variables declaration//GEN-END:variables
}
