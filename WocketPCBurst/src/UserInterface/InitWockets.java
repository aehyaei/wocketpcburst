/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface;

import java.awt.CardLayout;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.bluetooth.RemoteDevice;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane;
import org.xmlpull.v1.XmlPullParserException;
import communication.PcClient;
import wockets.data.ParticipantData;
import xmlManager.XMLWriter;

/**
 *
 * @author Aida
 */
public class InitWockets extends javax.swing.JPanel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JPanel userProcessContainer; 
    ParticipantData pData;
    Vector<?> btDevices;
    PcClient btConnect;
    int index = 0;
    Hashtable<String, String> wockets = new Hashtable<String, String>();
    
    public InitWockets(JPanel upc, ParticipantData pd) {
        initComponents(); 
        userProcessContainer = upc;
        pData = pd;
        wockets.put("Red A", "");
        wockets.put("Red W", "");
        wockets.put("Green A", "");
        wockets.put("Green W", "");
        redWjButton.setVisible(false);
        greenAjButton.setVisible(false);
        greenWjButton.setVisible(false);
        confirmjButton.setVisible(false);
        reMatchjButton.setVisible(false);
        try {
            PcClient.findDevices();
        } catch (IOException ex) {
            Logger.getLogger(InitWockets.class.getName()).log(Level.SEVERE, null, ex);
        }
        btDevices = PcClient.vecDevices;
        refresh();  
        //PcClient.beepRunnable.run();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        wocketsTable = new javax.swing.JTable();
        redAjButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        messageLabel = new javax.swing.JLabel();
        redWjButton = new javax.swing.JButton();
        greenAjButton = new javax.swing.JButton();
        greenWjButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        confirmjButton = new javax.swing.JButton();
        canceljButton = new javax.swing.JButton();
        reMatchjButton = new javax.swing.JButton();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        wocketsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Index", "Wocket Address", "Wocket Name"
            }
        ));
        jScrollPane1.setViewportView(wocketsTable);

        redAjButton.setText("Set as Red A");
        redAjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redAjButtonActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("PC-Wocket Application");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Set Wockets");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Select the MAC number which matches to the \"Red A\" Wocket:");

        messageLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        messageLabel.setText("   ");

        redWjButton.setText("Set as Red W");
        redWjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redWjButtonActionPerformed(evt);
            }
        });

        greenAjButton.setText("Set as Green A");
        greenAjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                greenAjButtonActionPerformed(evt);
            }
        });

        greenWjButton.setText("Set as Green W");
        greenWjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                greenWjButtonActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Then click the bottun:");

        confirmjButton.setText("Confirm");
        confirmjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmjButtonActionPerformed(evt);
            }
        });

        canceljButton.setText("Cancel");
        canceljButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                canceljButtonActionPerformed(evt);
            }
        });

        reMatchjButton.setText("Re-match");
        reMatchjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reMatchjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 434, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(161, 161, 161)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(greenAjButton)
                                    .addComponent(redAjButton)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(canceljButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(reMatchjButton)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(redWjButton)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(greenWjButton)
                                .addGap(23, 23, 23)
                                .addComponent(confirmjButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(messageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(186, 186, 186)
                        .addComponent(jLabel3)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addGap(52, 52, 52)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(redAjButton)
                    .addComponent(redWjButton))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(messageLabel))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(greenAjButton)
                            .addComponent(greenWjButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(confirmjButton)
                            .addComponent(canceljButton)
                            .addComponent(reMatchjButton))))
                .addContainerGap(25, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    
    private void refresh(){
        int rowCount = wocketsTable.getRowCount();
        int i;

        for ( i= rowCount-1; i>= 0; i--){
        ((DefaultTableModel)wocketsTable.getModel()).removeRow(i);   
        }
                
       
        int btSize = btDevices.size();
        int cnt = 0;
        for (int k=0; k<btSize; k++) {
            RemoteDevice remoteDevice=(RemoteDevice)btDevices.elementAt(k);
            String adr= remoteDevice.getBluetoothAddress();            
            if (adr.contains("0006660")){
                cnt++;
                Object[]new_row = new Object[3];
                new_row[0] = cnt; 
                new_row[1] = adr;
                if (wockets.containsValue(adr)){
                    Set<String> nameList = wockets.keySet();
                    for (String name: nameList){
                        if (wockets.get(name).equals(adr)){
                            new_row[2]= name;
                            break;
                        }
                    }
                } else
                    new_row[2] = adr.substring(8);
                    
                ((DefaultTableModel)wocketsTable.getModel()).addRow(new_row);            
            } else {
                btDevices.removeElementAt(k);
                k--;
                btSize--;
            }
        }
    }
    
    private void redAjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redAjButtonActionPerformed

        int temp = wocketsTable.getSelectedRow();
        if (temp == -1){
            JOptionPane.showMessageDialog(null, "Please select the Wocket with MAC number matches to the Red A Wocket");
            return;
        }
        index = (Integer) wocketsTable.getValueAt(wocketsTable.getSelectedRow(), 0);
        String wAddress = (String)wocketsTable.getValueAt(wocketsTable.getSelectedRow(), 1);
        //pData.setRaMAC(wName);
        pData.getWockets().get(1).setMacID(wAddress);
        wockets.put("Red A", wAddress);
        redAjButton.setVisible(false);
        redWjButton.setVisible(true);
        jLabel4.setText("Select the MAC number which matches to the \"Red W\" Wocket:");
        refresh();
    }//GEN-LAST:event_redAjButtonActionPerformed

    private void redWjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redWjButtonActionPerformed
        int temp = wocketsTable.getSelectedRow();
        if (temp == -1){
            JOptionPane.showMessageDialog(null, "Please select the Wocket with MAC number matches to the Red W Wocket");
            return;
        }
        index = (Integer) wocketsTable.getValueAt(wocketsTable.getSelectedRow(), 0);
        String wAddress = (String)wocketsTable.getValueAt(wocketsTable.getSelectedRow(), 1);
        //pData.setRaMAC(wName);
        pData.getWockets().get(0).setMacID(wAddress);
        wockets.put("Red W", wAddress);
        redWjButton.setVisible(false);
        greenAjButton.setVisible(true);
        jLabel4.setText("Select the MAC number which matches to the \"Green A\" Wocket:");
        refresh();
    }//GEN-LAST:event_redWjButtonActionPerformed

    private void greenAjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_greenAjButtonActionPerformed
        int temp = wocketsTable.getSelectedRow();
        if (temp == -1){
            JOptionPane.showMessageDialog(null, "Please select the Wocket with MAC number matches to the Green A Wocket");
            return;
        }
        index = (Integer) wocketsTable.getValueAt(wocketsTable.getSelectedRow(), 0);
        String wAddress = (String)wocketsTable.getValueAt(wocketsTable.getSelectedRow(), 1);
        //pData.setGaMAC(wName);
        pData.getWockets().get(3).setMacID(wAddress);
        wockets.put("Green A", wAddress);
        greenAjButton.setVisible(false);
        greenWjButton.setVisible(true);
        jLabel4.setText("Select the MAC number which matches to the \"green W\" Wocket:");
        refresh();
    }//GEN-LAST:event_greenAjButtonActionPerformed

    private void greenWjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_greenWjButtonActionPerformed
        int temp = wocketsTable.getSelectedRow();
        if (temp == -1){
            JOptionPane.showMessageDialog(null, "Please select the Wocket with MAC number matches to the Green A Wocket");
            return;
        }
        index = (Integer) wocketsTable.getValueAt(wocketsTable.getSelectedRow(), 0);
        String wAddress = (String)wocketsTable.getValueAt(wocketsTable.getSelectedRow(), 1);
        //pData.setGwMAC(wName);
        pData.getWockets().get(2).setMacID(wAddress);
        wockets.put("Green W", wAddress);
        refresh();
        greenWjButton.setVisible(false);
        confirmjButton.setVisible(true);
        reMatchjButton.setVisible(true);
        jLabel2.setVisible(false);
        jLabel4.setVisible(false);
    }//GEN-LAST:event_greenWjButtonActionPerformed

    private void confirmjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmjButtonActionPerformed
       try {
			XMLWriter.write(pData);
		} catch (XmlPullParserException e) {
			System.out.println("XmlPullParserException:"+e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IOException:"+e);
			e.printStackTrace();
		}
        userProcessContainer.remove(this);
        CardLayout c1 = (CardLayout)userProcessContainer.getLayout();
        c1.previous(userProcessContainer);
    }//GEN-LAST:event_confirmjButtonActionPerformed

    private void canceljButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_canceljButtonActionPerformed
        userProcessContainer.remove(this);
        CardLayout c1 = (CardLayout)userProcessContainer.getLayout();
        c1.previous(userProcessContainer);
    }//GEN-LAST:event_canceljButtonActionPerformed

    private void reMatchjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reMatchjButtonActionPerformed
        wockets.put("Red A", "");
        wockets.put("Red W", "");
        wockets.put("Green A", "");
        wockets.put("Green W", "");
        redAjButton.setVisible(true);
        redWjButton.setVisible(false);
        greenAjButton.setVisible(false);
        greenWjButton.setVisible(false);
        confirmjButton.setVisible(false);
        canceljButton.setVisible(false);
        reMatchjButton.setVisible(false);
        try {
            PcClient.findDevices();
        } catch (IOException ex) {
            Logger.getLogger(InitWockets.class.getName()).log(Level.SEVERE, null, ex);
        }
        btDevices = PcClient.vecDevices;
        refresh();
    }//GEN-LAST:event_reMatchjButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton canceljButton;
    private javax.swing.JButton confirmjButton;
    private javax.swing.JButton greenAjButton;
    private javax.swing.JButton greenWjButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel messageLabel;
    private javax.swing.JButton reMatchjButton;
    private javax.swing.JButton redAjButton;
    private javax.swing.JButton redWjButton;
    private javax.swing.JTable wocketsTable;
    // End of variables declaration//GEN-END:variables
}
