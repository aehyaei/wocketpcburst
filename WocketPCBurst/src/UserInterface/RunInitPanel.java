package userInterface;

import java.awt.CardLayout;
import java.io.BufferedInputStream;
import java.io.IOException;
import javax.swing.JPanel;
import org.xmlpull.v1.XmlPullParserException;
import wockets.data.ParticipantData;
import xmlManager.XMLReader;
import xmlManager.XMLWriter;

/**
 *
 * @author Aida
 */
public class RunInitPanel extends javax.swing.JPanel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JPanel userProcessContainer;
    ParticipantData pData;
    
    
    public RunInitPanel(JPanel upc, ParticipantData pd) {
        initComponents();
        userProcessContainer = upc;
        pData = pd;
    }

    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        initializeButton = new javax.swing.JButton();
        runButton = new javax.swing.JButton();
        setWocketsjButton = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("PC-Wocket Application");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Finding Wockets");

        initializeButton.setText("Initialize new participant");
        initializeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initializeButtonActionPerformed(evt);
            }
        });

        runButton.setText("Start data Collection");
        runButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runButtonActionPerformed(evt);
            }
        });

        setWocketsjButton.setText("Set new Wockets");
        setWocketsjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setWocketsjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(109, 109, 109)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(runButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(setWocketsjButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(initializeButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(jLabel3)))
                .addContainerGap(97, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addGap(57, 57, 57)
                .addComponent(runButton)
                .addGap(26, 26, 26)
                .addComponent(initializeButton)
                .addGap(27, 27, 27)
                .addComponent(setWocketsjButton)
                .addContainerGap(99, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void initializeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initializeButtonActionPerformed
        JPanel initializationPanel =  new InitiParticipant(userProcessContainer, pData);
        userProcessContainer.add("initializationPanel",initializationPanel);        
        CardLayout c1 = (CardLayout)userProcessContainer.getLayout();
        c1.next(userProcessContainer);
    }//GEN-LAST:event_initializeButtonActionPerformed

    private void runButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runButtonActionPerformed
    	BufferedInputStream iStream = XMLReader.fileFinder(pData.getpID());
 	    if (iStream == null) {
            try {
                    XMLWriter.write(pData);
            } catch (XmlPullParserException e) {
                    System.out.println("XmlPullParserException:"+e);
            } catch (IOException e) {
                    System.out.println("IOException:"+e);
            }
 	    }
        JPanel runPanel =  new SearchPanel(userProcessContainer, pData);
        userProcessContainer.add("runPanel",runPanel);
        CardLayout c1 = (CardLayout)userProcessContainer.getLayout();
        c1.next(userProcessContainer);
    }//GEN-LAST:event_runButtonActionPerformed

    private void setWocketsjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setWocketsjButtonActionPerformed
        JPanel init2Panel =  new InitWockets(userProcessContainer, pData);
        userProcessContainer.add("init2Panel",init2Panel);
        CardLayout c1 = (CardLayout)userProcessContainer.getLayout();
        c1.next(userProcessContainer);
    }//GEN-LAST:event_setWocketsjButtonActionPerformed

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton initializeButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton runButton;
    private javax.swing.JButton setWocketsjButton;
    // End of variables declaration//GEN-END:variables
}
