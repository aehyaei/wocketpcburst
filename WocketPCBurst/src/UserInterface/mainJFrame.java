package userInterface;

import java.awt.CardLayout;
import java.util.ArrayList;
import javax.bluetooth.RemoteDevice;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import communication.PcClient;
//import org.xmlpull.v1.*;
import wockets.data.ParticipantData;

/**
 *
 * @author Aida Ehyaei
 */
public class mainJFrame extends javax.swing.JFrame {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static ParticipantData pData;
    
    public mainJFrame() {
        initComponents();
        pData = new ParticipantData();
        pData.initialize();
        stopjButton.setVisible(false);
        swapButton.setVisible(false);
        this.setTitle("Wocket data collection");
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        startjButton = new javax.swing.JButton();
        stopjButton = new javax.swing.JButton();
        swapButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();
        userProcessContainer = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(640, 480));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jSplitPane1.setDividerLocation(120);

        startjButton.setText("Start");
        startjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startjButtonActionPerformed(evt);
            }
        });

        stopjButton.setText("Stop");
        stopjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopjButtonActionPerformed(evt);
            }
        });

        swapButton.setText("   Swap   ");
        swapButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                swapButtonActionPerformed(evt);
            }
        });

        closeButton.setText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(startjButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                    .add(stopjButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                    .add(swapButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 483, Short.MAX_VALUE)
                    .add(closeButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(155, 155, 155)
                .add(startjButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(stopjButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(swapButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 84, Short.MAX_VALUE)
                .add(closeButton)
                .addContainerGap())
        );

        jSplitPane1.setLeftComponent(jPanel1);

        userProcessContainer.setLayout(new java.awt.CardLayout());

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jTextArea1.setRows(5);
        jTextArea1.setText("\n\n\n\n\n\n\n\n\nWocket Data Collection\n\nThis program leads you to use Wockets for monitoring a patient.");
        jScrollPane1.setViewportView(jTextArea1);

        userProcessContainer.add(jScrollPane1, "card3");

        jSplitPane1.setRightComponent(userProcessContainer);

        getContentPane().add(jSplitPane1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    static SwingWorker<String, Object> swingWorker = null;
    
    public static void swStart(final ArrayList<RemoteDevice> remoteDevice, final JTextArea textArea) {
    	if (swingWorker == null) {
            swingWorker = new SwingWorker<String, Object>() {
                @Override
                public String doInBackground() {
                    new connectRunnable(remoteDevice, textArea).run();
                    return "";
                }
            };
            swingWorker.execute();
        }
    }
    static class connectRunnable implements Runnable {
    	private ArrayList<RemoteDevice> remoteDevice;
		private JTextArea textArea;
		public connectRunnable(ArrayList<RemoteDevice> remoteDevice, JTextArea textArea) {
    		this.remoteDevice = remoteDevice;
    		this.textArea = textArea;
    	}

        @Override
        public void run() {
            PcClient.start();
            PcClient.collectData(pData, remoteDevice, textArea);            
        }
    }
    
private void startjButtonActionPerformed(java.awt.event.ActionEvent evt) {                                              
    PcClient.start();
    swingWorker = null;
    JPanel runInitPanel =  new RunInitPanel(userProcessContainer, pData);
    userProcessContainer.add("runInitPanel",runInitPanel);
    CardLayout c1 = (CardLayout)userProcessContainer.getLayout();
    c1.next(userProcessContainer);
    startjButton.setVisible(false);
    stopjButton.setVisible(true);
    swapButton.setVisible(true);
}                                            

private void stopjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopjButtonActionPerformed
    userProcessContainer.removeAll(); 
    PcClient.stop();
    if (swingWorker != null)
    	swingWorker.cancel(true);
    stopjButton.setVisible(false);
    swapButton.setVisible(false);
    startjButton.setVisible(true); 
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
}//GEN-LAST:event_stopjButtonActionPerformed

    private void swapButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_swapButtonActionPerformed
        userProcessContainer.removeAll(); 
        PcClient.stop();         
        swingWorker.cancel(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        PcClient.start();
        swingWorker = null;
        JPanel runPanel =  new SearchPanel(userProcessContainer, pData);
        userProcessContainer.add("runPanel",runPanel);
        CardLayout c1 = (CardLayout)userProcessContainer.getLayout();
        c1.next(userProcessContainer);        
        stopjButton.setVisible(true);
        startjButton.setVisible(false);
    }//GEN-LAST:event_swapButtonActionPerformed

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
    	PcClient.stop();
        System.exit(0);
    }//GEN-LAST:event_closeButtonActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowClosing

    

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(mainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(mainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(mainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(mainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new mainJFrame().setVisible(true);
                
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton closeButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JButton startjButton;
    private javax.swing.JButton stopjButton;
    private javax.swing.JButton swapButton;
    private javax.swing.JPanel userProcessContainer;
    // End of variables declaration//GEN-END:variables
}
